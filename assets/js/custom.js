var winW = $(window).width();

var winH = $(window).height();

var homeSwiper, courseSwiper, blogSlider, aboutTestimonialSlider, partnerSlider1, partnerSlider2;

$("a").each(function() {
    if ($(this).attr("href") == "#" || $(this).attr("href") == "" || $(this).attr("href") == " ") {
        $(this).attr("href", "javascript:void(0)");
    }
});

function imgToBg(objItem, objSrc) {
    $(objItem).each(function() {
        var imgSrc = $(this).find(objSrc).attr("src");
        $(this).css("background-image", "url(" + imgSrc + ")");
    });
}

function sliderCourse() {
    courseSwiper = new Swiper(".swiper-course", {
        spaceBetween: 20,
        slidesPerView: "auto",
        centeredSlides: false,
        navigation: {
            nextEl: ".slideNav .swiper-button-next",
            prevEl: ".slideNav .swiper-button-prev"
        },
        pagination: {
            el: ".slideNav .swiper-pagination",
            clickable: true
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
                centeredSlides: false
            },
            1025: {
                slidesPerView: 3,
                spaceBetween: 40,
                centeredSlides: false
            }
        }
    });
}

function swiperAlumni() {
    courseSwiper = new Swiper(".js-alumni .swiper-container", {
        spaceBetween: 20,
        slidesPerView: "auto",
        centeredSlides: true,
        navigation: {
            nextEl: ".js-alumni .slideNav .swiper-button-next",
            prevEl: ".js-alumni .slideNav .swiper-button-prev"
        },
        breakpoints: {
            768: {
                slidesPerView: 4,
                spaceBetween: 30,
                centeredSlides: false
            },
            1025: {
                slidesPerView: 4,
                spaceBetween: 40,
                centeredSlides: false
            }
        }
    });
}

function sliderTestimonials() {
    var galleryThumbs = new Swiper(".testimonial-img", {
        spaceBetween: 0,
        slidesPerView: "auto",
        centeredSlides: true,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true
    });
    var galleryTop = new Swiper(".testimonial-decs", {
        spaceBetween: 50,
        effect: "fade",
        loop: true,
        navigation: {
            nextEl: ".bs-testimonials .slideNav .swiper-button-next",
            prevEl: ".bs-testimonials .slideNav .swiper-button-prev"
        },
        pagination: {
            el: ".bs-testimonials .slideNav .swiper-pagination",
            clickable: true
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });
}

function homeBannerSwiper() {
    homeSwiper = new Swiper(".js-home-banner .swiper-container", {
        slidesPerView: 1,
        effect: "fade",
        speed: 1e3,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        },
        pagination: {
            el: ".js-home-banner .swiper-pagination",
            clickable: true
        }
    });
}

function blogBannerSwiper() {
    homeSwiper = new Swiper(".js-blog-banner .swiper-container", {
        slidesPerView: 1,
        effect: "fade",
        speed: 1e3,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        },
        pagination: {
            el: ".js-blog-banner .swiper-pagination",
            clickable: true
        }
    });
}

function showCase() {
    $(".js-trigger").on("click", function() {
        $(".js-trigger").removeClass("active");
        $(this).addClass("active");
        var src = $(this).data("img");
        $(".js-target").attr("src", src);
        imgToBg(".js-addto", ".js-img");
    });
}

function mobileMenuOpenClose() {
    $(".js-menu-btn").on("click", function() {
        $(".js-nav").addClass("active");
    });
    $(".js-nav-close").on("click", function() {
        $(".js-nav").removeClass("active");
    });
}

function equalizerHeight(jsContainer, jsItem, jsEqualize) {
    $(jsContainer).each(function() {
        var height = $(this).find(jsItem).outerHeight();
        $(jsEqualize).height(height);
    });
}

function blogSwiper() {
    blogSlider = new Swiper(".js-blog-swiper", {
        spaceBetween: 20,
        slidesPerView: "auto",
        centeredSlides: true,
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
                centeredSlides: false
            },
            1025: {
                slidesPerView: 3,
                spaceBetween: 40,
                centeredSlides: false
            }
        }
    });
}

function tabSelection() {
    $(".bs-tabs").each(function(e) {
        var obj = this;
        var selectId = $(obj).find(".tab-select select").attr("id");
        $("#" + selectId).on("change", function(e) {
            var currSel = e.currentTarget.selectedOptions[0].index;
            var selTab = $(obj).find(".nav.nav-tabs li").eq(currSel);
            $(selTab).find("a").click();
        });
    });
}

function mobileFooter() {
    $(".js-pointer").on("click", function() {
        if ($(this).hasClass("active") == true) {
            $(".js-pointer").removeClass("active");
            $(".link-wrap").slideUp("300");
        } else {
            $(".js-pointer").removeClass("active");
            $(".link-wrap").slideUp("300");
            $(this).toggleClass("active");
            $(this).siblings(".link-wrap").slideDown("300");
        }
    });
}

function hoverActive() {
    if (!$(".lyt-plan").hasClass("typ2")) {
        $(".lyt-plan .bs-plan").hover(function() {
            $(".lyt-plan .bs-plan").removeClass("active");
            $(this).addClass("active");
        });
    }
}

function fliter() {
    $(".js-fliter").on("click", function() {
        $(".js-fliter").removeClass("active");
        var getId = $(this).find("a").attr("id");
        $(this).addClass("active");
        $(".js-filteritem").removeClass("active");
        if ($("[data-filter=" + getId + "]").closest(".js-filteritem").hasClass("swiper-slide")) {
            courseSwiper.destroy();
            $("[data-filter=" + getId + "]").closest(".js-filteritem").addClass("active");
            sliderCourse();
        } else {
            $("[data-filter=" + getId + "]").closest(".js-filteritem").addClass("active");
        }
    });
}

function jsMove() {
    $(".js-move").appendTo(".js-placed");
}

function aboutTestimonialSwiper() {
    aboutTestimonialSlider = new Swiper(".lyt-testimonials .swiper-container", {
        slidesPerView: 1,
        centeredSlides: true,
        spaceBetween: 30,
        navigation: {
            nextEl: ".lyt-testimonials .swiper-button-next",
            prevEl: ".lyt-testimonials .swiper-button-prev"
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 30,
                centeredSlides: true,
                loop: false
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 20,
                centeredSlides: true,
                loop: false
            }
        }
    });
}

function partnerSwiper() {
    partnerSlider1 = new Swiper("#partnerSwiper1", {
        slidesPerView: 1,
        spaceBetween: 30,
        navigation: {
            nextEl: "#partnerSwiper1 .swiper-button-next",
            prevEl: "#partnerSwiper1 .swiper-button-prev"
        }
    });
    partnerSlider2 = new Swiper("#partnerSwiper2", {
        slidesPerView: 1,
        spaceBetween: 30,
        navigation: {
            nextEl: "#partnerSwiper2 .swiper-button-next",
            prevEl: "#partnerSwiper2 .swiper-button-prev"
        }
    });
}

function logoSlide() {
    partnerSlider1 = new Swiper("#logo-slide", {
        slidesPerView: 1,
        spaceBetween: 30,
        speed: 800,
        autoplay: {
            delay: 3e3,
            disableOnInteraction: false
        },
        pagination: {
            el: "#logo-slide .swiper-pagination",
            clickable: true
        }
    });
}

function mobMenu() {
    $(".js-mobMenu .nav-link").on("click", function() {
        $(".js-mobMenu").toggleClass("active");
        $(".js-mobMenu .lvl2").slideToggle("slow");
    });
}



function openCloseSearch() {
    var target = $(".bs-search-panel");
    $(".js-open-search-panel").on("click", function() {
        target.addClass("active");
    });
    $(".js-close-search-panel").on("click", function() {
        target.removeClass("active");
    });
}

function mobileBanner() {
    $(".js-responsive-png").each(function() {
        var e = $(this).find("img").attr("src").replace(".png", "-m.png");
        $(this).find("img").attr("src", e);
    });
    $(".js-responsive-jpg").each(function() {
        var e = $(this).find("img").attr("src").replace(".jpg", "-m.jpg");
        $(this).find("img").attr("src", e);
    });
}

function magicTabHoverEffect() {
    var e = $(".js-magic-line");
    $(".magic-line").remove(), e.append("<li class='magic-line'></li>");
    var t = $(".js-magic-line .magic-line");
    t.width($(".js-magic-line li.active").width()).css("left", $(".js-magic-line li.active").position().left), 
    $(".js-magic-line li a").hover(function() {}, function() {
        t.stop().animate({
            left: $(".js-magic-line li.active").position().left,
            width: $(".js-magic-line li.active").width()
        }, 400);
    });
}

function formValidate() {
    
    
    $("#queryForm").validate({
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            mob: {
                required: true,
                minlength: 10
            },
            mailid: {
                required: true,
                email: true
            },
            text: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please enter Name",
                minlength: "Your name more than 3 characters"
            },
            mob: {
                required: "Please Enter Mobile Number",
                minlength: "Please Enter 10 Digit Mobile Number"
            },
            mailid: {
                required: "Please provide a Email ID"
            },
            text: {
                required: "Please provide your remarks"
            }
        }
    });
    
}

function videoModalIframe(){
    $('.js-video-trigger').on('click',function(){
        var theModal = $(this).data("target"),
            videoSRC = $(this).attr("data-video"),
            videoSRCauto = videoSRC + "?autoplay=1&modestbranding=1&rel=0&controls=1&showinfo=0&html5=1";
            console.log(theModal);
        $(theModal + " iframe").attr("src", videoSRCauto);
        $(theModal + " button.close").click(function() {
            $(theModal + " iframe").attr("src", videoSRC);
        });
    });
    $('#video').on('hidden.bs.modal', function (e) {
        $(this).find("iframe").attr("src", "");
    });
}

function blogVideoPlay() {
    $(".js-video-play").on("click", function() {
        
        var theModal = $(this).siblings("iframe"),
            videoSRC = $(this).attr("data-video"),
            videoSRCauto = videoSRC + "?autoplay=1&modestbranding=1&rel=0&controls=1&showinfo=0&html5=1";
            console.log(theModal);
        $(theModal).attr("src", videoSRCauto);
        $(this).parent(".bs-video").addClass("active");
        // $(theModal + " button.close").click(function() {
        //     $(theModal + " iframe").attr("src", videoSRC);
        // });
    });
}

$(function() {
    if ($(".js-addto").length != 0) {
        setTimeout(function() {
            imgToBg(".js-addto", ".js-img");
        }, 100);
    }
    if ($(".swiper-course").length != 0) {
        sliderCourse();
    }
    if ($(".bs-testimonials").length != 0) {
        sliderTestimonials();
    }
    if ($(".bs-advantage").length != 0) {
        showCase();
    }
    if ($(".js-home-banner").length != 0) {
        homeBannerSwiper();
    }
    if ($(".js-blog-banner").length != 0) {
        blogBannerSwiper();
    }
    if ($(".js-blog-swiper").length != 0) {
        blogSwiper();
    }
    if ($(".mobile").length != 0) {
        if ($(".bs-header").length != 0) {
            mobileMenuOpenClose();
        }
        if ($(".bs-footer").length != 0) {
            mobileFooter();
        }
        if ($(".bs-search-panel").length != 0) {
            openCloseSearch();
        }
        if ($(".bs-champion-band").length != 0) {
            mobileBanner();
        }
    }
    if ($(".js-select").length != 0) {
        $(".js-select").select2({
            minimumResultsForSearch: -1
        });
        $(".js-typfliter").on("select2:select", function(e) {
            var storeid = e.params.data.id;
            $(".js-filteritem").removeClass("active");
            if ($("[data-filter=" + storeid + "]").closest(".js-filteritem").hasClass("swiper-slide")) {
                courseSwiper.destroy();
                $("[data-filter=" + storeid + "]").closest(".js-filteritem").addClass("active");
                sliderCourse();
            } else {
                $("[data-filter=" + storeid + "]").closest(".js-filteritem").addClass("active");
            }
        });
    }
    if ($(".js-select-filter").length != 0) {
        $(".js-select-filter").select2({
            minimumResultsForSearch: -1
        });
    }
    if ($(".bs-tabs").length != 0) {
        tabSelection();
    }
    $("#tabSelect").select2({
        minimumResultsForSearch: -1
    });
    if ($(".lyt-plan").length != 0) {
        hoverActive();
    }
    if ($(".js-fliter").length != 0) {
        fliter();
    }
    if ($(".mobile").length != 0) {
        jsMove();
    }
    if ($(".mobile").length != 0) {
        mobMenu();
    }
    // a[href^="#"]
    $(document).on("click", '.jsScroll-trig', function(e) {
        e.preventDefault();
        var a = this.hash;
        if ($(this).hasClass("filter-link")) {
            $(".filter-link").closest("li").removeClass("active");
            $(this).closest("li").addClass("active");
        }
        if ($(".bs-filter").hasClass("fixed") == true) {
            $("html, body").stop().animate({
                scrollTop: $(a).offset().top - 155
            }, 1e3, "linear");
        } else {
            $("html, body").stop().animate({
                scrollTop: $(a).offset().top - 265
            }, 1e3, "linear");
        }
    });
    if ($(".lyt-testimonials").length != 0) {
        aboutTestimonialSwiper();
    }
    if ($(".lyt-partners").length != 0) {
        partnerSwiper();
    }
    if ($(".bs-banner").length != 0) {
        logoSlide();
    }
    if ($(".bs-video").length != 0) {
        blogVideoPlay();
    }
    if ($(".js-alumni").length != 0) {
        swiperAlumni();
    }
    formValidate();
    if ($('.js-video-trigger').length != 0) {
        videoModalIframe();
    }
});

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    console.log(scroll);
    if ($(".js-competition-filter").length != 0) {
        if ($(window).width() == 1024 && $(".desktop").length != 0) {
            if (scroll >= 500) {
                $(".jsFilter").addClass("fixed");
            } else {
                $(".jsFilter").removeClass("fixed");
            }
        } else if($(window).width() >= 1200 && $(".desktop").length != 0){
            if (scroll >= 670) {
                $(".jsFilter").addClass("fixed");
            } else {
                $(".jsFilter").removeClass("fixed");
            }
        }else {
            if (scroll > 1119) {
                $(".jsFilter").addClass("fixed");
            } else {
                $(".jsFilter").removeClass("fixed");
            }
        }
    } else if ($(window).width() == 1024 && $(".desktop").length != 0) {
        if (scroll >= 450) {
            $(".jsFilter").addClass("fixed");
        } else {
            $(".jsFilter").removeClass("fixed");
        }
    } else {
        if (scroll > 600) {
            $(".jsFilter").addClass("fixed");
        } else {
            $(".jsFilter").removeClass("fixed");
        }
    }
});

$(window).on("load", function() {
    if ($(".mobile").lenth != 0) {
        if ($(".swiper-course").length != 0) {}
    }
});