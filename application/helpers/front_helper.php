<?php
function css($uri, $tag=true) {
    if($tag) {
        $media=false;
        if(is_string($tag)) {
            $media = 'media="'.$tag.'"';
        }
        return '<link href="'.theme_url('css/'.$uri).'" type="text/css" rel="stylesheet" '.$media.'/>';
    }
    return '<link href="'.theme_url($uri).'" type="text/css" rel="stylesheet"/>';
    // return theme_url('assets/css/'.$uri);
}
function js($uri, $tag=true) { // $tag need js from js folder
    if($tag) {
        return '<script src="'.theme_url('js/'.$uri).'"></script>';
        // return '<link href="'.theme_url('js/'.$uri).'" type="text/css" rel="stylesheet" '.$media.'/>';
    }
    return '<script src="'.theme_url($uri).'"></script>';
    // return theme_url($uri);
}
function theme_url($uri=null) {
    $CI = & get_instance();

    if($uri == null) {
        $url = base_url().'theme/';
    } else {
        $url = base_url().'theme/'.$uri;
    }
    return $url;
    //exit;
}
function admin_js($uri, $tag=true) {
    if($tag) {
        return '<script src="'.admin_theme_url('dist/js/'.$uri).'"></script>';
        // return '<link href="'.theme_url('js/'.$uri).'" type="text/css" rel="stylesheet" '.$media.'/>';
    }
    return admin_theme_url('dist/js/'.$uri);
}
function admin_theme_url($uri=null) {
    if($uri == null) {
        $url = base_url().'theme/admin/';
    } else {
        $url = base_url().'theme/admin/'.$uri;
    }
    return $url;
    //exit;
}
function admin_css($uri, $tag=true) {
    if($tag) {
        $media=false;
        if(is_string($tag)) {
            $media = 'media="'.$tag.'"';
        }
        return '<link href="'.admin_theme_url('dist/css/'.$uri).'" type="text/css" rel="stylesheet" '.$media.'/>';
    }
    return admin_theme_url('dist/css/'.$uri);
}
function img($uri, $options=null) {
    $html = '';
    if($options) {
        foreach ($options as $key => $value) {
            $html[] = $key.'="'.$value.'"';
        }
        $html = implode(" ", $html);
    }
    return '<img src="'.theme_url('img/'.$uri).'" '.$html.'>';  
}
function admin_img($uri, $options=null) {
    $html = '';
    if($options) {
        foreach ($options as $key => $value) {
            $html[] = $key.'="'.$value.'"';
        }
        $html = implode(" ", $html);
    }
    return '<img src="'.admin_theme_url('admin/img/'.$uri).'" '.$html.'>';  
}
function checkUser() {
    $CI = & get_instance();
    return $CI->session->userdata('uid');
}
function upload_url($str) {
    $url = base_url('uploads/'.$str);
    return str_replace("/index.php", "", $url);
}
function list_image($file) {
    if(empty($file)) {
        $file = "default.jpg";
    }
    return upload_url('listings/original/'.$file);
}
function active_link($controller)
{
    $CI =& get_instance();
    
    $class = $CI->router->fetch_class();

    if($class=="welcome" && $controller=="home") {
        
        return 'active';
    }
    return ($class == $controller) ? 'active' : '';
}

function showSuccess($msg) {
    return '<div class="alert alert-success fade in" style="margin-top:18px;"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a> '.$msg.'</div>';
    // return '<p class="bg-msg bg-success"></p>';
}
function showError($msg) {
    return '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a> '.$msg.'</div>';
    // return '<p class="bg-msg bg-danger"></p>';
}
function admin_url($uri) {
    return site_url("admin/".$uri);
}


function timestamp() {
     date_default_timezone_set("Asia/Kolkata");     
    return date('Y-m-d H:i:s', time());
}

function _do_upload($file, $path) {
    $CI =& get_instance();
    
    $CI->load->helper('form');
    $data = $CI->input->post();
    $config['upload_path'] = './uploads/'.$path;
    $config['allowed_types'] = '*';
    $config['max_size']   = '30000';
    $config['encrypt_name'] = TRUE;
    /* $config['max_width']  = '1024';
      $config['max_height']  = '768'; */
    $CI->load->library('upload', $config);
    if (!$CI->upload->do_upload($file)) {
        $data['inputerror'][] = $file;
        $errormsg = 'Upload error: ' . $CI->upload->display_errors(); //show ajax error
        $data['status'] = FALSE;
        echo json_encode($errormsg);
        exit();
    } else {
        $upload = '';
        $upload = $CI->upload->data();         
        return $upload['file_name'];
    }
}

?>