<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

  require_once(APPPATH.'controllers/cart.php');
  function activate_menu($controller) {
    // Getting CI class instance.
    $CI = get_instance();
    // Getting router class to active.
    $class = $CI->router->fetch_class();
    
    return ($class == $controller) ? 'active' : '';
  }

  function get_expertise()
  {
     $CI = get_instance();
     $expertise_data =   $CI->master_model->getRecords('table_category',array(),'table_category.*',array('sort_order'=>'asc')); 
     return $expertise_data;
  }
  

  function check_login(){
    //echo 11111;exit;
    
     $CI = get_instance();
     $userData = $CI->session->userdata('user');

     if($userData==false){
        redirect(base_url());
     }

    }

 function check_login_checkout(){
    $CI = get_instance();
     $userData = $CI->session->userdata('user');

     if($userData==false){
        $CI->session->set_userdata('redirect_url', base_url()."shipping");
        redirect(base_url().'login');
     }
 }   


 function check_login_page($args){
    $CI = get_instance();
    $CI->load->library('user_agent');

     $userData = $CI->session->userdata('user');

     if($userData==false){
        if($args=="profile"){
          $CI->session->set_userdata('redirect_url', base_url()."profile");  
        }else if($args=="orders"){
          $CI->session->set_userdata('redirect_url', base_url()."orders"); 
        }else if($args=="wishlist"){
          $refer =  $CI->agent->referrer();
          $CI->session->set_userdata('redirect_url', base_url()."wishlist"); 
        }else if($args=="cart"){
          $CI->session->set_userdata('redirect_url', base_url()."cart"); 
        }
        
        redirect(base_url().'login');
     }
 }   

 function check_login_ajax($args){
  
    $CI = get_instance();
    $CI->load->library('user_agent');
    $userData = $CI->session->userdata('user');
    if($userData==false){
      if($args=="wishlist"){
            $refer =  $CI->agent->referrer();
            //print_r($refer);exit;
            $CI->session->set_userdata('redirect_url', $refer); 
            return true;
          }
      }
 }
    function check_is_login(){
     $CI = get_instance();
     $userData = $CI->session->userdata('user');
     //print_r($userData);exit;
     
      return $userData;
    }

    function get_cart_count(){
      
     $CI = get_instance();
     $cartData = $CI->session->userdata('cart');
    if($cartData){
      $cartCount = count($cartData); 
    }else{
      $cartCount = 0;
    }
     
     return $cartCount;
     //var_dump($cartData);exit;
    }


    function load_wishlist(){
       $CI = get_instance();
       $CI->load->helper('cookie');
       $userData = $CI->session->userdata('user');
       //print_r($userData);exit;
      if($userData){
        $data = $CI->master_model->getRow('table_wishlist', array('user_id'=>$userData['id']),  array('product_id') );  
        if(!empty($data)){
          set_cookie('wishlist_cookie',$data['product_id'],'3600');
          $stored_cookie = get_cookie('wishlist_cookie');

          if($data['product_id'] != $stored_cookie){
            redirect($CI->uri->uri_string());
          }
          //redirect($_SERVER['REQUEST_URI'], 'refresh'); 
        }
      }
      $redirect = $CI->session->userdata('redirect_url');

    }

    function load_cart(){
      
      $CI = get_instance();
      $userData = $CI->session->userdata('user');
      $current_cart_data = $CI->session->userdata('cart');
      //print_r($current_cart_data);exit;
      if($userData && empty($current_cart_data)){
        $data = $CI->master_model->getRow('table_cart', array('user_id'=>$userData['id']),  array('contents') );  
        //print_r($data);exit;
        if(!empty($data)){
          $cart_data_init = unserialize($data['contents']);
          $cart_data = $cart_data_init['cart'];
         /* echo "<pre>";
         print_r($cart_data);
         echo "--------";
         print_r($current_cart_data);exit;*/
         
          $CI->session->set_userdata($cart_data_init);

          if($current_cart_data!==$cart_data){
            redirect($CI->uri->uri_string());
          }
        }else{
          $cart_data = array();
          $CI->session->unset_userdata('cart');
          if($current_cart_data==false){ $current_cart_data = array(); }
          if($current_cart_data!==$cart_data){
            redirect($CI->uri->uri_string());
          }
        }
      }else if($userData && !empty($current_cart_data)){
        
        $data = $CI->master_model->getRow('table_cart', array('user_id'=>$userData['id']),  array('contents') );
        if(!empty($data)){
          $cart_data_init = unserialize($data['contents']);
          $cart_data = $cart_data_init['cart'];
          //print_r($current_cart_data);
          //print_r($cart_data);
          $new_cart_data = array_merge($current_cart_data,$cart_data);
          //print_r($new_cart_data);exit;
          $obj_cart = new Cart();
         
          $final_cart_data =  array("cart"=>$new_cart_data);
          $CI->session->set_userdata($final_cart_data);
          $obj_cart->add_cart_db($final_cart_data);
          if($new_cart_data!==$cart_data){
            redirect($CI->uri->uri_string());
          }
        }else{
          
           if(!empty($current_cart_data)){

            $obj_cart = new Cart();
            $current_cart_data =  array("cart"=>$current_cart_data);
            $obj_cart->add_cart_db($current_cart_data);
         }
        }

      }else{
       
         $current_cart_data = $CI->session->userdata('cart');

         if(!empty($current_cart_data)){

            $obj_cart = new Cart();
            $current_cart_data =  array("cart"=>$current_cart_data);
            $obj_cart->add_cart_db($current_cart_data);
         }
      }
    }

    function set_currency_initial(){
      
       $CI = get_instance();
       $currency = $CI->session->userdata('currency');

      if(!isset($currency) || $currency==""){
         $CI->session->set_userdata('currency', "INR");
      }
      
    }

    function set_geoCurrecny(){
      $ip = $_SERVER['REMOTE_ADDR']; 
      $CI = get_instance();
      $dataArray = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
      $currency = $CI->session->userdata('currency');
      $currency_flag = $CI->session->userdata('currency_flag');
      /*echo "<pre>";
      print_r($dataArray);exit;*/
      //echo "Hello visitor from: ".$dataArray->geoplugin_countryName;
      if(($dataArray->geoplugin_countryName=="India") && ($currency_flag!="manual")){
         $CI->session->set_userdata('currency', "INR");
         $CI->session->set_userdata('currency_flag', "auto");
      }else{
        $CI->session->set_userdata('currency', "USD");
        $CI->session->set_userdata('currency_flag', "auto");
      }
    }

  function get_footer_data()
  {
     $CI = get_instance();
       $master =  $CI->master_model->getRow('master', '',  array('address', 'contact', 'toll_free', 'copy_right') );       
     return $master;
  }
  function get_youtube_video_ID($youtube_video_url) {
          $url_string = parse_url($youtube_video_url, PHP_URL_QUERY);
          parse_str($url_string, $args);
          //return isset($args['v']) ? $args['v'] : false;
          return isset($args['v']) ? $args['v'] : '';
  }    

  function set_referer(){
    $CI = get_instance();
    $refer =  base_url($CI->uri->uri_string());
    $CI->session->set_userdata('redirect_url', $refer);
    /*$CI->load->library('user_agent');
    if ($CI->agent->is_referral())
    {
      echo 1111111;
       $refer =  $CI->agent->referrer();
       $CI->session->set_userdata('redirect_url', $refer);
       $redirect = $CI->session->userdata('redirect_url');
       var_dump($redirect);exit;
    }*/
  }
?>
