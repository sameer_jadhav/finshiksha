<footer>
        <!-- footer start -->
        <div class="bs-footer">
    <div class="logo visible-xs">
        <a href="#">
            <img src="assets/images/logo.png" alt="logo">
        </a>
    </div>
    <div class="top-band">
        <ul class="lvl1">
            <li>
                <div class="quick-link">
                    <h4 class="title js-pointer">Courses</h4>
                    <div class="link-wrap">
                        <ul>
                            <li>
                                <a href="#">Motilal Oswal GradPro</a>
                            </li>
                            <li>
                                <a href="#">Art of Stock Picking</a>
                            </li>
                            <li>
                                <a href="#">Applied Economics</a>
                            </li>
                            <li>
                                <a href="#">Financial Modelling</a>
                            </li>
                            <li>
                                <a href="#">Banking & Markets</a>
                            </li>
                            <li>
                                <a href="#">Financial Statement Analysis</a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a href="#">Investment Banking</a>
                            </li>
                            <li>
                                <a href="#">Equity Valuation</a>
                            </li>
                            <li>
                                <a href="#">Credit Analysis</a>
                            </li>
                            <li>
                                <a href="#">Applied Derivatives</a>
                            </li>
                            <li>
                                <a href="#">Analyst Program</a>
                            </li>
                            <li>
                                <a href="#">Finance Bootcamp</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <div class="quick-link">
                    <h4 class="title js-pointer">Combo Courses</h4>
                    <div class="link-wrap">
                        <ul>
                            <li>
                                <a href="#">Investment Banking + Credit Analysis</a>
                            </li>
                            <li>
                                <a href="#">Equity Valuation + Investment Banking</a>
                            </li>
                            <li>
                                <a href="#">Equity Valuation + Investment Banking + Credit Analysis</a>
                            </li>
                            <li>
                                <a href="#">Equity Valuation + Credit Analysis</a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a href="#">Financial Statement Analysis + Banking & Market</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="visible-xs">
                <div class="quick-link">
                    <h4 class="title js-pointer">Finshiksha</h4>
                    <div class="link-wrap">
                        <ul>
                            <li>
                                <a href="#">Explore all Courses</a>
                            </li>
                            <li>
                                <a href="#">Webinars</a>
                            </li>
                            <li>
                                <a href="#">Competition</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="visible-xs">
                <div class="quick-link">
                    <h4 class="title js-pointer">Support</h4>
                    <div class="link-wrap">
                        <ul>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact</a>
                            </li>
                            <li>
                                <a href="#">Blog</a>
                            </li>
                            <li>
                                <a href="#">Policies</a>
                            </li>
                            <li>
                                <a href="#">Terms & conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="bottom-band">
        <ul>
            <li>
                <div class="logo hidden-xs">
                    <a href="#">
                        <img src="assets/images/logo.png" alt="logo">
                    </a>
                </div>
                <div class="social-link">
                    <ul>
                        <li>
                            <a href="#">
                                <span class="icon icon-fb"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon icon-tw"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon icon-yt"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon icon-lin"></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <label class="copyright">Copyright © 2018 - FinShiksha</label>
            </li>
            <li>
                <a class="contact" href="tel:919137314913">(+91) 91373 14913</a>
                <a class="contact" href="mailto:programs@finshiksha.com">programs@finshiksha.com</a>
            </li>
            <li class="hidden-xs">
                <div class="quick-link">
                    <h4 class="title">Finshiksha</h4>
                    <div class="link-wrap">
                        <ul>
                            <li>
                                <a href="#">Explore all Courses</a>
                            </li>
                            <li>
                                <a href="#">Webinars</a>
                            </li>
                            <li>
                                <a href="#">Competition</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="hidden-xs">
                <div class="quick-link">
                    <h4 class="title">Support</h4>
                    <div class="link-wrap">
                        <ul>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Contact</a>
                            </li>
                            <li>
                                <a href="#">Blog</a>
                            </li>
                            <li>
                                <a href="#">Faq</a>
                            </li>
                            <li>
                                <a href="#">Policies</a>
                            </li>
                            <li>
                                <a href="#">Terms & conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="bs-floating-menu">
    <ul class="list">
        <li class="item">
            <a href="#">
                <i class="icon icon-chatbox"></i>
                <span class="text">Enquire</span>
            </a>
        </li>
        <li class="item">
            <a href="#">
                <i class="icon icon-mail"></i>
                <span class="text">Email</span>
            </a>
        </li>
        <li class="item">
            <a href="#">
                <i class="icon icon-question"></i>
                <span class="text">FAQ</span>
            </a>
        </li>
    </ul>
</div>
        <!-- footer end -->
    </footer>