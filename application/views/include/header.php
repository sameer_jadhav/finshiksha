<?php 
$user_sess = check_is_login(); 
 if($user_sess==false){
    $login_active = "";
 }else{
    $login_active = "withlogin";
 }
  ?>


<header>
        <div class="bs-header <?php echo $login_active;?>">
            <!-- header start -->
            <a class="btn-menu js-menu-btn" href="#">
    <span></span>
    <span></span>
    <span></span>
</a>
<a class="logo" href="home.html">
    <img src="./assets/images/logo.png" alt="FinShiksha Logo">
</a>
<div class="navigation-bar">
    <div class="nav-wrap js-nav">
        <ul>
            <li class="active">
                <a href="<?php echo base_url();?>" class="nav-link">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>course-listing" class="nav-link">Courses</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>webinar-listing" class="nav-link">Webinar</a>
            </li>
            <li class="js-mobMenu">
                <a href="#" class="nav-link with-icon">Company</a>
                <div class="lvl2">
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>aboutus" class="nav-link">Abouts Us</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>competition" class="nav-link">Competition</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>corporate" class="nav-link">Corporate</a>
                        </li>
                        <li>
                            <a href="#" class="nav-link">Job</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="<?php echo base_url();?>blog-listing" class="nav-link">Blog</a>
            </li>
            <li>
                <?php 
                $cart_url = "#";
                if($user_sess){ 
                    $cart_url = base_url()."cart";
                }
                    ?>
                <a href="<?Php echo $cart_url;?>" id="cart_count">0</a>
            </li>
            <li class="visible-xs d">
                <a href="#" data-toggle="modal" data-target="#login" class="nav-link">Login / register</a>
            </li>
        </ul>
        <a href="#" class="btn-close visible-xs js-nav-close">
            <span class="icon icon-close"></span>
        </a>
    </div>
    <div class="side-nav">
        <button type="button" data-toggle="modal" data-target="#login" class="loginBtn btn btn-default">Login / register</button>
        <!-- <button type="button" data-toggle="modal" data-target="#login" class="btn btn-user visible-xs"><img src="assets/images/user.png"></button> -->
    </div>
    <?php if($user_sess){ ?>
    <div class="loginName">
        <span class="login-name">
            <div class="hidden-xs">
                <span>Hi,</span>
                <span><?php echo ucwords($user_sess['first_name']." ".$user_sess['last_name']);?></span>
                
            </div>
             
            <div class="visible-xs">
                <div class="profile-img">
                    <img src="<?php echo base_url();?>assets/images/user.png" alt="user">
                </div>
            </div>
        </span>
        <div class="profile-menu">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>dashboard"><i class="icon icon-dashboard"></i>My Dashboard</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>mycourses"><i class="icon icon-school"></i>My Courses</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>mywebinar"><i class="icon icon-webinar"></i>My Webinar</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>user/logout"><i class="icon icon-logout"></i>Sign Out</a>
                </li>
            </ul>
        </div>
    </div>
    <?php } ?>
</div>
            <!-- header end -->
        </div>
    </header>