<script type="text/javascript">
    
$("#signupForm").validate({
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            mob: {
                required: true,
                minlength: 10
            },
            pass: {
                required: true,
                minlength: 8
            },
            confirm_pass: {
                required: true,
                minlength: 8,
                equalTo: "#pass"
            },
            emailid: {
                required: true,
                email: true
            }
        },
        messages: {
            name: {
                required: "Please enter Name",
                minlength: "Your name more than 3 characters"
            },
            mob: {
                required: "Please Enter Mobile Number",
                minlength: "Please Enter 10 Digit Mobile Number"
            },
            pass: {
                required: "Please provide a password",
                minlength: "More than 8 characters long"
            },
            confirm_pass: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            emailid: {
                required: "Please provide Email ID"
            }
        },
        submitHandler: function(form) {
        $.ajax({
            url: "<?php echo site_url("user/register") ?>", 
            type: "POST",             
            data: $('#signupForm').serialize(),    
            dataType: "json",      
            success: function(data) {
                if(data.status=="error"){
                    $("#reg-error").text(data.message)
                }else{
                    $(".registerWrap").removeClass("active");
                    $(".otpWrap").addClass("active");
                    $("#reg_email").val(data.email_id);
                }
            }
        });
        return false;
        },
    });

    $("#otpVal").validate({
        rules: {
            otp_val: {
                required: true,
                
            }
        },
        messages: {
            otp_val: {
                required: "Please provide OTP"
            }
        },
        submitHandler: function(form) {
        $.ajax({
            url: "<?php echo site_url("user/submit_otp") ?>", 
            type: "POST",             
            data: $('#otpVal').serialize(),    
            dataType: "json",      
            success: function(data) {
                console.log(data)
                //if(data.status=="error"){
                   $("#otpre-error").html(); 
                   $("#otpre-error").html(data.message) 
                //}
                if(data.status=="success"){
                    setTimeout(function() { 
                        $('#login').modal('hide');
                    }, 6000);
                    

                }
            }
        });
        return false;
        },
    });

    function resend_otp(){
        $.ajax({
            url: "<?php echo site_url("user/resend_otp") ?>", 
            type: "POST",             
            data: $('#otpVal').serialize(),    
            dataType: "json",      
            success: function(data) {
                //alert(1)
                $("#otpre-error").html(data.message)
            }
        });
    }

    function gotologin(){

        $("#logintab").addClass("active");
        $(".otpWrap").removeClass("active");
        //$("#logintab").tab("show")
    }

    $("#loginForm").validate({
        rules: {
            mob: {
                required: true,
                minlength: 10
            },
            pass: {
                required: true,
                minlength: 8
            },
            emailid: {
                required: true,
                email: true
            }
        },
        messages: {
            mob: {
                required: "Please Enter Mobile Number",
                minlength: "Please Enter 10 Digit Mobile Number"
            },
            pass: {
                required: "Please provide a password",
                minlength: "More than 8 characters long"
            },
            emailid: {
                required: "Please provide Email ID"
            }
        },
        submitHandler: function(form) {
        $.ajax({
            url: "<?php echo site_url("user/login") ?>", 
            type: "POST",             
            data: $('#loginForm').serialize(),    
            dataType: "json",      
            success: function(data) {
                console.log(data)
                //if(data.status=="error"){
                    if(data.status=="success"){
                        window.location.href= "<?php echo base_url();?>"
                    }else{
                        $("#login-error").html(); 
                        $("#login-error").html(data.message)      
                    }
                   
                //}
            }
        });
        return false;
        },
    });

    function buyCourse(courseId,subType){
        $.ajax({
            url: "<?php echo site_url("user/buycourse") ?>", 
            type: "POST",             
            data: { courseId : courseId ,subType: subType},  
            dataType: "json",      
            success: function(data) {
                console.log(data)
                //if(data.status=="error"){
                    if(data.status=='login-error'){
                        $('#login').modal('show');
                    }else{
                        if(data.status=="success"){
                             $("#cart_count").text(data.data)
                        }else{
                            $("#login-error").html(); 
                            $("#login-error").html(data.message)      
                        }    
                    }
                    
                   
                //}
            }
        });
    }

    function getcart(){
        $.ajax({
            url: "<?php echo site_url("user/getcart") ?>", 
            type: "GET",             
             dataType: "json",
            success: function(data) {
                console.log(data.status)
                //if(data.status=="error"){
                    if(data.status=='login-error'){
                        //$('#login').modal('show');
                    }else{
                        if(data.status=="success"){
                            $("#cart_count").text(data.data.length)
                        }  
                    }
                    
                   
                //}
            }
        });
    }

    getcart();
</script>