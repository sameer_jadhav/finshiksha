<!-- popup start -->
    <div class="bs-modal modal fade" id="faq" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon icon-close"></span></button>
            <div class="modal-header">
                <h4 class="modal-title">How to solve payment Issues</h4>
            </div>
            <div class="modal-body">
                <div class="bs-accordian">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Will I receive the recording after the live webinar is over? <span class="icon icon-plus"></span></a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    We host more than 150,000 courses on our online learning marketplace. Our marketplace model means we do not own the copyright to the content of the courses; the respective instructors own these rights. On rare occasions, instructors may remove their courses
                                    from our marketplace, or, Udemy may have to remove a course from the platform for policy or legal reasons. If this does happen to a course you're enrolled in, please contact us and we'll be ready to help
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Where can I find the recording of the live webinar?<span class="icon icon-plus"></span></a>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                    on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Will I receive the recording after the live webinar is over?<span class="icon icon-plus"></span></a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                    on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Will I receive the recording after the live webinar is over?<span class="icon icon-plus"></span></a>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                    on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link with-line">View all</button>
            </div>
        </div>
    </div>
</div>


<div class="bs-modal modal fade" id="video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close out-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon icon-close"></span></button>
            <div class="modal-body">
                <iframe width="100%" height="450" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>



<div class="bs-modal modal fade js-login-popup" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm typ-login" role="document">
        <div class="modal-content">
            <button type="button" class="close out-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon icon-close"></span></button>
            <div class="modal-body">
                <div class="bs-tabs typ2">
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#logintab" aria-controls="logintab" role="tab" data-toggle="tab">Login</a></li>
                      <li role="presentation"><a href="#signuptab" aria-controls="signuptab" role="tab" data-toggle="tab">Register</a></li>
                    </ul>
                  
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="logintab">
                            <div class="bs-login">
                            <div class="loginWrap">
                                <form id="loginForm" method="post" action="">
                                    <div class="form-list">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="emailid" placeholder="Enter Email ID">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="pass" placeholder="Password">
                                            <a href="<?php echo $this->config->item('api_url');?>user/password" class="rel-info">Forgot Password</a>
                                        </div>
                                        <h3 id="login-error" class="subtitle" ></h3>
                                        <div class="act-btn">
                                            <button type="submit" class="btn btn-default btn-with-icon">Submit <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="signuptab">
                            <div class="bs-login">
                                <div class="registerWrap active">
                                    <form id="signupForm" method="post" action="">
                                        <div class="form-list">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" placeholder="Enter Full Name">
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="mob" placeholder="Moile Number">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="emailid" placeholder="Enter Email ID">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" id="pass" class="form-control" name="pass" placeholder="Enter Password">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="confirm_pass" placeholder="Confirm Password">
                                            </div>
                                            <h3 id="reg-error" class="subtitle" ></h3>
                                            <div class="act-btn">
                                                <button type="submit" class="btn btn-default btn-with-icon">Join <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <div class="otpWrap">
                                    <form id="otpVal" method="post" action="">
                                        <h3 class="subtitle">Enter Verification code from Email</h3>
                                        <div class="form-list">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="otp_val" placeholder="Enter OTP">
                                                <a href="#" onclick="resend_otp()" class="rel-info">Resend Verification Code</a>
                                            </div>
                                            <h3 id="otpre-error" class="subtitle" ></h3>
                                            <input type="hidden" class="form-control" name="reg_email" id="reg_email" value="" >
                                            <div class="act-btn">
                                                <button type="submit" class="btn btn-default btn-with-icon">Submit <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="login-social">
                    <label class="text">Join with</label>
                    <div class="icons">
                        <a href="#" class="img img-fb">
                            <img src="assets/images/img-fb.svg" alt="fb">
                        </a>
                        <a href="#" class="img img-gmail">
                            <img src="assets/images/img-gmail.svg" alt="gmail">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- popup end -->