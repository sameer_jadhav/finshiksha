<script type="text/javascript">
	
	function add_wishlist(product_id){

            //$('#btnWishlist'+product_id).text('processing'); //change button text
            $('#btnWishlist'+product_id).addClass("btn-load");
            $('#btnWishlist'+product_id).attr('disabled', true); //set button enable 

            $.ajax({
                        url: '<?php echo site_url('wishlist/add/') ?>/'+product_id,
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.status) //if success close modal and reload ajax table
                            {   
                                //$('#newAddress').modal('hide'); // show bootstrap modal                                
                                //alert('success');
                                if(data.redirect==true){
                                    window.location.href= "<?php echo base_url();?>login";
                                    return false;
                                }
                                 $("#toaster-text").text("Item added to your Wishlist");
                                 $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                                
                            } else
                            {
                                alert('error');
                            }
                            $('#btnWishlist'+product_id).addClass("active");
                            $('#btnWishlist'+product_id).find('span').addClass("active");
                            $('#btnWishlistmod'+product_id).addClass("active");
                            $('#btnWishlistmodad'+product_id).addClass("active");
                            $('#btnWishlistcat'+product_id).find('span').addClass("active");
                            //alert(1)
                            $('#btnWishlist'+product_id).attr("onclick","remove_wishlist_modal("+product_id+")")
                             $('#btnWishlistcat'+product_id).attr("onclick","remove_wishlist_modal("+product_id+")")
                              $('#btnWishlistmod'+product_id).attr("onclick","remove_wishlist_modal("+product_id+")")
                            $('#btnRemoveWishlist'+product_id).attr("onclick","remove_wishlist_modal("+product_id+")")
                            $('#btnRemoveWishlistcat'+product_id).attr("onclick","remove_wishlist_modal("+product_id+")")
                            $('#btnRemoveWishlistmod'+product_id).attr("onclick","remove_wishlist_modal("+product_id+")")

                            //$('#btnWishlist'+product_id).text('added to wishlist'); //change button text
                            $('#btnWishlist'+product_id).attr('disabled', false); //set button enable 


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            $('#btnWishlist'+product_id).removeClass("btn-load");
                            $('#btnWishlist'+product_id).text('add to wishlist'); //change button text
                            $('#btnWishlist'+product_id).attr('disabled', false); //set button enable 

                        }
                    });
        }
/////set product variable price

 $(function(){
  $('input[type="radio"][name=weight_class]').click(function(){
  	//alert(1)
    if ($(this).is(':checked'))
    {
      var data = $(this).val();
      var data = data.split(";");
      //alert(data);
      $("#product_price").text("Rs. "+data[0])
      $("#product_weight").text("/"+data[1]+" gms")

    }
  });
});

$(document).on("click", 'input[type="radio"][name=weight_class]', function(event) { 
    //alert(1)
     if ($(this).is(':checked'))
    {
      var data = $(this).val();
      var data = data.split(";");
      var variation_id = $(this).attr("data-variation-id");
      //alert(variation_id);
      $("#product_price").text("Rs. "+data[0])
      $("#product_weight").text("/"+data[1]+" gms")
      $("#cart_summary_btn").attr("data-var-id",variation_id);
      $("#add_to_cart").attr("data-var-id",variation_id);
      //$("#add_to_cart").attr("data-var-id",variation_id);
    }
});


$(document).on("change", '#product_qty_dd', function(event) { 
    var data = $(this).val();
    var data = data.split(";");
    var variation_id = data[2];
    var product_id = data[3];
    $("#amt_val_cat"+product_id).text("Rs. "+data[0])
    $("#amt_qty_cat"+product_id).text(data[1]+" gm")
    $("#add_to_cart"+product_id).attr("data-var-id",variation_id);

    $("#amt_val_trend"+product_id).text("Rs. "+data[0])
    $("#amt_qty_trend"+product_id).text(data[1]+" gm")
    $("#add_to_cart_trend"+product_id).attr("data-var-id",variation_id);
});


$(document).on("change", '#product_qty_dd_feat', function(event) { 
    var data = $(this).val();
    var data = data.split(";");
    var variation_id = data[2];
    var product_id = data[3];
   

    $("#amt_val_trend"+product_id).text("Rs. "+data[0])
    $("#amt_qty_trend"+product_id).text(data[1]+" gm")
    $("#add_to_cart_trend"+product_id).attr("data-var-id",variation_id);
});




function update_cart_item(product_id,variation_id,quantity,cart_id){
    $.ajax({
                    url: '<?php echo site_url("cart/add_to_cart") ?>',
                    type: "POST",
                    data: { id : product_id ,quantity: quantity,cart_id:cart_id,variation_id : variation_id,flag:"var_update"},
                    dataType: "JSON",
                    success: function (data)
                    {

                        if (data.status=="error") //if success close modal and reload ajax table
                        {
                            alert("Something went wrong!! Try again later");
                        } else
                        {                    
                              refresh_cart();
                        }
                        

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
}
function set_variable_product(dataval){
	var data = dataval;
    var data = data.split(";");
    $("#product_price").text("Rs. "+data[0])
    $("#product_weight").text("/"+data[1]+" gms")
} 

function product_summary(product_id){
	//alert(product_id);
 $.ajax({url: "<?php echo base_url();?>product/summary/"+product_id, success: function(result){
            //alert("success"+result);
              $("#product_summary_body").html(result);
              $("#productModal").modal('show'); 
              productViewSwiper();
              $("#productModal").on('shown.bs.modal', function() {
                galleryTop.update();
                galleryThumbs.update();
            });
              $('.bs-custom-select.typ-box select').select2({
                minimumResultsForSearch: -1,
            })
        }});
}

$("#productModal").on("hidden.bs.modal", function () {
   // alert(1)
    $("#product_summary_body").html("");
});

function remove_wishlist_modal(product_id) {
            //$('#btnRemoveWishlist'+product_id).text('processing'); //change button text
            $('#btnRemoveWishlist'+product_id).addClass("btn-load");
            $('#btnRemoveWishlist'+product_id).attr('disabled', true); //set button enable 
            $('#confirm_remove').modal('show');
            $("#proceed_btn").attr("data-product-id",product_id)
            $('#cancel_btn').on('click', function(e) {
                   $('#btnRemoveWishlist'+product_id).text('Remove'); //change button text
                   $('#btnRemoveWishlist'+product_id).attr('disabled', false); //set button enable
                   $('#confirm_remove').modal('hide');
                    return false; 
                })

      } 

       $('#proceed_btn').on('click', function() {
        var product_id = $("#proceed_btn").attr("data-product-id")
                    $.ajax({
                        url: '<?php echo site_url('wishlist/remove/') ?>/'+product_id,
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.status) //if success close modal and reload ajax table
                            {   
                                //$('#newAddress').modal('hide'); // show bootstrap modal                                
                                //alert(product_id);
                                $('#confirm_remove').modal('hide');
                                $("#toaster-text").text("Item removed from your Wishlist");
                                 $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                                 //window.location.replace('<?php echo base_url() ?>wishlist');
                                 $("#wishproductlist"+product_id).hide()
                                
                            } else
                            {
                                alert('error');
                            }
                            $('#btnRemoveWishlist'+product_id).find('span').removeClass("active");
                            $('#btnWishlist'+product_id).find('span').removeClass("active");
                            $('#btnWishlistcat'+product_id).find('span').removeClass("active");
                            $('#btnRemoveWishlistmodad'+product_id).removeClass("active");
                            $('#btnRemoveWishlistmod'+product_id).removeClass("active");
                            
                            $('#btnWishlist'+product_id).attr("onclick","add_wishlist("+product_id+")")
                            $('#btnWishlistcat'+product_id).attr("onclick","add_wishlist("+product_id+")")
                            $('#btnWishlistmod'+product_id).attr("onclick","add_wishlist("+product_id+")")
                            $('#btnRemoveWishlist'+product_id).attr("onclick","add_wishlist("+product_id+")")
                            $('#btnRemoveWishlistcat'+product_id).attr("onclick","add_wishlist("+product_id+")")
                            $('#btnRemoveWishlistmod'+product_id).attr("onclick","add_wishlist("+product_id+")")
                            $('#btnRemoveWishlist'+product_id).attr('disabled', false); //set button enable
                            /*
                            $('#btnRemoveWishlist'+product_id).text('Removed'); //change button text
                            $('#btnRemoveWishlist'+product_id).attr('disabled', true); //set button enable */


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            $('#btnRemoveWishlist'+product_id).removeClass("btn-load");
                            $('#btnRemoveWishlist'+product_id).text('Remove'); //change button text
                            $('#btnRemoveWishlist'+product_id).attr('disabled', false); //set button enable 

                        }
                    });
                
       })


function add_to_cart(product){
    //alert(123)
        var product_id = $(product).attr("data-product-id");
        var var_id = $(product).attr("data-var-id");
        
        var quantity = $("#count_summary").val();
        if(quantity==undefined){
            quantity = 1;
        }
        //alert(quantity);
        //$(product).text("adding...");
        $(product).addClass("btn-load");
        $(product).attr('disabled', true);
        $.ajax({
                    url: '<?php echo site_url("cart/add_to_cart") ?>',
                    type: "POST",
                    data: { id : product_id ,quantity: quantity,variation_id : var_id},
                    dataType: "JSON",
                    success: function (data)
                    {

                        if (data.status=="error") //if success close modal and reload ajax table
                        {
                            alert("Something went wrong!! Try again later");
                        } else
                        {                    
                             //alert(data.message);
                                //$("#toaster-text").text(data.message);
                                    if($(".cart-count").hasClass("hide")){
                                        $(".cart-count").removeClass("hide");
                                        $(".cart-count").addClass("add");
                                    }
                                    $("#toaster-text-cart").text("Item added to cart")
                                    $("#wishproductlist"+product_id).hide()
                                    $("#cart_toast").addClass("active");
                                setTimeout(function(){
                                    $("#cart_toast").removeClass("active");
                                }, 5000);
                                $.ajax({
                                    url: '<?php echo site_url('wishlist/remove/') ?>/'+product_id,
                                    dataType: "JSON"
                                })
                              $(".cart-count").text(data.cart_count)
                              refresh_cart();
                              refresh_cart_page();
                        }
                        $(product).removeClass("btn-load");
                        $(product).attr('disabled', false); 

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
    }


    function refresh_cart(){

        $.ajax({
                    url: '<?php echo site_url("cart/refresh_cart") ?>',
                    type: "get",
                    
                    success: function (data)
                    {

                        if (data.status=="error") //if success close modal and reload ajax table
                        {
                            alert("Something went wrong!! Try again later");
                        } else
                        {        
                            $('.bs-custom-select.typ-box select').select2({
                                minimumResultsForSearch: -1,
                            })  

                            $(".bs-cart-fly").html(data);
                            disableplusminus() 
                            ///need to add loader here
                            $(".bs-cart-fly").addClass("active");
                            setTimeout(function(){
                                    $(".bs-cart-fly").removeClass("active");
                                }, 2000);
                        }
                        

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });

    }

    function refresh_cart_page(){

        $.ajax({
                    url: '<?php echo site_url("cart/refresh_cart_page") ?>',
                    type: "get",
                    
                    success: function (data)
                    {

                        if (data.status=="error") //if success close modal and reload ajax table
                        {
                            alert("Something went wrong!! Try again later");
                        } else
                        { 
                            $(".cart-list").html(data)
                            $(".bs-pd-fly").removeClass("open")
                            $(".cm-overlay ").removeClass("active");
                            $.ajax({
                                url: '<?php echo site_url("cart/cart_sidebar") ?>',
                                type: "get",
                                success: function (datacalc)
                                {
                                    $(".cart_sidebar").html(datacalc)
                                }
                            });
                        }
                        

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });

    

    }

    $(document).on('click','#close_cart_fly',function(){
        // alert(1)
        $(".bs-cart-fly").removeClass("open");
        $(".cm-overlay").removeClass("active");
    })
    
     function remove_cart_item_modal(cart_item_id) {
            $('#confirm_remove_cart_item').modal('show');
            $("#proceed_btn_cart_item").attr("data-cart-id",cart_item_id)
            $('#cancel_btn_cart_item').on('click', function(e) {
                   $('#confirm_remove_cart_item').modal('hide');
                    return false; 
                })

      }

      $("#proceed_btn_cart_item").on("click",function(){

            var cart_item_id = $("#proceed_btn_cart_item").attr("data-cart-id");
            //alert(cart_item_id)
            $.ajax({
                    url: '<?php echo site_url("cart/remove_from_cart") ?>',
                    type: "POST",
                    data: { id : cart_item_id },
                    dataType: "JSON",
                    success: function (data)
                    {   
                        
                        //var cart_count = Object.keys(data).length;
                       
                        if (data.total==0) //if success close modal and reload ajax table
                        {
                             window.location.replace('<?php echo base_url() ?>cart');
                            
                        } else 
                        {        
                            //console.log(data);            
                            $("#cart_item_"+cart_item_id).hide();
                            $(".cart-count").text(data.cart_count)
                            var current_currency = "<?php echo $this->session->userdata('currency');?>"
                            //alert(current_currency);
                            var subtotal = data.cart_total.subtotal;
                            var subtotal_tax = data.cart_total.subtotal_tax;
                            var shipping_charge = data.cart_total.shipping_total;
                            var total = data.cart_total.total;
                            //$("#item_total").text( subtotal - data.cart_total.subtotal_tax);
                            var amt_wo_tax = Math.ceil(subtotal/1.18);
                            $("#cart_subtotal").text(amt_wo_tax);    
                             var cart_tax = subtotal - amt_wo_tax;
                            $("#cart_taxes").text(cart_tax);
                            $("#cart_total").text(total)
                            
                            

                            var current_cart_subtotal =  $("#cart_total").text();
                            var current_shipping_charge = $("#cart_shipping").text();
                            $("#cart_total").text(Number(current_shipping_charge) + Number(current_cart_subtotal))
                            $("#toaster-text-cart").text("Item removed from cart")
                           
                            $('#confirm_remove_cart_item').modal('hide');
                                $("#cart_toast").addClass("active");
                                setTimeout(function(){
                                    $("#cart_toast").removeClass("active");
                                }, 5000);
                            //$("#cart_shipping").text(shipping_charge);
                            //$("#cart_total").text(total);
        
                        }
                        

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
        
      })



      function remove_cart_item(cart_item_id){

            $.ajax({
                    url: '<?php echo site_url("cart/remove_from_cart") ?>',
                    type: "POST",
                    data: { id : cart_item_id },
                    dataType: "JSON",
                    success: function (data)
                    {   
                        
                        //var cart_count = Object.keys(data).length;
                       
                        if (data.total==0) //if success close modal and reload ajax table
                        {
                             //window.location.replace('<?php //echo base_url() ?>cart');
                             refresh_cart();
                             
                            
                        } else 
                        {        
                            //console.log(data);            
                            $("#cart_item_"+cart_item_id).hide();
                            $("#cart_item_fly_"+cart_item_id).hide();
                            
                            $(".cart-count").text(data.cart_count)
                            var current_currency = "<?php echo $this->session->userdata('currency');?>"
                            //alert(current_currency);
                            var subtotal = data.cart_total.subtotal;
                            var subtotal_tax = data.cart_total.subtotal_tax;
                            var shipping_charge = data.cart_total.shipping_total;
                            var total = data.cart_total.total;
                            //$("#item_total").text( subtotal - data.cart_total.subtotal_tax);
                            if(current_currency == 'INR'){
                                $("#cart_taxes").text((subtotal*18)/100);    
                                var order_sum_tax = (subtotal*18)/100;

                            }else if(current_currency == 'USD'){
                                $("#cart_taxes").text(subtotal_tax);
                                var order_sum_tax = subtotal_tax;
                            }
                            $("#item_total").text((subtotal - order_sum_tax).toFixed(2));
                            $("#cart_subtotal").text( Number(subtotal) + Number(subtotal_tax));
                            $("#cart_fly_subtotal").text("Rs."+subtotal);
                            var current_cart_subtotal =  $("#cart_subtotal").text();
                            var current_shipping_charge = $("#cart_shipping").text();
                            $("#cart_total").text(Number(current_shipping_charge) + Number(current_cart_subtotal))
                            $("#toaster-text-cart").text("Item removed from Cart");
                                $("#cart_toast").addClass("active");
                                setTimeout(function(){
                                    $("#cart_toast").removeClass("active");
                                }, 5000);
                            //$("#cart_shipping").text(shipping_charge);
                            //$("#cart_total").text(total);
        
                        }
                        

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
        }


        function update_to_cart(obj){
            var maxVal = $(obj).parent().find('.count').attr("max");
            //alert(maxVal)
            var quantity = parseInt($(obj).parent().find(".count").val());
            console.log(maxVal,quantity );

            if ($(obj).hasClass('js-plus')) {
                quantity = quantity + 1;

                if (quantity >= 1 && quantity <= maxVal) {
                    $(obj).parent().find(".count").val(quantity);
                }
            }
            else if ($(obj).hasClass('js-minus')) {
                quantity = quantity - 1;
                if (quantity >= 1) {
                    $(obj).parent().find(".count").val(quantity);

                }
            }

            if (quantity == 1) {
                $(obj).parent().find(".js-minus").attr('disabled', 'disabled').addClass('disabled');
                $(obj).parent().find(".js-plus").removeAttr('disabled').removeClass('disabled');
            }
            else if (quantity == maxVal) {
                $(obj).parent().find(".js-plus").attr('disabled', 'disabled').addClass('disabled');
                $(obj).parent().find(".js-minus").removeAttr('disabled').removeClass('disabled');;
            }
            else if (quantity > 1 && quantity <= maxVal) {
                $(obj).parent().find(".js-plus").removeAttr('disabled').removeClass('disabled');
                $(obj).parent().find(".js-minus").removeAttr('disabled').removeClass('disabled');;
            }
            var product_id = $(obj).attr("data-product-id");
            var variation_id = $(obj).attr("data-var-id");
            var quant = $(obj).parent().find(".js-count").val();
        //alert(quant);return false;
        // $(product).text("adding to bag...");
        // $(product).attr('disabled', true);
            $.ajax({
                url: '<?php echo site_url("cart/add_to_cart") ?>',
                type: "POST",
                data: { id : product_id ,quantity: quant,flag:"update",variation_id : variation_id},
                dataType: "JSON",
                success: function (data)
                {

                    if (data.status=="error") //if success close modal and reload ajax table
                    {
                        alert("Something went wrong!! Try again later");
                    } else
                    {        
                        var cart_calc = data.cart_calculation
                        //console.log(cart_calc);
                        var current_currency = "<?php echo $this->session->userdata('currency');?>"
                        //alert(cart_calc.subtotal)
                        //$("#line_item_total_"+product_id).text((data.line_item_total).toFixed(2))
                        $("#cart_subtotal").text(Math.ceil(cart_calc.subtotal/1.18));    
                        var amt_wo_tax = Math.ceil(cart_calc.subtotal/1.18);
                        var cart_tax = cart_calc.subtotal - amt_wo_tax;
                        $("#cart_taxes").text(cart_tax);
                        $("#cart_total").text(cart_calc.total)
                        $("#cart_fly_subtotal").text(cart_calc.subtotal)
                        
                        var current_cart_subtotal =  $("#cart_total").text();
                        var current_shipping_charge = $("#cart_shipping").text();
                        $("#cart_total").text(Number(current_shipping_charge) + Number(current_cart_subtotal))
                        $("#toaster-text-cart").text("Cart Updated successfully");
                                $("#cart_toast").addClass("active");
                                setTimeout(function(){
                                    $("#cart_toastl").removeClass("active");
                                }, 5000);
                        //alert(data.message);
                        //location.reload();
                    }
                    //$(product).text("add to bag");
                // $(product).attr('disabled', false); 

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('error');

                }
            });
        }

        /*function redirect_to_login(page){
            alert(1)
                if(page=="cart"){
                    <?php  //$this->session->set_userdata('redirect_url', base_url()."cart"); ?>
                    window.location.href="login";
                }
                
           }*/
function disableplusminus(){
    $(".bs-val-variation").each(function() {
            var currVal = $(this).find('.js-count').val();
            var minusTrigger = $(this).find('.js-minus');
            var plusTrigger = $(this).find('.js-plus');
            var maxLimit = parseInt($(this).find('.js-plus').data("max"));
            console.log(currVal);
            if (currVal <= 1) {
            minusTrigger.attr("disabled", true);
            console.log(currVal + 'minus');
            } else if (currVal == maxLimit) {
            plusTrigger.attr("disabled", true);
            console.log(currVal + 'plus');
            }
            });
}


function getShipping(){
            var shipping_pin = $("#shipping_pincode").val();
            if(shipping_pin!=""){
                $.ajax({
                    url: '<?php echo site_url("cart/calculate") ?>',
                    type: "POST",
                    data: { pincode : shipping_pin },
                    dataType: "JSON",
                    success: function (data)
                    {   
                        
                        if(data=="0"){
                            $("#delivery_error").text("Not deliverable on this Pincode")

                            //$(".continue_shipping").attr("disabled","disabled")
                            //$("#message-alert-pincode").modal('show');
                            $("#pincode_status").val("invalid")
                       }else{
                            $("#delivery_error").text("Deliverable on this Pincode")
                            $("#pincode_status").val("valid")
                            $("#shipping_pincode").val(shipping_pin)
                            //$(".continue_shipping").attr("disabled", false)
                            //$("#message-alert-pincode").modal('hide');
                       
                       }
                       //var current_cart_subtotal =  $("#cart_subtotal").text();
                       //var current_cart_tax =  $("#cart_taxes").text();
                       
                       //$("#cart_shipping").text(data);
                       //$("#cart_total").text(Number(data) + Number(current_cart_subtotal) + Number(current_cart_tax))

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        //alert('error');

                    }
                });
            }
        }

$(document).ready(function () {
    jQuery("#newsletter_form").validate({
       rules: {           
               subscribe_email: {
                   required: true,
                   email: true
               }
           },
           errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
   });

     

});


$.validator.setDefaults({
        submitHandler: function() {
            //$('#btnSub').text('processing...'); //change button text
            $('#btnSub').addClass("btn-load");
            $('#btnSub').attr('disabled', true); //set button disable 
            $('#error_sub').html('');
              $.ajax({
                    url: '<?php echo site_url("home/newletterSub") ?>',
                    type: "POST",
                    data: $('#newsletter_form').serialize(),
                    dataType: "html",
                    success: function (data)
                    {
                        //console.log(data)
                        if($.trim(data)=="duplicate"){
                            //$('#error_sub').html("You have already subscribed");
                            $("#toaster-text").text("You have already signed  up for our newsletter");
                                $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                        }else 
                        if ($.trim(data)=="success") //if success close modal and reload ajax table
                        {
                           $('#newsletter_form').trigger("reset");
                           //$('#error_sub').html("Subscribed Successfully");
                            $("#toaster-text").text("You have Successfully signed  up for our newsletter");
                                $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                        } else if($.trim(data)=="fail")
                        {                    
                             $("#toaster-text").text("Subscribing for our newsletter has failed!");
                                $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                            //$('#error_sub').html("Subscribing Failed");
                        } 
                        $('#btnSub').removeClass("btn-load");
                        //$('#btnSub').text('submit'); //change button text
                        $('#btnSub').attr('disabled', false); //set button enable 


                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
        }
    });

</script>