
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('include/head.php'); ?>

<body>
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-gradient2 typ-slider js-blog-banner typ-blog-listing">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="banner-cont js-addto">
                    <div class="container">
                        <div class="banner-info">
                            <span class="cm-tag">Bestseller</span>
                            <ul class="mod-tags">
                                <li><a href="javascript:void(0)" class="tag-link">mba</a></li>
                                <li><a href="javascript:void(0)" class="tag-link">graduate</a></li>
                                <li><a href="javascript:void(0)" class="tag-link">working professional</a></li>
                                <li><a href="javascript:void(0)" class="tag-link">financial markets</a></li>
                              </ul>
                            <h2 class="title typ-sm">Know How Everyone Can be Rich Through Online Courses</h2>
                            <button type="button" class="btn btn-default btn-with-icon btn-reverse">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                        </div>
                    </div>
                    <img src="assets/images/banner/banner2.jpg" alt="banner1" class="js-img">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="banner-cont cm-bg-blue">
                    <div class="banner-cont js-addto">
                        <div class="container">
                            <div class="banner-info">
                                <span class="cm-tag">Bestseller</span>
                                <ul class="mod-tags">
                                    <li><a href="javascript:void(0)" class="tag-link">mba</a></li>
                                    <li><a href="javascript:void(0)" class="tag-link">graduate</a></li>
                                    <li><a href="javascript:void(0)" class="tag-link">working professional</a></li>
                                    <li><a href="javascript:void(0)" class="tag-link">financial markets</a></li>
                                  </ul>
                                <h2 class="title typ-sm">Know How Everyone Can be Rich Through Online Courses</h2>
                                <button type="button" class="btn btn-default btn-with-icon btn-reverse">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <img src="assets/images/banner/banner1.jpg" alt="banner1" class="js-img">
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="banner-cont js-addto">
                    <div class="container">
                        <div class="banner-info">
                            <span class="cm-tag">Bestseller</span>
                            <ul class="mod-tags">
                                <li><a href="javascript:void(0)" class="tag-link">mba</a></li>
                                <li><a href="javascript:void(0)" class="tag-link">graduate</a></li>
                                <li><a href="javascript:void(0)" class="tag-link">working professional</a></li>
                                <li><a href="javascript:void(0)" class="tag-link">financial markets</a></li>
                              </ul>
                            <h2 class="title typ-sm">Know How Everyone Can be Rich Through Online Courses</h2>
                            <button type="button" class="btn btn-default btn-with-icon btn-reverse">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                        </div>
                    </div>
                    <img src="assets/images/team.jpg" alt="banner1" class="js-img">
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>
            <!-- banner end -->
            <div class="bs-section">
                <div class="sec-cont">
                    <div class="container">
                        <div class="bs-filter">
                            <div class="bs-search typ2">
                                <input type="text" class="form-control" placeholder="Search Articles">
                                <button type="button" class="btn-addon js-open-search-panel"><span class="icon icon-search"></span></button>
                            </div>
                            <div class="bs-select visible-xs">
                                <select class="js-select js-typfliter">
                                    <option value="all">All</option>
                                    <option value="graduate">Graduate</option>
                                    <option value="mba">MBA</option>
                                    <option value="professional">Professional</option>
                                </select>
                            </div>
                            <div class="rhs hidden-xs">
                                <!-- <button class="btn btn-rounded">
                                    <span class="text">All Categories</span>
                                    <span class="icon  icon-filter"></span>
                                </button> -->
                                <div class="bs-filter-dropdown typ-sort">
                                    <select class="js-select-filter" >
                                        <option value="all">All Categories</option>
                                        <option value="moisturiser">Pricing Plans</option>
                                        <option value="facescrub">Course &amp; webinar</option>
                                        <option value="superfoods">Usage guide</option>
                                    </select>
                                    <span class="icon-filter icon icon-filter"></span>
                                </div>
                                <!-- <button class="btn btn-rounded">
                                    <span class="text">Recent Post</span>
                                    <span class="icon icon-sortdown"></span>
                                </button> -->
                                <div class="bs-filter-dropdown typ-sort">
                                    <select class="js-select-filter" >
                                        <option value="all">Recent Post</option>
                                        <option value="moisturiser">Pricing Plans</option>
                                        <option value="facescrub">Course &amp; webinar</option>
                                        <option value="superfoods">Usage guide</option>
                                    </select>
                                    <span class="icon-filter icon icon-sortdown"></span>
                                </div>
                            </div>
                            <div class="action-wrap visible-xs">
                                <button class="btn-icon" type="button"><span class="icon icon-sortdown"></span></button>
                            </div>
                        </div>
                        <ul class="row lyt-blog">
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/b3.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog typ2 js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/b2.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/b1.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog typ2 js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/img8.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/img7.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/team.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/b1.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/b2.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item">
                                <div class="bs-blog typ2 js-addto">
                                    <div class="img-wrap">
                                        <img src="assets/images/b3.jpg" class="js-img">
                                    </div>
                                    <span class="cm-tag">Latest</span>
                                    <div class="cont-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <div class="desc">
                                            <p>Know how everyone can be rich through online courses</p>
                                        </div>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </li>
                        </ul>
                        <div class="act-wrap view-more-btn">
                            <button type="button" class="btn btn-link with-line">Load More</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-desc">
                    <div class="bs-ad-banner">
                        <div class="container">
                            <div class="img-wrap js-addto">
                                <img src="assets/images/ad-banner.jpg" class="js-img">
                            </div>
                            <div class="element-wrap">
                                <span class="net"></span>
                            </div>
                            <div class="info-wrap">
                                <h3 class="title">Your Personal and Professional Goals with Finshiksha </h3>
                                <div class="desc">
                                    <p>Join now to receive personalized recommendations from finshiksha</p>
                                </div>
                                <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section typ-lyt">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-right">Top Courses</h2>
                    </div>
                    <div class="sec-desc">
                        <div class="row lyt-course">
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course4.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">EV</a></li>
                                            <li><a href="#" class="tag-link">IB,EV</a></li>
                                            <li><a href="#" class="tag-link">Credit</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>The most comprehensive course on Valuation in India</p>
                                            </li>
                                            <li>
                                                <p>Live Project with FinShiksha</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                      <img src="assets/images/products/course3.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                        <span class="cm-rating"><i class="icon icon-star"></i></span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course last">
                                    <div class="img-wrap js-addto">
                                      <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="blog-details.html" class="link"></a>
                                </div>
                            </div>
                        </div>
                        <div class="act-wrap view-more-btn">
                            <a href="course-details1.html" class="btn btn-link with-line">View all courses</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Upcoming Webinars</h2>
                    </div>
                    <div class="sec-desc">
                        <div class="lyt-upcoming-webinar">
                            <ul class="list-wrap row">
                                <li class="col-md-12">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img6.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                        <span data-filter="upcoming"></span>
                                        <span data-filter="all"></span>
                                    </div>
                                </li>
                                <li class="col-md-12">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img2.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">
                                                    Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                                </button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                        <span data-filter="past"></span>
                                        <span data-filter="all"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="act-wrap view-more-btn">
                            <button type="button" class="btn btn-link with-line">View All Webinar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    
        <!-- footer start -->
    <?php $this->load->view('include/footer.php'); ?>  
        <!-- footer end -->
    
    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>