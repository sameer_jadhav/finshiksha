
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('include/head.php'); ?>

<body>
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-center typ-sm typ-contact">
    <div class="banner-cont">
        <div class="banner-info">
            <h2 class="title">Let us know how to help</h2>
        </div>
    </div>
</div>
            <!-- banner end -->
            <section>
                <div class="bs-section typ-contact">
                    <div class="sec-cont">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-sm-offset-1 col-md-offset-0">
                                    <h2 class="sec-title element-left">Connect With Us</h2>
                                    <div class="bs-info-list">
                                        <ul class="list">
                                            <li class="item">
                                                <div class="info-item">
                                                    <div class="icon-wrap">
                                                        <span class="bg-icon icon-address"></span>
                                                    </div>
                                                    <div class="text-wrap">
                                                        <h3 class="title">Address</h3>
                                                        <address class="text">91 Springboard, Akruti Trade Center, B Wing, 5th Floor, MIDC, Andheri East, Mumbai - 400093</address>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="info-item">
                                                    <div class="icon-wrap">
                                                        <span class="bg-icon icon-call"></span>
                                                    </div>
                                                    <div class="text-wrap">
                                                        <h3 class="title">Call Us</h3>
                                                        <p class="text"><a href="tel:+91732927429">+91-732927429</a> | <a href="tel:+0224546345">+22 4546 345</a></p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="info-item">
                                                    <div class="icon-wrap">
                                                        <span class="bg-icon icon-email"></span>
                                                    </div>
                                                    <div class="text-wrap">
                                                        <h3 class="title">Email Us</h3>
                                                        <a href="mailto:contact@finshiksha.com" class="text">contact@finshiksha.com</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-5">
                                    <div class="bs-form bs-panel">
                                        <div class="panel-head">
                                            <h3 class="panel-title">Have any query ?</h3>
                                        </div>
                                        <form id="queryForm" method="get" action="">
                                            <div class="panel-wrap">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Enter Name">
                                                </div>
                                                <div class="form-group">
                                                    <input type="number" class="form-control" name="mob" placeholder="Moile Number">
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" name="mailid" placeholder="Email ID">
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" name="text" placeholder="Remarks"></textarea>
                                                </div>
                                            </div>
                                            <div class="panel-foot">
                                                <button type="submit" class="btn btn-default btn-with-icon">Submits <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section">
                    <div class="sec-head">
                        <h2 class="sec-title element-right">Frequently Asked Questions</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                            <ul class="lyt-btn-list">
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">Lorem Ipsum is simply dummy text of the printing</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">How to solve payment Issues</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">How to solve payment Issues</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">How to solve payment Issues</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">How to solve payment Issues</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">How to solve payment Issues</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">How to solve payment Issues</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="btn btn-faq" data-toggle="modal" data-target="#faq">How to solve payment Issues</a>
                                </li>
                            </ul>
                            <div class="act-wrap view-more-btn">
                                <button type="button" class="btn btn-link with-line">View all</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>

    <?php $this->load->view('include/footer.php'); ?>

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>