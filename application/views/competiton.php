
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('include/head.php'); ?>
<body class="course-details js-competition-filter pg-competition">
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-competition">
    <div class="banner-cont js-addto">
        <div class="container">
            <div class="banner-info">
                <h3 class="sub-title">Finshiksha's</h3>
                <h2 class="title">
                    <span class="cm-line-break">Learning</span>
                    <span class="cm-line-break">Championship</span>
                </h2>
                <label class="time-stamp">2020</label>
            </div>
            <div class="rel-info-wrap">
                <div class="rel-info">
                    <div class="rhs">
                        <img src="assets/images/winner-bg.jpg" alt="winner-bg" />
                        <div class="info">
                            <span class="text">Winners get upto</span>
                            <span class="price">Rs. 3,50,000</span>
                        </div>
                    </div>
                    <div class="lhs">
                        <h4 class="info-title">Corporate Partners</h4>
                        <div class="swiper-container partners-logo" id="logo-slide">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="mod-logo-list typ-sm">
                                        <ul class="list">
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/amazon.png" alt="amazon">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/loreal.png" alt="loreal">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/google.png" alt="google">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/deloitte.png" alt="deloitte">
                                                </div>
                                            </li>
                                            <li class="item last">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/db.png" alt="db">
                                                </div>
                                            </li>
                                            <li class="item last">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/et.png" alt="et">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="mod-logo-list typ-sm">
                                        <ul class="list">
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/scb.png" alt="scb">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/bajaj.png" alt="bajaj">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/honda.png" alt="honda">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/deloitte.png" alt="deloitte">
                                                </div>
                                            </li>
                                            <li class="item last">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/db.png" alt="db">
                                                </div>
                                            </li>
                                            <li class="item last">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/et.png" alt="et">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="mod-logo-list typ-sm">
                                        <ul class="list">
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/amazon.png" alt="amazon">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/loreal.png" alt="loreal">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/google.png" alt="google">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/deloitte.png" alt="deloitte">
                                                </div>
                                            </li>
                                            <li class="item last">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/db.png" alt="db">
                                                </div>
                                            </li>
                                            <li class="item last">
                                                <div class="img-wrap">
                                                    <img src="assets/images/logos/et.png" alt="et">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                    <div class="act-bar">
                        <span  class="note">Dont miss great oppportunity</span>
                        <button type="button" class="btn btn-default btn-with-icon btn-reverse">Apply Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
        <img src="assets/images/banner/competition.png" alt="competition" class="js-img">
    </div>
</div>
            <!-- banner end -->
            <section>
                <div class="bs-section">
                    <div class="sec-cont">
                        <div class="container">
                            <div class="mod-orange-band">
                                <div class="lhs">
                                    <span class="tag">Latest Update</span>
                                    <div class="decs">
                                        <h4 class="title">Entry to the Championship has closed</h4>
                                        <p class="para">If you still would like to learn and take up our courses</p>
                                    </div>
                                </div>
                                <div class="rhs">
                                    <button type="button" class="btn btn-default btn-with-icon">Take Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                </div>
                            </div>
                        </div>
                        <div class="bs-filter jsFilter typ-center">
                            <div class="container">
                                <ul class="filter">
                                    <li class="active">
                                        <a class="filter-link jsScroll-trig" href="#benefits">Benefits</a>
                                    </li>
                                    <li>
                                        <a class="filter-link jsScroll-trig" href="#process">Process</a>
                                    </li>
                                    <li>
                                        <a class="filter-link jsScroll-trig" href="#courses">Courses</a>
                                    </li>
                                    <li>
                                        <a class="filter-link jsScroll-trig" href="#corporatePartners">Corporate Partners</a>
                                    </li>
                                    <li>
                                        <a class="filter-link jsScroll-trig" href="#results">Results</a>
                                    </li>
                                </ul>
                            </div>
                      </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-championship">
                    <div class="sec-head" id="benefits">
                        <h2 class="sec-title element-left">Why Participate in Championship</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                            <ul class="mod-icon-text type2">
                                <li>
                                    <img src="assets/images/championship/championship1.svg" alt="championship1">
                                    <span class="text">
                                        <span class="cm-line-break">Strengthen your CV with Industry</span>
                                        <span class="cm-line-break">Recognised Certification</span>
                                    </span>
                                </li>
                                <li>
                                    <img src="assets/images/championship/championship2.svg" alt="championship2">
                                    <span class="text">
                                        <span class="cm-line-break">Internship / Placement Opportunities</span>
                                        <span class="cm-line-break">with Renowned Firms</span>
                                    </span>
                                </li>
                                <li>
                                    <img src="assets/images/championship/championship3.svg" alt="championship3">
                                    <span class="text">
                                        <span class="cm-line-break">Hands on Experience through</span>
                                        <span class="cm-line-break">Live Projects</span>
                                    </span>
                                </li>
                                <li class="last">
                                    <img src="assets/images/championship/championship4.svg" alt="championship4">
                                    <span class="text">
                                        <span class="cm-line-break">Courses available at discounted</span>
                                        <span class="cm-line-break">rates in the competion</span>
                                    </span>
                                </li>
                                <li class="last">
                                    <img src="assets/images/championship/championship5.svg" alt="championship5">
                                    <span class="text">
                                        <span class="cm-line-break">Chance to win 15 Prizes</span>
                                        <span class="cm-line-break">worth Rs 350,000</span>
                                    </span>
                                </li>
                                <li class="last">
                                    <img src="assets/images/championship/championship6.svg" alt="championship6">
                                    <span class="text">
                                        <span class="cm-line-break">Year Long access to self paced </span>
                                        <span class="cm-line-break">content</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section">
                    <div class="sec-cont">
                        <div class="sub-sec typ-steps no-space" id="process">
                            <div class="container">
                                <h3 class="subtitle element-left">Championship Process</h3>
                                <div class="mod-steps">
                                    <ul>
                                        <li>
                                            <div class="mod-icon-text">
                                                <div class="img img-click">
                                                    <img src="assets/images/img-click.svg" alt="img-click">
                                                </div>
                                                <div class="decs">
                                                    <h4 class="title">Enroll & Learn</h4>
                                                    <p>
                                                        <span class="cm-line-break">Enroll your combo course & be </span>
                                                        <span>a part of this competion</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="mod-icon-text">
                                                <div class="img img-mail">
                                                    <img src="assets/images/img-mail.svg" alt="img-mail">
                                                </div>
                                                <div class="decs">
                                                    <h4 class="title">Apply</h4>
                                                    <p>
                                                        <span class="cm-line-break">Enroll your combo course & be </span>
                                                        <span>a part of this competion</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="mod-icon-text">
                                                <div class="img img-time">
                                                    <img src="assets/images/img-time.svg" alt="img-time">
                                                </div>
                                                <div class="decs">
                                                    <h4 class="title">Certification</h4>
                                                    <p>
                                                        <span class="cm-line-break">Enroll your combo course & be </span>
                                                        <span>a part of this competion</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="mod-icon-text">
                                                <div class="img img-speaker">
                                                    <img src="assets/images/img-speaker.svg" alt="img-speaker">
                                                </div>
                                                <div class="decs">
                                                    <h4 class="title">Result</h4>
                                                    <p>
                                                        <span class="cm-line-break">Enroll your combo course & be </span>
                                                        <span>a part of this competion</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            </section>
            <div class="bs-ad-banner typ-full js-addto">
                <div class="container">
                    
                        <img src="assets/images/teacher.png" class="js-img">
                    
                    <div class="info-wrap">
                        <h3 class="title">Gateway to the most coveted Jobs in Finance</h3>
                        <div class="desc">
                            <p>A National Level Competition for B-School students, the FinShiksha Learning Championship aims to find participants who can show.</p>
                        </div>
                    </div>
                    <button type="button" data-toggle="modal" data-target="#video" class="btn btn-video typ2 js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
                </div>
            </div>
            <section>
                <div class="bs-section typ-timelines">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Competition Timelines</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                            <div class="timeline-img">
                                <img class="hidden-xs" src="assets/images/Timeline.png" alt="timeline">
                                <img class="visible-xs" src="assets/images/timeline-m.png" alt="timeline">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-required">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Required Courses</h2>
                    </div>
                    <div class="sec-cont" id="courses">
                        <div class="container">
                            <div class="bs-tabs typ-rounded">
                                <ul class="nav nav-tabs js-magic-line" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#1years" aria-controls="general" role="tab" data-toggle="tab">1st Year Courses </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#2years" aria-controls="pricing" role="tab" data-toggle="tab">2nd Year Courses </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="1years">
                                        <div class="lyt-course-opt typ-sm">
                                            <ul class="row">
                                                <li class="col-md-6 col-xs-12">
                                                    <div class="mod-course-opt typ2">
                                                        <h2 class="course-name">Marketing & Research</h2>
                                                        <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                        <div class="action-wrap">
                                                            <div class="bs-currency">
                                                                <div class="og-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12.400</span>
                                                                </div>
                                                                <div class="old-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12,000</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                        </div>
                                                   </div>
                                                </li>
                                                <li class="col-md-6 col-xs-12">
                                                    <div class="mod-course-opt typ2">
                                                        <h2 class="course-name">Marketing & Research</h2>
                                                        <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                        <div class="action-wrap">
                                                            <div class="bs-currency">
                                                                <div class="og-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12.400</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                        </div>
                                                   </div>
                                                </li>
                                            </ul>
                                            <ul class="row">
                                                 <li class="col-md-12 col-xs-12 last">
                                                    <div class="mod-course-opt with-tag active">
                                                         <ul class="mod-tags">
                                                             <li><a href="#" class="tag-link">Combo Course</a></li>
                                                         </ul>
                                                         <h2 class="course-name">Investment Banking + Credit  Analysis</h2>
                                                         <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                         <div class="action-wrap">
                                                             <div class="bs-currency">
                                                                 <div class="og-price">
                                                                     <span class="icon icon-inr">Rs.</span>
                                                                     <span class="value">12.400</span>
                                                                 </div>
                                                                 <div class="old-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12,000</span>
                                                                </div>
                                                             </div>
                                                             <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                         </div>
                                                    </div>
                                                 </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="2years">
                                        <div class="lyt-course-opt">
                                            <ul class="row">
                                                <li class="col-md-4  col-xs-12">
                                                    <div class="mod-course-opt typ2">
                                                        <h2 class="course-name">Marketing & Research</h2>
                                                        <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                        <div class="action-wrap">
                                                            <div class="bs-currency">
                                                                <div class="og-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12.400</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                        </div>
                                                   </div>
                                                </li>
                                                <li class="col-md-4  col-xs-12">
                                                    <div class="mod-course-opt typ2">
                                                        <h2 class="course-name">Marketing & Research</h2>
                                                        <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                        <div class="action-wrap">
                                                            <div class="bs-currency">
                                                                <div class="og-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12.400</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                        </div>
                                                   </div>
                                                </li>
                                                <li class="col-md-4  col-xs-12">
                                                    <div class="mod-course-opt typ2">
                                                        <h2 class="course-name">Marketing & Research</h2>
                                                        <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                        <div class="action-wrap">
                                                            <div class="bs-currency">
                                                                <div class="og-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12.400</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                        </div>
                                                   </div>
                                                </li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-md-6  col-xs-12">
                                                   <div class="mod-course-opt with-tag active">
                                                        <ul class="mod-tags">
                                                            <li><a href="#" class="tag-link">Combo Course</a></li>
                                                        </ul>
                                                        <h2 class="course-name">Investment Banking + Credit  Analysis</h2>
                                                        <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                        <div class="action-wrap">
                                                            <div class="bs-currency">
                                                                <div class="og-price">
                                                                    <span class="icon icon-inr">Rs.</span>
                                                                    <span class="value">12.400</span>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                        </div>
                                                   </div>
                                                </li>
                                                <li class="col-md-6  col-xs-12">
                                                    <div class="mod-course-opt with-tag active">
                                                         <ul class="mod-tags">
                                                             <li><a href="#" class="tag-link">Combo Course</a></li>
                                                         </ul>
                                                         <h2 class="course-name">Investment Banking + Credit  Analysis</h2>
                                                         <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                         <div class="action-wrap">
                                                             <div class="bs-currency">
                                                                 <div class="og-price">
                                                                     <span class="icon icon-inr">Rs.</span>
                                                                     <span class="value">12.400</span>
                                                                 </div>
                                                             </div>
                                                             <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                         </div>
                                                    </div>
                                                 </li>
                                                 <li class="col-md-6  col-xs-12 last">
                                                    <div class="mod-course-opt with-tag active">
                                                         <ul class="mod-tags">
                                                             <li><a href="#" class="tag-link">Combo Course</a></li>
                                                         </ul>
                                                         <h2 class="course-name">Investment Banking + Credit  Analysis</h2>
                                                         <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                         <div class="action-wrap">
                                                             <div class="bs-currency">
                                                                 <div class="og-price">
                                                                     <span class="icon icon-inr">Rs.</span>
                                                                     <span class="value">12.400</span>
                                                                 </div>
                                                             </div>
                                                             <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                         </div>
                                                    </div>
                                                 </li>
                                                 <li class="col-md-6  col-xs-12 last">
                                                    <div class="mod-course-opt with-tag active">
                                                         <ul class="mod-tags">
                                                             <li><a href="#" class="tag-link">Combo Course</a></li>
                                                         </ul>
                                                         <h2 class="course-name">Investment Banking + Credit  Analysis</h2>
                                                         <p class="para">Advanced techniques and shortcuts to design faster than most professionals…</p>
                                                         <div class="action-wrap">
                                                             <div class="bs-currency">
                                                                 <div class="og-price">
                                                                     <span class="icon icon-inr">Rs.</span>
                                                                     <span class="value">12.400</span>
                                                                 </div>
                                                             </div>
                                                             <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                         </div>
                                                    </div>
                                                 </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-corporate">
                    <div class="sec-head" id="corporatePartners">
                        <h2 class="sec-title element-left">Our Corporate Partners</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                            <div class="bs-logo-list">
                                <ul class="ll-wrap">
                                    <li class="ll-item">
                                        <img src="assets/images/logos/1.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/2.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/3.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/4.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/5.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/6.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/7.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/8.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/9.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/10.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/11.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/12.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/13.jpg">
                                    </li>
                                    <li class="ll-item">
                                        <img src="assets/images/logos/14.jpg">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="bs-champion-band js-addto js-responsive-png">
                            <img src="assets/images/trophy.png" class="js-img">
                            <div class="container">
                                <div class="info-wrap">
                                    <span class="cm-label">Flat 10% off</span>
                                    <h3 class="cb-title">FinShiksha Learning Championship 2018</h3>
                                    <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-judges">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Meet Our Judges</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="bs-teams typ2">
                            <div class="container">
                                <ul class="teams-wrap">
                                    <li class="teams-item">
                                        <div class="team-tile">
                                            <div class="img-wrap js-addto">
                                                <img src="assets/images/p2.jpg" alt="Team 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Dishant Shah</h3>
                                                <strong class="designation">CEO in Amazon</strong>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="teams-item">
                                        <div class="team-tile">
                                            <div class="img-wrap js-addto">
                                                <img src="assets/images/p3.jpg" alt="Team 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Karl Gonsalves</h3>
                                                <strong class="designation">Managing Director Amity</strong>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="teams-item">
                                        <div class="team-tile">
                                            <div class="img-wrap js-addto">
                                                <img src="assets/images/p4.jpg" alt="Team 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Purohit Swanimathan</h3>
                                                <strong class="designation">Proffesional Advisor</strong>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section">
                    <div class="sec-head" id="results">
                        <h2 class="sec-title element-left word-trim">FinShiksha Learning Championship</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                        <div class="bs-tabs typ-horizontal">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#results" aria-controls="general" role="tab" data-toggle="tab">results</a>
                                </li>
                                <li role="presentation">
                                    <a href="#updates" aria-controls="pricing" role="tab" data-toggle="tab">updates</a>
                                </li>
                                <li role="presentation">
                                    <a href="#championshipHighlights" aria-controls="webinar" role="tab" data-toggle="tab">championship highlights</a>
                                </li>
                                <li role="presentation">
                                    <a href="#corporatePartners" aria-controls="guide" role="tab" data-toggle="tab">corporate partners</a>
                                </li>
                                <li role="presentation">
                                    <a href="#prizeBenefits" aria-controls="pricing" role="tab" data-toggle="tab">Prize &amp; Benefits</a>
                                </li>
                                <li role="presentation">
                                    <a href="#faq" aria-controls="webinar" role="tab" data-toggle="tab">FAQ's</a>
                                </li>
                                <li role="presentation">
                                    <a href="#tnc" aria-controls="guide" role="tab" data-toggle="tab">Terms &amp; Condition</a>
                                </li>
                            </ul>
                            <div class="tab-select bs-custom-select typ-box">
                                <select class="js-select" id="tabSelect">
                                    <option value="all" data-href="#results">results</option>
                                    <option value="moisturiser" data-href="#updates">updates</option>
                                    <option value="facescrub" data-href="#championshipHighlights">championship highlights</option>
                                    <option value="superfoods" data-href="#corporatePartners">corporate partners</option>
                                    <option value="moisturiser" data-href="#prizeBenefits">Prize &amp; Benefits</option>
                                    <option value="facescrub" data-href="#faq">FAQ's</option>
                                    <option value="superfoods" data-href="#tnc">Terms &amp; Condition</option>
                                </select>
                            </div>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="results">a</div>
                                <div role="tabpanel" class="tab-pane" id="updates">b</div>
                                <div role="tabpanel" class="tab-pane" id="championshipHighlights">c</div>
                                <div role="tabpanel" class="tab-pane" id="corporatePartners">d</div>
                                <div role="tabpanel" class="tab-pane" id="prizeBenefits">e</div>
                                <div role="tabpanel" class="tab-pane" id="faq">f</div>
                                <div role="tabpanel" class="tab-pane" id="tnc">g</div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

    
        <!-- footer start -->
    <?php $this->load->view('include/footer.php'); ?>  
        <!-- footer end -->
    

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>