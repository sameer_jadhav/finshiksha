
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('include/head.php'); ?>

<body>
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-gradient typ-details">
    <div class="banner-cont js-addto">
        <img src="assets/images/web-banner.png" alt="banner1" class="js-img">
        <div class="container">
            <div class="banner-info">
                <div class="webi-info">
                    <span class="cm-stream">Streaming now</span>
                    <time datetime="04:00-08:00" class="cm-duration">Today 4:00 PM - 8:00 PM</time>
                </div>
                <h3 class="title">Change The Way Of Looking Into Analytic Charts</h3>
                <strong class="author">By <span class="italic">Abhinav Gupta</span><a href="javascript:void(0)" class="lin icon icon-in"></a></strong>
                <div class="desc typ2">
                    <p>TThis course is a perfectly designed capital market course</p>
                </div>
                <ul class="mod-icon-text">
                    <li>
                        <span class="icon icon-idea"></span>
                        <span class="text">Premium</span>
                    </li>
                    <li>
                        <span class="icon icon-calendar"></span>
                        <span class="text">Sat 16th May</span>
                    </li>
                    <li>
                        <span class="icon icon-clock"></span>
                        <span class="text">5pm - 6pm (1 Hours)</span>
                    </li>
                </ul>
            </div>
            <div class="banner-img js-addto">
                <img src="assets/images/webinar-man.png" alt="banner1" class="js-img">
                <button type="button" data-toggle="modal" data-target="#video" class="btn btn-video typ2  js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
                <!-- <span class="cm-rating"><i class="icon icon-star"></i></span> -->
            </div>
            <div class="banner-element">
                <span class="net"></span>
            </div>
        </div>
    </div>
</div>

            <!-- banner end -->
            <div class="bs-section typ-action">
                <div class="sec-desc">
                    <div class="container">
                        <div class="bs-action-band">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">graduate</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">financial markets</a></li>
                            </ul>
                            <div class="action-wrap">
                                <div class="bs-currency">
                                    <div class="old-price">
                                        <span class="icon icon-inr">Rs.</span>
                                        <span class="value">12,000</span>
                                    </div>
                                    <div class="og-price">
                                        <span class="icon icon-inr">Rs.</span>
                                        <span class="value">1120</span>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                <div class="action-call">
                                    <button type="button" class="btn-call"><span class="icon icon-call"></span><span class="text">Request a Call</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-desc">
                  <div class="container">
                    <div class="lyt-detail">
                      <div class="lhs">
                        <div class="sub-sec">
                          <h3 class="subtitle element-left">Introduction</h3>
                          <p>In this Webinar, the speaker would be discussing in details about the market dynamics.</p>
                          <ul class="bs-list typ-hash">
                            <li>How the stock market works?</li>
                            <li>Characteristics of different phases of the markets.</li>
                            <li>Being Bull or Bear?</li>
                            <li>Psychology of Bulls and Bears</li>
                            <li>When to Sell a profitable position?</li>
                            <li>When to book loss?</li>
                            <li>How to sell Short?</li>
                          </ul>
                        </div>
                        <div class="sub-sec">
                          <h3 class="subtitle xspace element-left">Objective</h3>
                          <p>The objectve of  this webinar is to show that it is important to plan and device your exit strategy in advance of making your investments. Its often perceived that selling stocks is more challenging than it looks.  Choosing when to sell a stock can be a difficult task. For most traders,</p>
                          <p>it is hard to separate their emotions from their trades. We will learn some of the methods  to over come this and be on the right side of the trade with correct timing. The objectve of  this webinar is to show that it is important to plan and device your exit strategy in advance of making your investments. Its often perceived that selling stocks is more challenging than it looks.  Choosing when to sell a stock can be a difficult task. For most traders, it is hard to separate their emotions from their trades..</p>
                        </div>
                        <div class="sub-sec last">
                          <h3 class="subtitle xspace element-left">Materials For Participants</h3>
                          <ul class="bs-list typ-bullet">
                            <li>1 Hour of Extra demo</li>
                            <li>Presentation used by the speaker.</li>
                            <li>Anytime access video</li>
                          </ul>
                        </div>
                      </div>
                      <div class="side-bar">
                        <div class="mod-speaker">
                          <h3 class="title">About the Speaker</h3>
                          <div class="img-wrap">
                            <img src="assets/images/img2.jpg" alt="img6">
                          </div>
                          <div class="rel-info">
                            <h4 class="name">Ajay Arora<a href="javascript:void(0)" class="lin icon icon-in"></a></h4>
                            <label class="related-info">1256 Learners | 30 courses</label>
                            </div>
                          <div class="desc">
                            <p>My education, career, and even personal life have been molded by one simple principle; well designed information resonates</p>
                          </div>
                        </div>
                        <div class="lyt-webinar-list">
                          <h3 class="title">Upcoming Webinars</h3>
                          <ul>
                            <li>
                              <div class="bs-webinar typ-sm">
                                  <div class="info-wrap">
                                      <ul class="mod-tags">
                                          <li><a href="#" class="tag-link">Banking</a></li>
                                          <li><a href="#" class="tag-link">Finance</a></li>
                                          <li><a href="#" class="tag-link">Credit</a></li>
                                      </ul>
                                      <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                      <ul class="mod-icon-text">
                                          <li>
                                              <span class="icon icon-calendar"></span>
                                              <span class="text">12 Mar 2020</span>
                                          </li>
                                          <li>
                                              <span class="icon icon-clock"></span>
                                              <span class="text">2 Hours</span>
                                          </li>
                                      </ul>
                                      <label class="name">
                                          <span>By</span>
                                          <span class="italic">Abhinav Gupta<a href="javascript:void(0)" class="lin icon icon-in"></a></span>
                                      </label>
                                  </div>
                                  <a class="link" href="#"></a>
                              </div>
                            </li>
                            <li>
                              <div class="bs-webinar typ-sm">
                                  <div class="info-wrap">
                                      <ul class="mod-tags">
                                          <li><a href="#" class="tag-link">Banking</a></li>
                                          <li><a href="#" class="tag-link">Finance</a></li>
                                          <li><a href="#" class="tag-link">Credit</a></li>
                                      </ul>
                                      <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                      <ul class="mod-icon-text">
                                          <li>
                                              <span class="icon icon-calendar"></span>
                                              <span class="text">12 Mar 2020</span>
                                          </li>
                                          <li>
                                              <span class="icon icon-clock"></span>
                                              <span class="text">2 Hours</span>
                                          </li>
                                      </ul>
                                      <label class="name">
                                          <span>By</span>
                                          <span class="italic">Abhinav Gupta<a href="javascript:void(0)" class="lin icon icon-in"></a></span>
                                      </label>
                                  </div>
                                  <a class="link" href="#"></a>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="sub-sec typ-steps last">
                    <div class="container">
                        <h3 class="subtitle element-left">Steps to participate</h3>
                        <div class="mod-steps">
                            <ul>
                                <li>
                                    <div class="mod-icon-text">
                                        <div class="img img-click">
                                            <img src="assets/images/img-click.svg" alt="img-click">
                                        </div>
                                        <div class="decs">
                                            <p>
                                                <span class="cm-line-break">Click on 'Reserve Spot Today !' button and book</span>
                                                <span>your seat for the webinar.</span>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mod-icon-text">
                                        <div class="img img-mail">
                                            <img src="assets/images/img-mail.svg" alt="img-mail">
                                        </div>
                                        <div class="decs">
                                            <p>
                                                <span class="cm-line-break">You will receive an email containing</span>
                                                <span class="cm-line-break">a link to join the webinar.</span>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mod-icon-text">
                                        <div class="img img-time">
                                            <img src="assets/images/img-time.svg" alt="img-time">
                                        </div>
                                        <div class="decs">
                                            <p>
                                                <span class="cm-line-break">Click on the same link to join 15 minutes</span>
                                                <span class="cm-line-break">before the start of the webinar.</span>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mod-icon-text">
                                        <div class="img img-speaker">
                                            <img src="assets/images/img-speaker.svg" alt="img-speaker">
                                        </div>
                                        <div class="decs">
                                            <p>
                                                <span class="cm-line-break">Do necessary configuration of your</span>
                                                <span class="cm-line-break">headphone/speaker and system volume.</span>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                  </div>
                  <div class="bs-section typ-participation">
                    <div class="container">
                        <div class="sec-head">
                            <h2 class="sec-title typ-sm">Participants Speaks</h2>
                        </div>
                        <div class="sec-cont">
                            <!-- testimonial start -->
                            <div class="bs-testimonials">
    <div class="swiper-container testimonial-img">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img2.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img3.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img4.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img5.jpg" alt="testimonial images">
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-container testimonial-decs">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide-count">
        <span class="active">01</span> / <span class="total">03</span>
    </div>
    <div class="slideNav">
        <div class="swiper-button-next"></div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
                            <!-- testimonial end -->
                        </div>
                    </div>
                </div>
                  <div class="container">
                    <div class="sub-sec last">
                      <h3 class="subtitle element-left">FAQs</h3>
                      <a href="faq.html" class="btn btn-link with-line side-link hidden-xs">View all FAQ’a</a>
                      <div class="bs-accordian">
                        <div
                          class="panel-group"
                          id="accordion"
                          role="tablist"
                          aria-multiselectable="true"
                        >
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                              <a
                                role="button"
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#collapseOne"
                                aria-expanded="false"
                                aria-controls="collapseOne"
                                class="collapsed"
                                >Will I receive the recording after the live webinar
                                is over? <span class="icon icon-plus"></span
                              ></a>
                            </div>
                            <div
                              id="collapseOne"
                              class="panel-collapse collapse"
                              role="tabpanel"
                              aria-labelledby="headingOne"
                              aria-expanded="false"
                              style="height: 0px;"
                            >
                              <div class="panel-body">
                                We host more than 150,000 courses on our online
                                learning marketplace. Our marketplace model means we
                                do not own the copyright to the content of the
                                courses; the respective instructors own these rights.
                                On rare occasions, instructors may remove their
                                courses from our marketplace, or, Udemy may have to
                                remove a course from the platform for policy or legal
                                reasons. If this does happen to a course you're
                                enrolled in, please contact us and we'll be ready to
                                help
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                              <a
                                class="collapsed"
                                role="button"
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#collapseTwo"
                                aria-expanded="false"
                                aria-controls="collapseTwo"
                                >Where can I find the recording of the live
                                webinar?<span class="icon icon-plus"></span
                              ></a>
                            </div>
                            <div
                              id="collapseTwo"
                              class="panel-collapse collapse"
                              role="tabpanel"
                              aria-labelledby="headingTwo"
                              aria-expanded="false"
                              style="height: 0px;"
                            >
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high
                                life accusamus terry richardson ad squid. 3 wolf moon
                                officia aute, non cupidatat skateboard dolor brunch.
                                Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et.
                                Nihil anim keffiyeh helvetica, craft beer labore wes
                                anderson cred nesciunt sapiente ea proident. Ad vegan
                                excepteur butcher vice lomo. Leggings occaecat craft
                                beer farm-to-table, raw denim aesthetic synth nesciunt
                                you probably haven't heard of them accusamus labore
                                sustainable VHS.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <a
                                class="collapsed"
                                role="button"
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#collapseThree"
                                aria-expanded="false"
                                aria-controls="collapseThree"
                                >Will I receive the recording after the live webinar
                                is over?<span class="icon icon-plus"></span
                              ></a>
                            </div>
                            <div
                              id="collapseThree"
                              class="panel-collapse collapse"
                              role="tabpanel"
                              aria-labelledby="headingThree"
                              aria-expanded="false"
                            >
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high
                                life accusamus terry richardson ad squid. 3 wolf moon
                                officia aute, non cupidatat skateboard dolor brunch.
                                Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et.
                                Nihil anim keffiyeh helvetica, craft beer labore wes
                                anderson cred nesciunt sapiente ea proident. Ad vegan
                                excepteur butcher vice lomo. Leggings occaecat craft
                                beer farm-to-table, raw denim aesthetic synth nesciunt
                                you probably haven't heard of them accusamus labore
                                sustainable VHS.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                              <a
                                class="collapsed"
                                role="button"
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#collapseFour"
                                aria-expanded="false"
                                aria-controls="collapseFour"
                                >Will I receive the recording after the live webinar
                                is over?<span class="icon icon-plus"></span
                              ></a>
                            </div>
                            <div
                              id="collapseFour"
                              class="panel-collapse collapse"
                              role="tabpanel"
                              aria-labelledby="headingFour"
                              aria-expanded="false"
                            >
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high
                                life accusamus terry richardson ad squid. 3 wolf moon
                                officia aute, non cupidatat skateboard dolor brunch.
                                Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et.
                                Nihil anim keffiyeh helvetica, craft beer labore wes
                                anderson cred nesciunt sapiente ea proident. Ad vegan
                                excepteur butcher vice lomo. Leggings occaecat craft
                                beer farm-to-table, raw denim aesthetic synth nesciunt
                                you probably haven't heard of them accusamus labore
                                sustainable VHS.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                              <a
                                class="collapsed"
                                role="button"
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#collapseFive"
                                aria-expanded="false"
                                aria-controls="collapseFive"
                                >Will I receive the recording after the live webinar
                                is over?<span class="icon icon-plus"></span
                              ></a>
                            </div>
                            <div
                              id="collapseFive"
                              class="panel-collapse collapse"
                              role="tabpanel"
                              aria-labelledby="headingFive"
                              aria-expanded="false"
                            >
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high
                                life accusamus terry richardson ad squid. 3 wolf moon
                                officia aute, non cupidatat skateboard dolor brunch.
                                Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et.
                                Nihil anim keffiyeh helvetica, craft beer labore wes
                                anderson cred nesciunt sapiente ea proident. Ad vegan
                                excepteur butcher vice lomo. Leggings occaecat craft
                                beer farm-to-table, raw denim aesthetic synth nesciunt
                                you probably haven't heard of them accusamus labore
                                sustainable VHS.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                              <a
                                class="collapsed"
                                role="button"
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#collapseSix"
                                aria-expanded="false"
                                aria-controls="collapseSix"
                                >Will I receive the recording after the live webinar
                                is over?<span class="icon icon-plus"></span
                              ></a>
                            </div>
                            <div
                              id="collapseSix"
                              class="panel-collapse collapse"
                              role="tabpanel"
                              aria-labelledby="headingSix"
                              aria-expanded="false"
                            >
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high
                                life accusamus terry richardson ad squid. 3 wolf moon
                                officia aute, non cupidatat skateboard dolor brunch.
                                Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et.
                                Nihil anim keffiyeh helvetica, craft beer labore wes
                                anderson cred nesciunt sapiente ea proident. Ad vegan
                                excepteur butcher vice lomo. Leggings occaecat craft
                                beer farm-to-table, raw denim aesthetic synth nesciunt
                                you probably haven't heard of them accusamus labore
                                sustainable VHS.
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="act-btn visible-xs">
                        <a href="faq.html" class="btn btn-link with-line">View all FAQ’a</a>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-head">
                    <div class="container">
                        <h2 class="sec-title">Best Plans for Webinar</h2>
                    </div>
                </div>
                <div class="sec-desc">
                    <div class="lyt-plan typ-bg">
                        <div class="container">
                            <ul>
                                <li>
                                    <div class="bs-plan">
                                        <div class="img-wrap">
                                            <span class="icon icon-leaf"></span>
                                        </div>
                                        <h2 class="title">Single Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">500</span>
                                                <span class="qty">onwards</span>
                                            </div>
                                        </div>
                                        <label class="count">1 Webinar</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <a href="#" class="plan-link"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="bs-plan active">
                                        <div class="img-wrap">
                                            <span class="icon icon-money"></span>
                                        </div>
                                        <h2 class="title">Quadra Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">1,400</span>
                                                <span class="qty">/ 6 month</span>
                                            </div>
                                        </div>
                                        <label class="count">4 Webinars</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <a href="#" class="plan-link"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="bs-plan">
                                        <div class="img-wrap">
                                            <span class="icon icon-finance"></span>
                                        </div>
                                        <h2 class="title">Octo Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">2,400</span>
                                                <span class="qty">/ 1 year</span>
                                            </div>
                                        </div>
                                        <label class="count">8 Webinars</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <a href="#" class="plan-link"></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section typ-lyt">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-right">Related Courses</h2>
                    </div>
                    <div class="sec-desc">
                        <div class="row lyt-course">
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course4.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course3.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                        <span class="cm-rating"><i class="icon icon-star"></i></span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course last">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                          <span class="cm-tag">Bestseller</span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                        </div>
                        <div class="act-wrap view-more-btn">
                            <button type="button" class="btn btn-link with-line">View All Courses</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Past webinars</h2>
                    </div>
                    <div class="sec-desc">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-webinar">
                                    <div class="img-wrap">
                                        <img src="assets/images/img3.jpg" alt="webinar">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">Banking</a></li>
                                            <li><a href="#" class="tag-link">Finance</a></li>
                                            <li><a href="#" class="tag-link">Credit</a></li>
                                        </ul>
                                        <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                        <ul class="mod-icon-text">
                                            <li>
                                                <span class="icon icon-calendar"></span>
                                                <span class="text">Sat 16th May</span>
                                            </li>
                                            <li>
                                                <span class="icon icon-clock"></span>
                                                <span class="text">5pm - 6pm (1 Hours)</span>
                                            </li>
                                        </ul>
                                        <label class="name">
                                            <span>By</span>
                                            <span class="italic">Abhinav Gupta<a href="javascript:void(0)" class="lin icon icon-in"></a></span>
                                        </label>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-secondary">Buy Recording</button>
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">14,300</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="link" href="#"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-webinar">
                                    <div class="img-wrap">
                                        <img src="assets/images/img5.jpg" alt="webinar">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">Banking</a></li>
                                            <li><a href="#" class="tag-link">Finance</a></li>
                                            <li><a href="#" class="tag-link">Credit</a></li>
                                        </ul>
                                        <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                        <ul class="mod-icon-text">
                                            <li>
                                                <span class="icon icon-calendar"></span>
                                                <span class="text">Sat 16th May</span>
                                            </li>
                                            <li>
                                                <span class="icon icon-clock"></span>
                                                <span class="text">5pm - 6pm (1 Hours)</span>
                                            </li>
                                        </ul>
                                        <label class="name">
                                            <span>By</span>
                                            <span class="italic">Abhinav Gupta<a href="javascript:void(0)" class="lin icon icon-in"></a></span>
                                        </label>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-secondary">Buy Recording</button>
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">14,300</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="link" href="#"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-webinar">
                                    <div class="img-wrap">
                                        <img src="assets/images/img4.jpg" alt="webinar">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">Banking</a></li>
                                            <li><a href="#" class="tag-link">Finance</a></li>
                                            <li><a href="#" class="tag-link">Credit</a></li>
                                        </ul>
                                        <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                        <ul class="mod-icon-text">
                                            <li>
                                                <span class="icon icon-calendar"></span>
                                                <span class="text">Sat 16th May</span>
                                            </li>
                                            <li>
                                                <span class="icon icon-clock"></span>
                                                <span class="text">5pm - 6pm (1 Hours)</span>
                                            </li>
                                        </ul>
                                        <label class="name">
                                            <span>By</span>
                                            <span class="italic">Abhinav Gupta<a href="javascript:void(0)" class="lin icon icon-in"></a></span>
                                        </label>
                                        <div class="action-wrap">
                                            <button type="button" class="btn btn-secondary">Buy Recording</button>
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">14,300</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="link" href="#"></a>
                                </div>
                            </div>
                        </div>
                        <div class="act-wrap view-more-btn">
                            <button type="button" class="btn btn-link with-line">View All Webinar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    
        <!-- footer start -->
        <?php $this->load->view('include/footer.php'); ?>
        <!-- footer end -->
    

    <!-- popup start -->
<?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>