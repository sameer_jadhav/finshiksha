
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('include/head.php'); ?>

<body>
   <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <div class="container">
                <!-- banner start -->
                <div class="bs-blog-banner">
    <ul class="mod-tags">
        <li><a href="javascript:void(0)" data-filter="mba" class="tag-link">mba</a></li>
        <li><a href="javascript:void(0)" data-filter="professional" class="tag-link">working professional</a></li>
        <li><a href="javascript:void(0)" data-filter="all" class="tag-link">combo course</a></li>
    </ul>
    <h2 class="title">Stock Trading Strategies to Help Maximize Profit and Minimize Losses</h2>
    <ul class="info-wrap">
        <li class="info-item">
            <time>12 May 2020</time>
        </li>
        <li class="info-item">
            <time>7 mins read</time>
        </li>
        <li class="info-item">
            By <span class="italic">Abhinav Gupta</span>
        </li>
    </ul>
    <div class="img-wrap js-addto">
        <img src="assets/images/team.jpg" alt="Blog Banner" title="Blog Banner" class="js-img">
    </div>
</div>
                <!-- banner end -->
                <div class="bs-section">
                    <div class="sec-desc">
                        <div class="lyt-detail typ-blog">
                            <div class="lhs">
                                <p>Are you interested in trading stocks but have no idea where to  start?  Or maybe you just need some guidance along the way?  Although there are no specific stock trading strategies that can guarantee a profit, there are strategies that can help increase your chances of making some money and reduce the risk of losing it.  Whether you are a day trader, position trader or a swing trader, we have you covered.  So put the textbooks, stock charts, graphs and other materials away for a bit and let’s get this started.</p>
                                <p>Most stocks are traded on stock exchanges, which are places where sellers and buyers come together and decide on a price.  These stock exchanges are either a physical location, such as the New York Stock Exchange, or virtual, which is through the internet.  Keep in mind, however, that trading stocks and investing stocks do not share the same meaning.</p>
                                <h3 class="cm-head">Trading vs. Investing</h3>
                                <p>Stock trading and investing might confuse some  people into thinking they are the exact same thing.  In reality, there are some very fundamental differences when it comes to deciding between trading and investing.  It’s vital that you know the difference between the two before we go any further into strategies.</p>
                                <p>The goal of investing is to slowly build profit over an extended period of time through buying and holding stocks, bonds, mutual funds and other investments.  Investors gain profits through compounding, or reinvesting their profits and dividends into additional shares of stock.  These investments are usually held for years or even decades in order to take advantage of benefits such as interest, dividends and stock splits along the way.  It is given that markets fluctuate throughout time, but investors are more focused on the return on investment over the long haul.</p>
                                <p>Traders, on the other hand, are frequently buying and selling stocks and commodities with the goal of generating returns that outperform buy-and-hold investing.  Investors may be satisfied with a 10% annual return, but traders are seeking 10% return each month.  Each trader tends to have a certain style, or a holding period in which stocks and commodities are bought and sold.  There are generally three types of traders:</p>
                                <ul class="bs-list typ-bullet">
                                    <li>Day Trader – Positions are held throughout the day only, nothing overnight</li>
                                    <li>Position Trader – Positions are held from months to years</li>
                                    <li>Swing Trader – Positions are held from days to weeks</li>
                                </ul>
                                <p>Continue reading below and you will learn some basic strategies for all three types of traders.  Remember that this is merely a guide to help assist you throughout your trading endeavors to minimize risk and maximize reward.</p>
                                <h3 class="cm-head">Day Trading Strategies</h3>
                                <p>People who day trade can use certain techniques to increase their profit.  Some of these strategies are leverage and selling short.  Leverage is when you borrow money in order to make more of it.  So let’s say a certain trade results in a 10% return.  If you have $10,000 in your account, then your return will be $1,000.  But if you were to borrow another $10,000 and add it to your account, then your return will double to $2,000.  Basically, leverage allows you to increase the dollars returned to you without increasing the performance of the trade.</p>
                                <p>So how do you obtain the extra money?  Most traders borrow it from their brokerage firms using a margin account.  A margin account differs from your cash account in that it requires at least $2,000 in initial investments.  Once the account is opened, you can borrow up to 50% of the purchase price of a stock.</p>
                                <p>The other borrowing strategy is selling short.  This is when you borrow a security and then sell it with the hope of repaying the loan by buying back cheaper shares later on.  For this instance, you are looking for a security that is going down in price, rather than buying low and selling high.  When you find that type of security, place an order to sell the stock and tell your broker you want to borrow them.  After that, wait until the security goes down in price, then buy the shares in the market.  Then return the shares to the broker to pay back the loan, and keep the difference as profit.</p>
                                <div class="bs-video js-addto">
                                    <img src="assets/images/course-detail.jpg" alt="video poster image" class="js-img"/>
                                    <iframe class="video" width="100%" height="100%" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <button type="button" class="btn btn-video js-video-play" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
                                </div>
                                <h3 class="cm-head">Position Trading Strategies</h3>
                                <p>Position trading strategies are ideal for investors who want to make money trading stocks, but don’t have the time or resources to day trade or make high frequency trading.  There are a few guidelines you will want to follow as a position trader.</p>
                                <p>First, identify position trading opportunities.  This can include a wide variety of potential stock moving events including new product rollouts, anticipated policy changes, seasonal events, company events, or company earnings reports.</p>
                                <p>It is also important to buy or sell short if you are anticipating a price drop.  Doing this far enough in advance of a potential stock moving event allows you to take advantage of anticipation buying or selling that may start making the stock’s price to increase or decrease prior to that event.</p>
                                <p>Set up a stop loss to preserve position trading capital.  This should be entered to protect a stock position from unexpected events that cause a move in the stock’s price that isn’t anticipated.  This is particular important when position trading since these you probably won’t have the time to always monitor the news flow relating to a stock that you hold a position in.  A stop loss limits any unexpected losses from occurring and reduces your risk.</p>
                                <p>If possible, try to check the news flow related to the stocks you are holding at least once a day.  This helps assess whether new developments associated with your stock require adjustments in the position trade or even closure if necessary.  An example of this is if your position trade is the anticipation of a new product launch.  This news comes out saying that the product launch has been delayed for at least five months.  Wouldn’t you possibly reconsider your position trade?</p>
                                <h3 class="cm-head">Stock Trading Ninja</h3>
                                <p>Just because you learned a few trading strategies doesn’t mean you should quit your day job just yet!  Being a successful stock trader requires a lot more learning and studying than most people think.  If you want a more detailed system on how to make money trading stocks, look no further as the Stock Trading Ninja course might be just what you need.  Frank Bunn is a professional Stock Trader who has over 17 years of trading experience.  His program is ideal for both beginning and experienced traders and has been proven to be a money maker.  If you don’t believe me, read some of the reviews!</p>
                            </div>
                            <div class="side-bar">
                                <ul class="sb-wrap">
                                    <li class="sb-item">
                                        <div class="mod-speaker typ-blog">
                                            <h3 class="title">About the Author</h3>
                                            <div class="img-wrap">
                                                <img src="assets/images/img2.jpg" alt="img6">
                                            </div>
                                            <div class="rel-info">
                                                <h4 class="name">Ajay Arora<a href="javascript:void(0)" class="lin icon icon-in"></a></h4>
                                            </div>
                                            <div class="desc">
                                                <p>My education, career, and even personal life have been molded by one simple principle; well designed information resonates</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="sb-item">
                                        <div class="mod-combo">
                                            <h3 class="title">Top Articles</h3>
                                            <ul>
                                                <li>
                                                    <div class="combo">
                                                        <ul class="mod-tags">
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">banking</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">finance</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">credit</a>
                                                            </li>
                                                        </ul>
                                                        <h4 class="title">
                                                            Know How Everyone Can be Rich Through Online Courses
                                                        </h4>
                                                        <a class="link" href="javascript:void(0)"></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="combo">
                                                        <ul class="mod-tags">
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">Market</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">finance</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">Banking</a>
                                                            </li>
                                                        </ul>
                                                        <h4 class="title">
                                                            Forex Technical Analysis for Forex Challenged
                                                        </h4>
                                                        <a class="link" href="javascript:void(0)"></a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="combo">
                                                        <ul class="mod-tags">
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">Budget</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">Stock</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="tag-link">Finance</a>
                                                            </li>
                                                        </ul>
                                                        <h4 class="title">
                                                            Budgeting Tips : Top 3 Ways to Stay on Track
                                                        </h4>
                                                        <a class="link" href="javascript:void(0)"></a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="sb-item">
                                        <div class="mod-related-links">
                                            <h3 class="title">Related Topics</h3>
                                            <ul class="link-list">
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Marketing</a>
                                                </li>
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Stock</a>
                                                </li>
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Finance</a>
                                                </li>
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Business</a>
                                                </li>
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Management</a>
                                                </li>
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Credit</a>
                                                </li>
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Banking</a>
                                                </li>
                                                <li class="link-item">
                                                    <a href="#" class="related-link">Budget</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- blog start -->
            <div class="bs-section typ-more-blog">
    <div class="container">
        <div class="sec-head">
            <h2 class="sec-title element-left">Related Articles</h2>
        </div>
        <div class="sec-desc swiper-container js-blog-swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img7.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog typ2 js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img8.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/team.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
            </div>
            <!-- <div class="act-wrap view-more-btn">
                <button type="button" class="btn btn-link with-line">View all blogs</button>
            </div> -->
        </div>
    </div>
</div>
            <!-- blog end -->
            <div class="bs-section typ-lyt">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Top Courses</h2>
                    </div>
                    <div class="sec-desc">
                        <div class="row lyt-course">
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course4.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course3.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                        <span class="cm-rating"><i class="icon icon-star"></i></span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bs-course last">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                          <span class="cm-tag">Bestseller</span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" class="tag-link">mba</a></li>
                                            <li><a href="#" class="tag-link">working professional</a></li>
                                            <li><a href="#" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                            <li>
                                                <p>Certification in credit analysis</p>
                                            </li>
                                            <li>
                                                <p>Certification in investment banking</p>
                                            </li>
                                            <li>
                                                <p>Certification in lorem ipsum</p>
                                            </li>
                                        </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">1120</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="#" class="link"></a>
                                </div>
                            </div>
                        </div>
                        <div class="act-wrap view-more-btn">
                            <button type="button" class="btn btn-link with-line">View All Courses</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

   <?php $this->load->view('include/footer.php'); ?>

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>

    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>