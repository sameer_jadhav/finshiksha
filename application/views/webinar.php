
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('include/head.php'); ?>

<body>
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-webinar">
    <div class="banner-cont cm-bg-dark">
        <div class="container">
            <div class="banner-info">
                <h2 class="title">Save More with Webinar Plans</h2>
                <button type="button" class="btn btn-default btn-with-icon">Know More <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
            </div>
            <div class="banner-element">
                <span class="net"></span>
            </div>
        </div>
        <div class="bs-plan-list">
            <ul class="list-wrap">
                <li class="item">
                    <div class="icon-wrap">
                        <span class="icon icon-leaf"></span>
                    </div>
                    <div class="info-wrap">
                        <h3 class="plan-name">Single Plan</h3>
                        <strong class="plan-count">500</strong>
                        <label class="webi-count">1 Webinar</label>
                    </div>
                </li>
                <li class="item">
                    <div class="icon-wrap">
                        <span class="icon icon-money"></span>
                    </div>
                    <div class="info-wrap">
                        <h3 class="plan-name">Quadra Plan</h3>
                        <strong class="plan-count">1,400</strong>
                        <label class="webi-count">4 Webinars</label>
                    </div>
                </li>
                <li class="item">
                    <div class="icon-wrap">
                        <span class="icon icon-finance"></span>
                    </div>
                    <div class="info-wrap">
                        <h3 class="plan-name">Octo Plan</h3>
                        <strong class="plan-count">2,400</strong>
                        <label class="webi-count">8 Webinars</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
            <!-- banner end -->
            <div class="bs-section">
                <div class="sec-desc">
                    <div class="container">
                        <div class="bs-filter">
                            <ul class="filter hidden-xs">
                                <!-- <li class="js-fliter active">
                                    <a href="#">All Events</a>
                                </li> -->
                                <li class="js-fliter active">
                                    <a href="#">Upcoming</a>
                                </li>
                                <li class="js-fliter">
                                    <a href="#">Past</a>
                                </li>
                                <li>
                            </ul>
                            <div class="rhs hidden-xs">
                                <div class="bs-filter-dropdown typ-sort">
                                    <select class="js-select-filter" id="selectFilter">
                                        <option value="all">By Categories</option>
                                        <option value="moisturiser">Pricing Plans</option>
                                        <option value="facescrub">Course &amp; webinar</option>
                                        <option value="superfoods">Usage guide</option>
                                    </select>
                                    <span class="icon-filter icon icon-filter"></span>
                                </div>
                            </div>
                            <div class="bs-select visible-xs">
                                <select class="js-select">
                                    <!-- <option value="all">All Events</option> -->
                                    <option value="upcoming">Upcoming</option>
                                    <option value="past">Past</option>
                                </select>
                            </div>
                            <div class="action-wrap visible-xs">
                                <button class="btn-icon" type="button"><span class="icon icon-filter"></span></button>
                            </div>
                        </div>
                        <div class="lyt-webinar">
                            <ul class="webinar-list">
                                <li class="item">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img6.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                        <span data-filter="upcoming"></span>
                                        <span data-filter="all"></span>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img2.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">
                                                    Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                                </button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                        <span data-filter="past"></span>
                                        <span data-filter="all"></span>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img6.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                        <span data-filter="upcoming"></span>
                                        <span data-filter="all"></span>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img2.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">
                                                    Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                                </button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                        <span data-filter="past"></span>
                                        <span data-filter="all"></span>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img6.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                        <span data-filter="past"></span>
                                        <span data-filter="all"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="lyt-webinar dash-bor">
                            <h3 class="subtitle">Past Webinars</h3>
                            <ul class="row webinar-list">
                                <li class="item col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img3.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                                <li class="item col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img5.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                                <li class="item col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img4.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                                <li class="item col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img3.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                                <li class="item col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img5.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                                <li class="item col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img4.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                            </ul>
                            <div class="act-wrap view-more-btn">
                                <a href="webinar-listing.html" class="btn btn-link with-line">Load More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section bg2">
                <div class="sec-head">
                    <h2 class="sec-title element-right">Plans For Webinars</h2>
                </div>
                <div class="sec-desc">
                    <div class="lyt-plan typ2">
                        <div class="container">
                            <ul>
                                <li>
                                    <div class="bs-plan">
                                        <div class="img-wrap">
                                            <span class="icon icon-leaf"></span>
                                        </div>
                                        <h2 class="title">Single Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">500</span>
                                                <span class="qty">onwards</span>
                                            </div>
                                        </div>
                                        <label class="count">1 Webinar</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <ul class="detail-list">
                                            <li class="item"><span class="icon icon-tick"></span> 1 Webinar only</li>
                                            <li class="item"><span class="icon icon-tick"></span> No Bulk Discounting</li>
                                            <!-- <li class="item"><span class="icon icon-tick"></span> 3 Webinar events extra</li> -->
                                            <li class="item"><span class="icon icon-tick"></span> Validity 1 Webinar</li>
                                            <!-- <li class="item mobile"><span class="icon icon-tick"></span> Lorem ipsum</li>
                                            <li class="item mobile"><span class="icon icon-tick"></span> Lorem ipsum dolor sit</li> -->
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="bs-plan">
                                        <div class="img-wrap">
                                            <span class="icon icon-money"></span>
                                        </div>
                                        <h2 class="title">Quadra Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">1,400</span>
                                                <span class="qty">/ 6 month</span>
                                            </div>
                                        </div>
                                        <label class="count">4 Webinars</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <ul class="detail-list">
                                            <li class="item"><span class="icon icon-tick"></span> 4 Webinars</li>
                                            <li class="item"><span class="icon icon-tick"></span> 30% Doscount</li>
                                            <li class="item"><span class="icon icon-tick"></span> Validity 6 months</li>
                                            <!-- <li class="item"><span class="icon icon-tick"></span> Validy 3 months</li> -->
                                            <!-- <li class="item mobile"><span class="icon icon-tick"></span> Lorem ipsum</li>
                                            <li class="item mobile"><span class="icon icon-tick"></span> Lorem ipsum dolor sit</li> -->
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="bs-plan">
                                        <div class="img-wrap">
                                            <span class="icon icon-finance"></span>
                                        </div>
                                        <h2 class="title">Octo Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">2,400</span>
                                                <span class="qty">/ 1 year</span>
                                            </div>
                                        </div>
                                        <label class="count">8 Webinars</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <ul class="detail-list">
                                            <li class="item"><span class="icon icon-tick"></span> 8 Webinars</li>
                                            <li class="item"><span class="icon icon-tick"></span> 40% Doscount</li>
                                            <!-- <li class="item"><span class="icon icon-tick"></span> 3 Webinar events extra</li> -->
                                            <li class="item"><span class="icon icon-tick"></span> Validy 12 months</li>
                                            <!-- <li class="item mobile"><span class="icon icon-tick"></span> Lorem ipsum</li>
                                            <li class="item mobile"><span class="icon icon-tick"></span> Lorem ipsum dolor sit</li> -->
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="element">
                            <span class="shape shape1"></span>
                            <span class="shape shape2"></span>
                            <span class="shape shape3"></span>
                            <span class="shape shape4"></span>
                            <span class="icon icon-hash shape5"></span>
                            <span class="shape6"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-desc">
                    <div class="bs-ad-banner">
                        <div class="container">
                            <div class="img-wrap js-addto">
                                <img src="assets/images/ad-banner.jpg" class="js-img">
                            </div>
                            <div class="element-wrap">
                                <span class="net"></span>
                            </div>
                            <div class="info-wrap">
                                <h3 class="title">Your Personal and Professional Goals with Finshiksha </h3>
                                <div class="desc">
                                    <p>Join now to receive personalized recommendations from finshiksha</p>
                                </div>
                                <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- blog start -->
            <div class="bs-section">
    <div class="container">
        <div class="sec-head">
            <h2 class="sec-title element-left">Read Our Blogs</h2>
        </div>
        <div class="sec-desc swiper-container js-blog-swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img7.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog typ2 js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img8.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/team.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
            </div>
            <div class="act-wrap view-more-btn">
                <button type="button" class="btn btn-link with-line">View all blogs</button>
            </div>
        </div>
    </div>
</div>
            <!-- blog end -->
        </div>
    </main>

    <?php $this->load->view('include/footer.php'); ?>

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>