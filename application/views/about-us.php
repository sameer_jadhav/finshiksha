
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('include/head.php'); ?>

<body class="pg-about">
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-gradient3 typ-center typ-about">
    <div class="banner-cont js-addto">
        <div class="container">
            <div class="banner-info">
                <h2 class="title">Improving Life Through Learning</h2>
                <div class="desc typ2">
                    <p>The strength and consistency of the inverse relation between education and mortality over time</p>
                </div>
            </div>
        </div>
        <img src="assets/images/banner/home-banner-2.jpg" alt="banner1" class="js-img">
    </div>
</div>
            <!-- banner end -->
            <section>
                <div class="bs-section typ-infography">
                    <div class="sec-cont">
                        <div class="container">
                            <div class="bs-infography">
                                <div class="title-wrap">
                                    <h2 class="title">The leading global marketplace for learning and instruction</h2>
                                    <div class="desc">
                                        <p>They said, finance is complicated. They said, teaching finance is difficult, learning even more so. They said this is the most boring subject on earth. They also kept teaching the same old slides and content for ages. That was till yesterday – then we happened. FinShiksha – aims at uncomplicating finance, and showing how interesting these numbers could be.</p>
                                    </div>
                                </div>
                                <div class="info-wrap">
                                    <div class="bs-meter-reading">
                                        <ul class="list-wrap">
                                            <li class="lits-item">
                                                <strong class="meter-count"><span class="count">50</span> K</strong>
                                                <label class="meter-label">Instructors</label>
                                            </li>
                                            <li class="lits-item">
                                                <strong class="meter-count"><span class="count">2.5</span> L</strong>
                                                <label class="meter-label">Participants</label>
                                            </li>
                                            <li class="lits-item">
                                                <strong class="meter-count"><span class="count">295</span> M</strong>
                                                <label class="meter-label">Course Enrollment</label>
                                            </li>
                                            <li class="lits-item">
                                                <strong class="meter-count"><span class="count">300</span> HRS</strong>
                                                <label class="meter-label">Video Content</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-ad">
                    <div class="sec-cont">
                        <div class="bs-ad-banner typ-img js-addto">
                            <div class="container">
                                <div class="img-wrap ">
                                    <img src="assets/images/play.png" class="js-img">
                                </div>
                                <div class="element-wrap">
                                    <span class="net"></span>
                                </div>
                                <div class="info-wrap">
                                    <h3 class="title"><span class="text-sm">Our story</span>A Great Place to Grow with</h3>
                                    <button type="button" data-toggle="modal" data-target="#video" class="btn btn-video typ2 js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
                                </div>
                            </div>
                        </div>
                        <span class="net"></span>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-achieve">
                    <div class="container">
                        <div class="sec-head">
                            <h2 class="sec-title element-left">Achieve World Class Learning</h2>
                        </div>
                        <div class="sec-cont">
                            <div class="bs-learnings typ-col3">
                                <ul>
                                    <li class="item">
                                        <span class="icon icon-school"></span>
                                        <span class="text">Learn from leading <span class="cm-line-break">universities and companies</span></span>
                                    </li>
                                    <li class="item">
                                        <span class="icon icon-save"></span>
                                        <span class="text">Accesible, Find flexible, affordable options</span>
                                    </li>
                                    <li class="item">
                                        <span class="icon icon-star2"></span>
                                        <span class="text">Accesible, Find flexible, affordable options</span>
                                    </li>
                                    <li class="item">
                                        <span class="icon icon-skill"></span>
                                        <span class="text">Master skills with in-depth learning</span>
                                    </li>
                                    <li class="item">
                                        <span class="icon icon-clock2"></span>
                                        <span class="text">Accesible, Find flexible, affordable options</span>
                                    </li>
                                    <li class="item">
                                        <span class="icon icon-school"></span>
                                        <span class="text">Earn industry recognized credentials</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section">
                    <div class="container">
                        <!-- <div class="sec-head">
                            <h2 class="sec-title element-left">Our Corporate Partners</h2>
                            <div class="sec-desc">
                                <p>We offer the worlds largest network of finance industry</p>
                            </div>
                        </div> -->
                        <div class="sec-cont">
                            <div class="lyt-partners row">
                                <div class="lhs col-md-6 col-sm-6">
                                    <h4 class="sec-title element-left typ-sm">Our College Presences</h4>
                                    <div class="swiper-container" id="partnerSwiper1">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <div class="mod-logo-list">
                                                    <ul>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/18.jpg" alt="amazon">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/1.jpg" alt="loreal">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/17.jpg" alt="google">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/11.jpg" alt="deloitte">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/14.jpg" alt="db">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/13.jpg" alt="et">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/16.jpg" alt="scb">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/15.jpg" alt="bajaj">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/12.jpg" alt="honda">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="mod-logo-list">
                                                    <ul>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/18.jpg" alt="amazon">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/1.jpg" alt="loreal">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/17.jpg" alt="google">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/11.jpg" alt="deloitte">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/14.jpg" alt="db">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/13.jpg" alt="et">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/16.jpg" alt="scb">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/15.jpg" alt="bajaj">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/12.jpg" alt="honda">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slideNav">
                                            <div class="swiper-button-next"></div>
                                            <!-- <div class="swiper-pagination"></div> -->
                                            <div class="swiper-button-prev"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rhs col-md-6 col-sm-6">
                                    <h4 class="sec-title element-left typ-sm">Our Corporate partners</h4>
                                    <div class="swiper-container" id="partnerSwiper2">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <div class="mod-logo-list">
                                                    <ul>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/2.jpg" alt="amazon">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/3.jpg" alt="loreal">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/4.jpg" alt="google">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/5.jpg" alt="deloitte">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/6.jpg" alt="db">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/7.jpg" alt="et">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/8.jpg" alt="scb">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/9.jpg" alt="bajaj">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/10.jpg" alt="honda">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="mod-logo-list">
                                                    <ul>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/2.jpg" alt="amazon">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/3.jpg" alt="loreal">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/4.jpg" alt="google">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/5.jpg" alt="deloitte">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/6.jpg" alt="db">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/7.jpg" alt="et">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/8.jpg" alt="scb">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/9.jpg" alt="bajaj">
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="img-wrap">
                                                                <img src="assets/images/logos/10.jpg" alt="honda">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slideNav">
                                            <div class="swiper-button-next"></div>
                                            <!-- <div class="swiper-pagination"></div> -->
                                            <div class="swiper-button-prev"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section">
                    <div class="sec-head">
                        <h2 class="sec-title">Meet Our Team</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="bs-teams">
                            <div class="container">
                                <ul class="teams-wrap">
                                    <li class="teams-item">
                                        <div class="team-tile">
                                            <div class="img-wrap js-addto">
                                                <img src="assets/images/p1.jpg" alt="Team 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Peeyush Chitlangia</h3>
                                                <strong class="designation">Founder of Finshiksha</strong>
                                            </div>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#team" class="team-link"></a>
                                        </div>
                                    </li>
                                    <li class="teams-item">
                                        <div class="team-tile">
                                            <div class="img-wrap js-addto">
                                                <img src="assets/images/p2.jpg" alt="Team 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Parth Parikh</h3>
                                                <strong class="designation">Co-Founder of Finshiksha</strong>
                                            </div>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#team" class="team-link"></a>
                                        </div>
                                    </li>
                                    <li class="teams-item">
                                        <div class="team-tile">
                                            <div class="img-wrap js-addto">
                                                <img src="assets/images/p3.jpg" alt="Team 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Saloni Munshi</h3>
                                                <strong class="designation">Researcher in Finshiksha</strong>
                                            </div>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#team" class="team-link"></a>
                                        </div>
                                    </li>
                                    <li class="teams-item">
                                        <div class="team-tile">
                                            <div class="img-wrap js-addto">
                                                <img src="assets/images/p4.jpg" alt="Team 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Manoj D</h3>
                                                <strong class="designation">Researcher in Finshiksha</strong>
                                            </div>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#team" class="team-link"></a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section lyt-testimonials">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Testimonial</h2>
                        <div class="sec-desc">
                            <p>Our curated collection of top-rated business and technical courses gives companies,</p>
                        </div>
                    </div>
                    <div class="sec-cont">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="mod-testimonial js-addto">
                                        <img src="assets/images/testimonial.jpg" alt="Testimonial" class="js-img">
                                        <div class="title-wrap">
                                            <label class="testi-desig">Investment Banking + Credit Analysis</label>
                                            <h3 class="testi-title">Rahul Agrawal</h3>
                                        </div>
                                        <button type="button" data-toggle="modal" data-target="#video" class="btn btn-video  js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="mod-testimonial js-addto">
                                        <img src="assets/images/testimonial3.jpg" alt="Testimonial" class="js-img">
                                        <div class="title-wrap">
                                            <label class="testi-desig">Investment Banking + Credit Analysis</label>
                                            <h3 class="testi-title">Rahul Agrawal</h3>
                                        </div>
                                        <button type="button" data-toggle="modal" data-target="#video" class="btn btn-video js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="mod-testimonial js-addto">
                                        <img src="assets/images/testimonial4.jpg" alt="Testimonial" class="js-img">
                                        <div class="title-wrap">
                                            <label class="testi-desig">Investment Banking + Credit Analysis</label>
                                            <h3 class="testi-title">Rahul Agrawal</h3>
                                        </div>
                                        <button type="button" data-toggle="modal" data-target="#video" class="btn btn-video js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button swiper-button-next"></div>
                        <div class="swiper-button swiper-button-prev"></div>
                        <span class="net net1"></span>
                        <span class="net net2"></span>
                        <span class="icon  icon-s4"></span>
                    </div>
                </div>
            </section>
            <div class="bs-band">
                <div class="container">
                    <div class="info-wrap">
                        <h2 class="title">Work with best finance team </h2>
                        <span class="icon icon-hash"></span>
                        <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join with us <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- footer start -->
     <?php $this->load->view('include/footer.php'); ?>
    <!-- footer end -->
    

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>