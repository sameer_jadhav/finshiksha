


<section class="content">
    <?php
    if ($this->session->flashdata('msg')) {
        # code...
        echo $this->session->flashdata('msg');
    }
    ?>
    <div class="row">
        <form id="add_form" method="post" enctype="multipart/form-data" action="<?php echo admin_url('profile/save'); ?>" class="form-horizontal">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit</h3>
                    </div>
                    <br>
                    <div class="form-body  row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="typediv col-md-9">
                                    <input type="text" name="name" value="<?php echo $result->name ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="typediv col-md-9">
                                    <input type="text" disabled value="<?php echo $result->email ?>" class="form-control">
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label class="control-label col-md-3">Mobile</label>
                                <div class="typediv col-md-9">
                                    <input type="text" name="mobile" max="13" min="10"  value="<?php echo $result->mobile ?>" class="form-control " required>
                                </div> -->
                        </div>
                        </div>
                       <!--  <div class="col-lg-4">                            
                            <div class="form-group">
                                <label class="control-label col-md-3">Change Photo</label>
                                <div class="typediv col-md-9">
                                    <?php if (!empty($result->photo)) { 
                                        $this->_default_photo = base_url('uploads/users/thumbnail/'.$result->photo);

                                    } ?>


                                    <img widht="60px" height="80px" class="img-circle" id="profile-img-tag" name="photo" src="<?= $this->_default_photo ?>" />
                                    <input type="file"  name="photo" id="profile-img"  class="form-control ">
                                    <img src=""  width="200px" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" col-md-12">
                                   
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo admin_url('profile/password_change')?>">Change Password </a>&nbsp&nbsp&nbsp&nbsp&nbsp
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
    </div>
</section>




<script type="text/javascript">

    function readURL(input) {

        if (input.files && input.files[0]) {

            var reader = new FileReader();

            

            reader.onload = function (e) {

                $('#profile-img-tag').attr('src', e.target.result);

            }

            reader.readAsDataURL(input.files[0]);

        }

    }

    $("#profile-img").change(function(){

        readURL(this);

    });

</script>
<script type="text/javascript">
    function validationDemo() {
        $("#add_form").validate({
            rules: {

                email: {
                    email: true
                },

                number1: {
                    digits: true
                },
                category: {
                    required: true,
                },
                //                menu1: {
                //                    required: true,
                //                },
                number2: {
                    digits: true
                },

                type: {
                    required: function () {
                        return $('[name="type"]:checked').length === 0;
                    }
                }

            },
            errorPlacement: function (error, el) {
                if (el.attr('name') === 'type') {
                    error.appendTo('.typediv');
                } else {
                    error.insertAfter(el);
                }
            },

        });
    }
    validationDemo();
</script>