
<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About us</h3>
            </div>            
            <form method="post" enctype="multipart/form-data" action="<?php echo admin_url('master/update') ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Short About <small><i>(Home page)</i></small></label>
                  <textarea class="form-control" name="short_about" required><?php  echo $master->short_about ?></textarea>
                </div>
                <p>About Us Page</p>
                <div class="form-group">
                  <label for="exampleInputEmail1">INSIDE THE PANTRY</label>
                  <textarea class="form-control" name="description1" required><?php  echo $master->description1 ?></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Image 1</label>
                  <input type="file" id="image1" name="image1"><br>
                  <a href="<?php echo base_url('uploads/slider/'.$master->image1) ?>" target="_blank"><img src="<?php echo base_url('uploads/slider/'.$master->image1) ?>" width="50px"></a>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">MEET THE CHEF</label>
                  <textarea class="form-control" name="description2" required><?php  echo $master->description2 ?></textarea>
                </div>
                <!-- 
                <div class="form-group">
                  <label for="exampleInputFile">Image 2 <small>Multiple</small></label>
                  <input type="file" name="multiple_image[]" multiple  accept="image/png,image/jpg,image/jpeg">                  
                </div>
                <?php foreach ($about_images as $key => $value): ?>
                    <a href="<?php echo base_url('uploads/slider/'.$value->image) ?>" target="_blank">
                        <img src="<?php echo base_url('uploads/slider/'.$value->image) ?>" width="50px">&nbsp
                    </a>
                    <a href="javascript:void(0)" title="Delete" onclick="delete_image('<?php echo $value->id ?>')"><i class="fa fa-trash-o"></i></a>
                  <?php endforeach ?> -->

                  <div class="form-group">
                  <label for="exampleInputFile">Image 2</label>
                  <input type="file" id="image2" name="image2"><br>
                  <a href="<?php echo base_url('uploads/slider/'.$master->image2) ?>" target="_blank"><img src="<?php echo base_url('uploads/slider/'.$master->image2) ?>" width="50px"></a>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Image 3</label>
                  <input type="file" id="image3" name="image3"><br>
                  <a href="<?php echo base_url('uploads/slider/'.$master->image3) ?>" target="_blank"><img src="<?php echo base_url('uploads/slider/'.$master->image3) ?>" width="50px"></a>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Image 4</label>
                  <input type="file" id="image4" name="image4"><br>
                  <a href="<?php echo base_url('uploads/slider/'.$master->image4) ?>" target="_blank"><img src="<?php echo base_url('uploads/slider/'.$master->image4) ?>" width="50px"></a>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Image 5</label>
                  <input type="file" id="image5" name="image5"><br>
                  <a href="<?php echo base_url('uploads/slider/'.$master->image5) ?>" target="_blank"><img src="<?php echo base_url('uploads/slider/'.$master->image5) ?>" width="50px"></a>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Image 6</label>
                  <input type="file" id="image6" name="image6"><br>
                  <a href="<?php echo base_url('uploads/slider/'.$master->image6) ?>" target="_blank"><img src="<?php echo base_url('uploads/slider/'.$master->image6) ?>" width="50px"></a>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Image 7</label>
                  <input type="file" id="image7" name="image7"><br>
                  <a href="<?php echo base_url('uploads/slider/'.$master->image7) ?>" target="_blank"><img src="<?php echo base_url('uploads/slider/'.$master->image7) ?>" width="50px"></a>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">OUR INGREDIENTS</label>
                  <textarea class="form-control" name="description3" required><?php  echo $master->description3 ?></textarea>
                </div>
                <div class="box-header with-border">
                  <h3 class="box-title">Contact Us</h3>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                  <textarea class="form-control" name="address" required><?php  echo $master->address ?></textarea>
                </div>                
                <div class="form-group">
                  <label for="exampleInputEmail1">Contact</label>
                  <input class="form-control" name="contact" type="email" value="<?php  echo $master->contact ?>" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Toll Free</label>
                  <input class="form-control" name="toll_free" type="text" value="<?php  echo $master->toll_free ?>" required>
                </div>  

                <div class="form-group">
                  <label for="exampleInputEmail1">Copy right </label>
                  <textarea class="form-control" name="copy_right" required><?php  echo $master->copy_right ?></textarea>                  
                </div>  


              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          
        </div>
       
      </div>
      <!-- /.row -->
    <div>

</div>
</section>
     
        <script type="text/javascript">
            
            function save()
            {
                $('#btnSave').text('saving...'); //change button text
                $('#btnSave').attr('disabled', true); //set button disable 
                var url;
                if (save_method == 'add') {
                    url = "<?php echo admin_url('slider/add') ?>";
                } else {
                    url = "<?php echo admin_url('slider/update') ?>";
                }
                // ajax adding data to database
                var formData = new FormData($('#form')[0]);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    success: function (data)
                    {
                        if (data.status) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            reload_table();
                        } else
                        {
                            for (var i = 0; i < data.inputerror.length; i++)
                            {
                                $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                            }
                        }
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    }
                });
            }
            function delete_image(id)
            {
                if (confirm('Are you sure delete this data?'))
                {
                    // ajax delete data to database
                    $.ajax({
                        url: "<?php echo admin_url('master/delete_image') ?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function (data)
                        {
                            location.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error deleting data');
                        }
                    });
                }
            }


CKEDITOR.replace('short_about');
CKEDITOR.replace('description1');
CKEDITOR.replace('description2');
CKEDITOR.replace('description3');
CKEDITOR.replace('copy_right');
CKEDITOR.replace('address');


</script>
