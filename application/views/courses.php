
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('include/head.php'); ?>

<body>
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-gradient">
    <div class="banner-cont js-addto" style="background-image: url(&quot;assets/images/img5.jpg&quot;);">
        <div class="container">
            <div class="banner-info">
                <span class="cm-label">Flat 10% off</span>
                <h2 class="title typ-sm">Combo Courses Starts from</h2>
                <div class="bs-currency">
                    <div class="og-price">
                        <span class="icon icon-inr">Rs.</span>
                        <span class="value">11,300</span>
                    </div>
                </div>
                <div class="desc typ2">
                    <p>Build skills with courses, certificates, and degrees online from world-class universities and companies</p>
                </div>
                <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
            </div>
        </div>
        <img src="assets/images/team.jpg" alt="banner1" class="js-img">
    </div>
</div>
            <!-- banner end -->
            <div class="bs-section">
                <div class="sec-cont">
                    <div class="container">
                        <div class="bs-filter">
                            <ul class="filter-with-icon hidden-xs">
                                <li class="js-fliter active">
                                    <a href="#" id="all">
                                        <span class="icon icon-agenda"></span>
                                        <span class="text">All</span>
                                    </a>
                                </li>
                                <li class="js-fliter">
                                    <a href="#" id="graduate">
                                        <span class="icon icon-cap"></span>
                                        <span class="text">Graduate</span>
                                    </a>
                                </li>
                                <li class="js-fliter">
                                    <a href="#" id="mba">
                                        <span class="icon icon-tie"></span>
                                        <span class="text">MBA</span>
                                    </a>
                                </li>
                                <li class="js-fliter">
                                    <a href="#" id="professional">
                                        <span class="icon icon-businessman"></span>
                                        <span class="text">Professional</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="bs-select visible-xs">
                                <select class="js-select js-typfliter">
                                    <option value="all">All</option>
                                    <option value="graduate">Graduate</option>
                                    <option value="mba">MBA</option>
                                    <option value="professional">Professional</option>
                                </select>
                            </div>
                            <div class="rhs hidden-xs">
                                <div class="bs-filter-dropdown typ-sort">
                                    <select class="js-select-filter" id="selectFilter">
                                        <option value="all">Combo Courses</option>
                                        <option value="moisturiser">Pricing Plans</option>
                                        <option value="facescrub">Course &amp; webinar</option>
                                        <option value="superfoods">Usage guide</option>
                                    </select>
                                    <span class="icon-filter icon icon-sortdown"></span>
                                </div>
                            </div>
                            <div class="action-wrap visible-xs">
                                <button class="btn-icon" type="button"><span class="icon icon-sortdown"></span></button>
                            </div>
                        </div>
                        <ul class="row lyt-course">
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course.jpg" alt="Image" class="js-img">
                                        <span class="cm-tag">Bestseller</span>
                                        <span class="cm-rating"><i class="icon icon-star"></i></span>
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                            <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Certification in Equity Valuation</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">6,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Certification in Applied FSA</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">8,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course3.jpg" alt="Image" class="js-img">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Finance Essentials - Banking and Markets</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">10,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course4.jpg" alt="Image" class="js-img">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Certification in Credit Analysis</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">6,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course.jpg" alt="Image" class="js-img">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="graduate" class="tag-link">Graduate</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Certification in Investment Banking</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">8,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                            <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Certification in Investment Banking</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">15,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course3.jpg" alt="Image" class="js-img">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                            <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Certification in Investment Banking</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">6,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-4 item js-filteritem active">
                                <div class="bs-course">
                                    <div class="img-wrap js-addto">
                                        <img src="assets/images/products/course4.jpg" alt="Image" class="js-img">
                                    </div>
                                    <div class="info-wrap">
                                        <ul class="mod-tags">
                                            <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                            <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                            <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                        </ul>
                                        <h2 class="course-name">Certification in Investment Banking</h2>
                                        <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                        <div class="action-wrap">
                                            <div class="bs-currency">
                                                <div class="og-price">
                                                    <span class="icon icon-inr">Rs.</span>
                                                    <span class="value">10,000</span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                        </div>
                                    </div>
                                    <a href="course-details1.html" class="link"></a>
                                </div>
                            </li>
                        </ul>
                        <div class="act-wrap view-more-btn">
                            <button type="button" class="btn btn-link with-line">Load More</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-desc">
                    <div class="bs-ad-banner">
                        <div class="container">
                            <div class="img-wrap js-addto">
                                <img src="assets/images/ad-banner.jpg" class="js-img">
                            </div>
                            <div class="element-wrap">
                                <span class="net"></span>
                            </div>
                            <div class="info-wrap">
                                <h3 class="title">Your Personal and Professional Goals with Finshiksha </h3>
                                <div class="desc">
                                    <p>Join now to receive personalized recommendations from finshiksha</p>
                                </div>
                                <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-right">Students Testimonials</h2>
                    </div>
                    <div class="sec-cont">
                        <!-- testimonial start -->
                        <div class="bs-testimonials">
    <div class="swiper-container testimonial-img">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img2.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img3.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img4.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img5.jpg" alt="testimonial images">
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-container testimonial-decs">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide-count">
        <span class="active">01</span> / <span class="total">03</span>
    </div>
    <div class="slideNav">
        <div class="swiper-button-next"></div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
                        <!-- testimonial end -->
                    </div>
                </div>
            </div>
            <!-- blog start -->
            <div class="bs-section">
    <div class="container">
        <div class="sec-head">
            <h2 class="sec-title element-left">Read Our Blogs</h2>
        </div>
        <div class="sec-desc swiper-container js-blog-swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img7.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog typ2 js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img8.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/team.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
            </div>
            <div class="act-wrap view-more-btn">
                <button type="button" class="btn btn-link with-line">View all blogs</button>
            </div>
        </div>
    </div>
</div>
            <!-- blog end -->
        </div>
    </main>

    <?php $this->load->view('include/footer.php'); ?>

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>