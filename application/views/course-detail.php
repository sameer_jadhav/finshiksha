
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('include/head.php'); ?>

  <body class="course-details">
    <?php $this->load->view('include/header.php'); ?>

    <main>
      <div class="main">
        <!-- banner start -->
        <div class="bs-banner typ-gradient typ-details">
    <div class="banner-cont js-addto">
        <div class="container">
            <div class="banner-info">
                <span class="cm-tag">Bestseller</span>
                <!-- <span class="cm-rating"><i class="icon icon-star"></i></span> -->
                <!-- <h2 class="title typ-sm">Course</h2> -->
                <h3 class="title">Certification in Equity Valuation</h3>
                <div class="desc typ2">
                    <p>Learn to build Valuation Models like an analyst</p>
                </div>
                <!-- <strong class="author">By <span class="italic">Abhinav Gupta</span><a href="javascript:void(0)" class="lin icon icon-in"></a></strong>
                <ul class="mod-icon-text">
                    <li>
                        <span class="icon icon-language"></span>
                        <span class="text">English / Hindi</span>
                    </li>
                    <li>
                        <span class="icon icon-idea"></span>
                        <span class="text">Intermediate</span>
                    </li>
                    <li>
                        <span class="icon icon-idea"></span>
                        <span class="text">Free</span>
                    </li>
                    <li>
                        <span class="icon icon-calendar"></span>
                        <span class="text">Sat 16th May</span>
                    </li>
                    <li>
                        <span class="icon icon-clock"></span>
                        <span class="text">5pm - 6pm (1 Hours)</span>
                    </li>
                </ul> -->
            </div>
        </div>
        <img src="assets/images/course-detail.jpg" alt="banner1" class="js-img">
        <button type="button" data-toggle="modal" data-target="#video" class="btn btn-video js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg"></button>
    </div>
</div>
        <!-- banner end -->
        <div class="bs-section typ-action">
          <div class="sec-desc">
            <div class="container">
              <div class="bs-action-band">
                <ul class="mod-tags">
                  <li><a href="#" class="tag-link">mba</a></li>
                  <li><a href="#" class="tag-link">graduate</a></li>
                  <li><a href="#" class="tag-link">working professional</a></li>
                  <li><a href="#" class="tag-link">financial markets</a></li>
                </ul>
                <div class="action-wrap">
                  <div class="bs-currency">
                    <div class="old-price">
                      <span class="icon icon-inr">Rs.</span>
                      <span class="value">12,000</span>
                      
                  </div>
                    <div class="og-price">
                      <span class="icon icon-inr">Rs.</span>
                      <span class="value">7,400</span>
                      <span class="qty">/ Incl Taxes</span>
                    </div>
                    
                  </div>
                  <button
                    type="button"
                    class="btn btn-default btn-with-icon btn-action"
                  >
                    Buy Course
                    <span class="icon-wrap"
                      ><i class="icon icon-rightArrow"></i
                    ></span>
                  </button>
                  <div class="action-call">
                    <button type="button" class="btn-call">
                      <span class="icon icon-call"></span
                      ><span class="text">Request a Call</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="bs-section">
          <div class="sec-desc">
            <div class="bs-filter jsFilter">
                  <div class="container">
                    <ul class="filter">
                      <li class="active">
                        <a class="filter-link jsScroll-trig" href="#overview">Overview</a>
                      </li>
                      <li>
                        <a class="filter-link jsScroll-trig" href="#instructor">Instructor</a>
                      </li>
                      <li>
                        <a class="filter-link jsScroll-trig" href="#benefits">Benefits</a>
                      </li>
                      <li>
                        <a class="filter-link jsScroll-trig" href="#requirements">Requirements</a>
                      </li>
                      <li>
                        <a class="filter-link jsScroll-trig" href="#content">Content</a>
                      </li>
                      <li>
                        <a class="filter-link jsScroll-trig" href="#brochure">Brochure</a>
                      </li>
                      <li>
                        <a class="filter-link jsScroll-trig" href="#certificate">Certificate</a>
                      </li>
                      <li>
                        <a class="filter-link jsScroll-trig" href="#faq">FAQs</a>
                      </li>
                    </ul>
                  </div>
            </div>
            <div class="container">
                <div class="js-placed visible-xs"></div>
                <div class="lyt-detail">
                  <div class="lhs">
                    <div class="sub-sec">
                      <h3 class="subtitle element-left">What Will You Learn</h3>
                      <ul class="bs-list typ-hash">
                        <li>Building a detailed Valuation Model for 3 companies</li>
                        <li>Understanding on 3 different sectors for valuation</li>
                        <li>Practical Applications of DCF and Relative Valuations</li>
                        <li>Model Building like a sell side analyst</li>
                        <li>Analyzing and interpreting industry drivers</li>
                      </ul>
                    </div>
                    <div class="sub-sec" id="overview">
                      <h3 class="subtitle element-left">Overview</h3>
                      <p>Certification in Equity Valuation aims to give the candidate an introduction to the world of Equity Valuation and Financial Model Building. This program aims to equip analysts with the knowledge of evaluating a company using an appropriate valuation method, and the ability to build a financial model to do so. It also gives an insight into key inputs into the valuation process, and various valuation methodologies.</p>
                    </div>
                    <div class="sub-sec" id="benefits">
                      <h3 class="subtitle element-left">Benefits</h3>
                      <ul class="bs-list typ-numeric">
                        <li>You will be able to build a financial model from scratch</li>
                        <li>You will have skills that someone with a 1 year experience would have</li>
                        <li>The live project and certifications can both be mentioned in resume</li>
                        <li>Recognition across campuses and industry</li>
                      </ul>
                    </div>
                    <div class="sub-sec" id="requirements">
                      <h3 class="subtitle element-left">Requirements</h3>
                      <ul class="bs-list typ-bullet">
                        <li>Working understanding of Financial Statements</li>
                        <li>Understanding of Key Financial Concepts such as Present Value, Risk and Reward</li>
                        <li>Internet Connection with 1mbps speed</li>
                      </ul>
                    </div>
                  </div>
                  <div class="side-bar">
                    <div class="js-move">
                        <div class="mod-course">
                          <h3 class="title">This Course Includes</h3>
                          <ul>
                            <li>
                              <div class="mod-icon-text">
                                <span class="img img-video"></span>
                                <span class="text">35 videos</span>
                              </div>
                            </li>
                            <li>
                              <div class="mod-icon-text">
                                <span class="img img-hours"></span>
                                <span class="text">30 hours of effort needed / 10 hours video time</span>
                              </div>
                            </li>
                            <li>
                              <div class="mod-icon-text">
                                <span class="img img-materials"></span>
                                <span class="text">Downloadable excel templates for 2 companies</span>
                              </div>
                            </li>
                            <li>
                              <div class="mod-icon-text">
                                <span class="img img-smallTest"></span>
                                <span class="text">Self Assessment Tests</span>
                              </div>
                            </li>
                            <li>
                              <div class="mod-icon-text">
                                <span class="img img-materials"></span>
                                <span class="text">Live Project with FinShiksha - building a valuation model</span>
                              </div>
                            </li>
                            <li>
                              <div class="mod-icon-text">
                                <span class="img img-fullTest"></span>
                                <span class="text">Certification Test</span>
                              </div>
                            </li>
                            <li>
                              <div class="mod-icon-text">
                                <span class="img img-fullTest"></span>
                                <span class="text">1 year Access</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div class="act-btn">
                        <button type="button" class="btn btn-preview">
                          <span class="img img-downlod"></span>
                          <span class="text">Preview</span>
                        </button>
                        <button type="button" class="btn btn-download">
                          <span class="img img-downlod"></span>
                          <span class="text">Download Course Brochure</span>
                        </button>
                      </div>
                    </div>
                    <div class="mod-combo">
                      <h3 class="title">Suggested Combo Courses</h3>
                      <ul>
                        <li>
                          <div class="combo">
                            <ul class="mod-tags">
                              <li>
                                <a href="#" class="tag-link">Combo Course</a>
                              </li>
                            </ul>
                            <h4 class="title">
                              Investment Banking + Credit Analysis Finance
                            </h4>
                            <div class="bs-currency">
                              <div class="og-price">
                                <span class="icon icon-inr">Rs.</span>
                                <span class="value">14,300</span>
                              </div>
                              <div class="old-price">
                                <span class="icon icon-inr">Rs.</span>
                                <span class="value">12,000</span>
                              </div>
                            </div>
                            <a href="#" class="btn btn-link">View Course</a>
                            <a class="link" href="#"></a>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
            <div class="container">
              <div class="sub-sec" id="instructor">
                <h3 class="subtitle element-left">About the Instructor</h3>
                <ul class="instructor-wrap">
                  <li>
                    <div class="mod-instructor">
                      <div class="img-wrap">
                        <img src="assets/images/img2.jpg" alt="img6">
                      </div>
                      <div class="rel-info">
                        <h4 class="name">Ajay Arora <a href="javascript:void(0)" class="lin icon icon-in"></a></h4>
                        <label class="related-info">MBA &nbsp;|&nbsp; Professional</label>
                      </div>
                      <div class="desc">
                        <p>My education, career, and even personal life have been molded by one simple principle; well designed information resonates</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="mod-instructor">
                      <div class="img-wrap">
                        <img src="assets/images/img2.jpg" alt="img6">
                      </div>
                      <div class="rel-info">
                        <h4 class="name">Ajay Arora <a href="javascript:void(0)" class="lin icon icon-in"></a></h4>
                        <label class="related-info">MBA &nbsp;|&nbsp; Professional</label>
                      </div>
                      <div class="desc">
                        <p>My education, career, and even personal life have been molded by one simple principle; well designed information resonates</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="sub-sec last" id="content">
                <h3 class="subtitle element-left">Course Content</h3>
                <div class="bs-accordian typ2">
                  <div class="panel-group" id="course" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#course" href="#a" class="collapsed">
                              <span class="icon icon-plus"></span>
                              <span class="title">Introduction to this Course</span>
                              <span class="rel-info">
                                <span class="text">8 lectures</span>
                                <span class="time">21:00</span>
                              </span>
                          </a>
                        </div>
                        <div id="a" class="panel-collapse collapse" role="tabpanel" style="height: 0px;">
                          <div class="panel-body">
                            <ul class="lectures-list">
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">
                                      <span>Unit Objectives</span>
                                      <span class="small">Additional Internet Resources for Section 1</span>
                                    </span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-document"></span>
                                    <span class="text">Note About Setting up Front-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Setting Up Font-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">03:11</span>
                                  </div>
                              </li>
                              <li>
                                <div class="lhs">
                                  <span class="icon icon-document"></span>
                                  <span class="text">Note About Introduction to the Web</span>
                                </div>
                                <div class="rhs">
                                  <span class="time">00:11</span>
                                </div>
                              </li>
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Introduction to the Web</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <a role="button" data-toggle="collapse" data-parent="#course" href="#b" class="collapsed">
                              <span class="icon icon-plus"></span>
                              <span class="title">Introduction to Front End Development</span>
                              <span class="rel-info">
                                <span class="text">5 lectures</span>
                                <span class="time">30:00</span>
                              </span>
                          </a>
                        </div>
                        <div id="b" class="panel-collapse collapse" role="tabpanel" style="height: 0px;">
                          <div class="panel-body">
                            <ul class="lectures-list">
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Unit Objectives</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-document"></span>
                                    <span class="text">Note About Setting up Front-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Setting Up Font-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">03:11</span>
                                  </div>
                              </li>
                              <li>
                                <div class="lhs">
                                  <span class="icon icon-document"></span>
                                  <span class="text">Note About Introduction to the Web</span>
                                </div>
                                <div class="rhs">
                                  <span class="time">00:11</span>
                                </div>
                              </li>
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Introduction to the Web</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#course" href="#c" class="collapsed">
                                <span class="icon icon-plus"></span>
                                <span class="title">Introduction to HTML</span>
                                <span class="rel-info">
                                  <span class="text">3 lectures</span>
                                  <span class="time">15:00</span>
                                </span>
                            </a>
                        </div>
                        <div id="c" class="panel-collapse collapse" role="tabpanel" style="height: 0px;">
                          <div class="panel-body">
                            <ul class="lectures-list">
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Unit Objectives</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-document"></span>
                                    <span class="text">Note About Setting up Front-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Setting Up Font-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">03:11</span>
                                  </div>
                              </li>
                              <li>
                                <div class="lhs">
                                  <span class="icon icon-document"></span>
                                  <span class="text">Note About Introduction to the Web</span>
                                </div>
                                <div class="rhs">
                                  <span class="time">00:11</span>
                                </div>
                              </li>
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Introduction to the Web</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#course" href="#d" class="collapsed">
                                <span class="icon icon-plus"></span>
                                <span class="title">Intermediate HTML</span>
                                <span class="rel-info">
                                  <span class="text">4 lectures</span>
                                  <span class="time">21:00</span>
                                </span>
                            </a>
                        </div>
                        <div id="d" class="panel-collapse collapse" role="tabpanel" style="height: 0px;">
                          <div class="panel-body">
                            <ul class="lectures-list">
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Unit Objectives</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-document"></span>
                                    <span class="text">Note About Setting up Front-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Setting Up Font-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">03:11</span>
                                  </div>
                              </li>
                              <li>
                                <div class="lhs">
                                  <span class="icon icon-document"></span>
                                  <span class="text">Note About Introduction to the Web</span>
                                </div>
                                <div class="rhs">
                                  <span class="time">00:11</span>
                                </div>
                              </li>
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Introduction to the Web</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#course" href="#e" class="collapsed">
                                <span class="icon icon-plus"></span>
                                <span class="title">Introduction to CSS</span>
                                <span class="rel-info">
                                  <span class="text">4 lectures</span>
                                  <span class="time">21:00</span>
                                </span>
                            </a>
                        </div>
                        <div id="e" class="panel-collapse collapse" role="tabpanel" style="height: 0px;">
                          <div class="panel-body">
                            <ul class="lectures-list">
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Unit Objectives</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-document"></span>
                                    <span class="text">Note About Setting up Front-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                              <li>
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Setting Up Font-End Developer Environment</span>
                                  </div>
                                  <div class="rhs">
                                    <span class="time">03:11</span>
                                  </div>
                              </li>
                              <li>
                                <div class="lhs">
                                  <span class="icon icon-document"></span>
                                  <span class="text">Note About Introduction to the Web</span>
                                </div>
                                <div class="rhs">
                                  <span class="time">00:11</span>
                                </div>
                              </li>
                              <li class="active">
                                  <div class="lhs">
                                    <span class="icon icon-triangle"></span>
                                    <span class="text">Introduction to the Web</span>
                                  </div>
                                  <div class="rhs">
                                    <button data-toggle="modal" data-target="#video" type="button" class="btn btn-secondary js-video-trigger" data-video="https://www.youtube.com/embed/-IjpeqpRfpg">Preview</button>
                                    <span class="time">01:40</span>
                                  </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="act-btn" id="brochure">
                  <button type="button" class="btn btn-download">
                    <span class="img img-downlod"></span>
                    <span class="text">Download Course Brochure</span>
                  </button>
                </div>
              </div>
            </div>
            <div class="sub-sec last" id="certificate">
              <div class="bs-certification">
                  <div class="container">
                      <div class="info-wrap">
                          <h3 class="title element-left">Certification</h3>
                          <div class="desc">
                              <p>The course will have an online assessment. The final Certification will be an open Online test, which would be untimed, and open book. We would try and keep the assessment as close to a real life situation as possible, where you will have all the tools that you would have in real life at your disposal.</p>
                              <p>In addition, the course also has an optional live project. This will help you apply the concepts learnt into a real world example. You will be asked to build a valuation model for an Indian firm from scratch.</p>
                          </div>
                      </div>
                      <div class="img-wrap js-addto">
                        <img src="assets/images/certificate.png" alt="certification" class="js-img">
                    </div>
                  </div>
              </div>
            </div>
            <div class="container" id="faq">
              <div class="sub-sec last">
                <h3 class="subtitle element-left">FAQs</h3>
                <a href="faq.html" class="btn btn-link with-line side-link hidden-xs">View all FAQ’a</a>
                <div class="bs-accordian">
                  <div
                    class="panel-group"
                    id="accordion"
                    role="tablist"
                    aria-multiselectable="true"
                  >
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <a
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion"
                          href="#collapseOne"
                          aria-expanded="false"
                          aria-controls="collapseOne"
                          class="collapsed"
                          >Will I receive the recording after the live webinar
                          is over? <span class="icon icon-plus"></span
                        ></a>
                      </div>
                      <div
                        id="collapseOne"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingOne"
                        aria-expanded="false"
                        style="height: 0px;"
                      >
                        <div class="panel-body">
                          We host more than 150,000 courses on our online
                          learning marketplace. Our marketplace model means we
                          do not own the copyright to the content of the
                          courses; the respective instructors own these rights.
                          On rare occasions, instructors may remove their
                          courses from our marketplace, or, Udemy may have to
                          remove a course from the platform for policy or legal
                          reasons. If this does happen to a course you're
                          enrolled in, please contact us and we'll be ready to
                          help
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingTwo">
                        <a
                          class="collapsed"
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion"
                          href="#collapseTwo"
                          aria-expanded="false"
                          aria-controls="collapseTwo"
                          >Where can I find the recording of the live
                          webinar?<span class="icon icon-plus"></span
                        ></a>
                      </div>
                      <div
                        id="collapseTwo"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingTwo"
                        aria-expanded="false"
                        style="height: 0px;"
                      >
                        <div class="panel-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high
                          life accusamus terry richardson ad squid. 3 wolf moon
                          officia aute, non cupidatat skateboard dolor brunch.
                          Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                          wolf moon tempor, sunt aliqua put a bird on it squid
                          single-origin coffee nulla assumenda shoreditch et.
                          Nihil anim keffiyeh helvetica, craft beer labore wes
                          anderson cred nesciunt sapiente ea proident. Ad vegan
                          excepteur butcher vice lomo. Leggings occaecat craft
                          beer farm-to-table, raw denim aesthetic synth nesciunt
                          you probably haven't heard of them accusamus labore
                          sustainable VHS.
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingThree">
                        <a
                          class="collapsed"
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion"
                          href="#collapseThree"
                          aria-expanded="false"
                          aria-controls="collapseThree"
                          >Will I receive the recording after the live webinar
                          is over?<span class="icon icon-plus"></span
                        ></a>
                      </div>
                      <div
                        id="collapseThree"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingThree"
                        aria-expanded="false"
                      >
                        <div class="panel-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high
                          life accusamus terry richardson ad squid. 3 wolf moon
                          officia aute, non cupidatat skateboard dolor brunch.
                          Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                          wolf moon tempor, sunt aliqua put a bird on it squid
                          single-origin coffee nulla assumenda shoreditch et.
                          Nihil anim keffiyeh helvetica, craft beer labore wes
                          anderson cred nesciunt sapiente ea proident. Ad vegan
                          excepteur butcher vice lomo. Leggings occaecat craft
                          beer farm-to-table, raw denim aesthetic synth nesciunt
                          you probably haven't heard of them accusamus labore
                          sustainable VHS.
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingFour">
                        <a
                          class="collapsed"
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion"
                          href="#collapseFour"
                          aria-expanded="false"
                          aria-controls="collapseFour"
                          >Will I receive the recording after the live webinar
                          is over?<span class="icon icon-plus"></span
                        ></a>
                      </div>
                      <div
                        id="collapseFour"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingFour"
                        aria-expanded="false"
                      >
                        <div class="panel-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high
                          life accusamus terry richardson ad squid. 3 wolf moon
                          officia aute, non cupidatat skateboard dolor brunch.
                          Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                          wolf moon tempor, sunt aliqua put a bird on it squid
                          single-origin coffee nulla assumenda shoreditch et.
                          Nihil anim keffiyeh helvetica, craft beer labore wes
                          anderson cred nesciunt sapiente ea proident. Ad vegan
                          excepteur butcher vice lomo. Leggings occaecat craft
                          beer farm-to-table, raw denim aesthetic synth nesciunt
                          you probably haven't heard of them accusamus labore
                          sustainable VHS.
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingFive">
                        <a
                          class="collapsed"
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion"
                          href="#collapseFive"
                          aria-expanded="false"
                          aria-controls="collapseFive"
                          >Will I receive the recording after the live webinar
                          is over?<span class="icon icon-plus"></span
                        ></a>
                      </div>
                      <div
                        id="collapseFive"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingFive"
                        aria-expanded="false"
                      >
                        <div class="panel-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high
                          life accusamus terry richardson ad squid. 3 wolf moon
                          officia aute, non cupidatat skateboard dolor brunch.
                          Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                          wolf moon tempor, sunt aliqua put a bird on it squid
                          single-origin coffee nulla assumenda shoreditch et.
                          Nihil anim keffiyeh helvetica, craft beer labore wes
                          anderson cred nesciunt sapiente ea proident. Ad vegan
                          excepteur butcher vice lomo. Leggings occaecat craft
                          beer farm-to-table, raw denim aesthetic synth nesciunt
                          you probably haven't heard of them accusamus labore
                          sustainable VHS.
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingSix">
                        <a
                          class="collapsed"
                          role="button"
                          data-toggle="collapse"
                          data-parent="#accordion"
                          href="#collapseSix"
                          aria-expanded="false"
                          aria-controls="collapseSix"
                          >Will I receive the recording after the live webinar
                          is over?<span class="icon icon-plus"></span
                        ></a>
                      </div>
                      <div
                        id="collapseSix"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingSix"
                        aria-expanded="false"
                      >
                        <div class="panel-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high
                          life accusamus terry richardson ad squid. 3 wolf moon
                          officia aute, non cupidatat skateboard dolor brunch.
                          Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                          wolf moon tempor, sunt aliqua put a bird on it squid
                          single-origin coffee nulla assumenda shoreditch et.
                          Nihil anim keffiyeh helvetica, craft beer labore wes
                          anderson cred nesciunt sapiente ea proident. Ad vegan
                          excepteur butcher vice lomo. Leggings occaecat craft
                          beer farm-to-table, raw denim aesthetic synth nesciunt
                          you probably haven't heard of them accusamus labore
                          sustainable VHS.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="act-btn visible-xs">
                  <a href="faq.html" class="btn btn-link with-line">View all FAQ’a</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="bs-section typ-lyt">
            <div class="container">
                <div class="sec-head">
                    <h2 class="sec-title element-right">Recommended Courses</h2>
                </div>
                <div class="sec-desc">
                    <div class="row lyt-course">
                        <div class="col-md-4 col-sm-4">
                            <div class="bs-course">
                                <div class="img-wrap js-addto">
                                    <img src="assets/images/products/course4.jpg" alt="Image" class="js-img">
                                    <span class="cm-tag">Bestseller</span>
                                </div>
                                <div class="info-wrap">
                                    <ul class="mod-tags">
                                        <li><a href="#" class="tag-link">EV</a></li>
                                        <li><a href="#" class="tag-link">IB,EV</a></li>
                                        <li><a href="#" class="tag-link">Credit</a></li>
                                    </ul>
                                    <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                    <ul class="meta-wrap">
                                        <li>
                                            <p>The most comprehensive course on Valuation in India</p>
                                        </li>
                                        <li>
                                            <p>Live Project with FinShiksha</p>
                                        </li>
                                    </ul>
                                    <div class="action-wrap">
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">1120</span>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                    </div>
                                </div>
                                <a href="#" class="link"></a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="bs-course">
                                <div class="img-wrap js-addto">
                                  <img src="assets/images/products/course3.jpg" alt="Image" class="js-img">
                                    <span class="cm-tag">Bestseller</span>
                                    <span class="cm-rating"><i class="icon icon-star"></i></span>
                                </div>
                                <div class="info-wrap">
                                    <ul class="mod-tags">
                                        <li><a href="#" class="tag-link">mba</a></li>
                                        <li><a href="#" class="tag-link">working professional</a></li>
                                        <li><a href="#" class="tag-link">combo course</a></li>
                                    </ul>
                                    <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                    <ul class="meta-wrap">
                                        <li>
                                            <p>Certification in credit analysis</p>
                                        </li>
                                        <li>
                                            <p>Certification in investment banking</p>
                                        </li>
                                        <li>
                                            <p>Certification in lorem ipsum</p>
                                        </li>
                                    </ul>
                                    <div class="action-wrap">
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">1120</span>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                    </div>
                                </div>
                                <a href="#" class="link"></a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="bs-course last">
                                <div class="img-wrap js-addto">
                                  <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                    <span class="cm-tag">Bestseller</span>
                                </div>
                                <div class="info-wrap">
                                    <ul class="mod-tags">
                                        <li><a href="#" class="tag-link">mba</a></li>
                                        <li><a href="#" class="tag-link">working professional</a></li>
                                        <li><a href="#" class="tag-link">combo course</a></li>
                                    </ul>
                                    <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                    <ul class="meta-wrap">
                                        <li>
                                            <p>Certification in credit analysis</p>
                                        </li>
                                        <li>
                                            <p>Certification in investment banking</p>
                                        </li>
                                        <li>
                                            <p>Certification in lorem ipsum</p>
                                        </li>
                                    </ul>
                                    <div class="action-wrap">
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">1120</span>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                    </div>
                                </div>
                                <a href="#" class="link"></a>
                            </div>
                        </div>
                    </div>
                    <div class="act-wrap view-more-btn">
                        <button type="button" class="btn btn-link with-line">View All Courses</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="bs-section">
            <div class="container">
                <div class="sec-head">
                    <h2 class="sec-title element-left">Upcoming Webinar</h2>
                </div>
                <div class="sec-desc">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="bs-webinar typ-horizontal">
                            <div class="img-wrap">
                                <img src="assets/images/img2.jpg" alt="webinar">
                            </div>
                            <div class="info-wrap">
                                <ul class="mod-tags">
                                    <li><a href="#" class="tag-link">Banking</a></li>
                                    <li><a href="#" class="tag-link">Finance</a></li>
                                    <li><a href="#" class="tag-link">Credit</a></li>
                                </ul>
                                <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                <label class="name">
                                    <span>By</span>
                                    <span class="italic">Abhinav Gupta</span>
                                    <a href="#" class="lin icon icon-in"></a>
                                </label>
                                <div class="para">
                                    <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                </div>
                                <ul class="mod-icon-text">
                                    <li>
                                        <span class="icon icon-idea"></span>
                                        <span class="text">Free</span>
                                    </li>
                                    <li>
                                        <span class="icon icon-calendar"></span>
                                        <span class="text">Sat 16th May</span>
                                    </li>
                                    <li>
                                        <span class="icon icon-clock"></span>
                                        <span class="text">5pm - 6pm (1 Hours)</span>
                                    </li>
                                </ul>
                                <div class="action-wrap">
                                    <button type="button" class="btn btn-default btn-with-icon">
                                        Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                    </button>
                                    <div class="bs-currency">
                                        <div class="og-price">
                                            <span class="icon icon-inr">Rs.</span>
                                            <span class="value">14,300</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="link" href="webinar-details.html"></a>
                            <span data-filter="past"></span>
                            <span data-filter="all"></span>
                        </div>
                        </div>
                    </div>
                    <div class="act-wrap view-more-btn">
                        <button type="button" class="btn btn-link with-line">View All Webinar</button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </main>

    
      <!-- footer start -->
      <?php $this->load->view('include/footer.php'); ?>
      <!-- footer end -->
    

    <!-- popup start -->
<?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
  </body>
</html>
