
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('include/head.php'); ?>

<body class="pg-corporate">
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <section>
                <div class="bs-section typ-corporate-bg">
                    <div class="bs-corporate-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-xs-12">
                                    <div class="sec-head">
                                        <h2 class="sec-title element-left">Our Hiring Partners</h2>
                                        <p>They said, finance is complicated. They said, teaching finance is difficult, learning even more so. They said this is the most boring subject on earth. They also kept  teaching the same old slides and content for ages</p>
                                    </div>
                                </div>
                                <div class="col-md-7 col-xs-12">
                                    <div class="sec-cont">
                                        <div class="hiring-partners">
                                            <ul>
                                                <li>
                                                    <div class="img-wrap typ-rev">
                                                        <img src="assets/images/logos/amazon.png" alt="amazon">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img-wrap">
                                                        <img src="assets/images/logos/loreal.png" alt="loreal">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img-wrap">
                                                        <img src="assets/images/logos/google.png" alt="google">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li>
                                                    <div class="img-wrap typ-rev">
                                                        <img src="assets/images/logos/deloitte.png" alt="deloitte">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img-wrap">
                                                        <img src="assets/images/logos/db.png" alt="db">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img-wrap">
                                                        <img src="assets/images/logos/et.png" alt="et">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li>
                                                    <div class="img-wrap">
                                                        <img src="assets/images/logos/amazon.png" alt="amazon">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img-wrap">
                                                        <img src="assets/images/logos/loreal.png" alt="loreal">
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="row corporate-list">
                                <li class="col-md-6 col-xs-12 item">
                                    <p>They said, finance is complicated. They said, teaching finance is difficult, learning even more so.</p>
                                </li>
                                <li class="col-md-6 col-xs-12 item">
                                    <p>They said, finance is complicated. They said, teaching finance is difficult, learning even more so.</p>
                                </li>
                                <li class="col-md-6 col-xs-12 item">
                                    <p>They said, finance is complicated. They said, teaching finance is difficult, learning even more so.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-alumni">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Our Alumni</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                            <div class="js-alumni slider-wrapper">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p1.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/deloitte.png" alt="logo">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p2.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/amazon.png" alt="amazon">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p3.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/loreal.png" alt="loreal">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p4.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/google.png" alt="google">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/img6.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/amazon.png" alt="amazon">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p1.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/deloitte.png" alt="logo">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p2.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/loreal.png" alt="loreal">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p3.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/deloitte.png" alt="logo">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/p4.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/google.png" alt="google">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="bs-alumni typ-vertical">
                                                <div class="img-wrap js-addto">
                                                    <img src="assets/images/img6.jpg" alt="Alumni 1" class="js-img">
                                                </div>
                                                <div class="info-wrap">
                                                    <h3 class="name">Rahul Agrawal</h3>
                                                    <p class="designation">Financial Advisor</p>
                                                </div>
                                                <div class="logo-wrap">
                                                    <img src="assets/images/logos/deloitte.png" alt="logo">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slideNav">
                                    <div class="swiper-button-next">
                                        <i class="icon icon-rightArrow"></i>
                                    </div>
                                    <div class="swiper-button-prev">
                                        <i class="icon icon-rightArrow"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Corporate Trainings</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                            <div class="lyt-training">
                                <ul class="training-wrap">
                                    <li class="training-item">
                                        <div class="mod-training">
                                            <div class="img-wrap">
                                                <img src="assets/images/logos/google.svg" alt="google">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="training-title">New Joinee Trainings</h3>
                                                <div class="training-desc">
                                                    <p>Training is teaching, or developing in oneself or others, any skills and knowledge or fitness that relate to specific useful competencies..</p>
                                                </div>
                                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#training">Read More</button>
                                                <div class="count-wrap">
                                                    <span class="count">5000 +</span> Trained
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="training-item">
                                        <div class="mod-training">
                                            <div class="img-wrap">
                                                <img src="assets/images/logos/salesforce.svg" alt="Salesforce">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="training-title">New Joinee Trainings</h3>
                                                <div class="training-desc">
                                                    <p>Training is teaching, or developing in oneself or others, any skills and knowledge or fitness that relate to specific useful competencies..</p>
                                                </div>
                                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#training">Read More</button>
                                                <div class="count-wrap">
                                                    <span class="count">5000 +</span> Trained
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="training-item">
                                        <div class="mod-training">
                                            <div class="img-wrap">
                                                <img src="assets/images/logos/dbs-bank.svg" alt="DBS Bank">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="training-title">New Joinee Trainings</h3>
                                                <div class="training-desc">
                                                    <p>Training is teaching, or developing in oneself or others, any skills and knowledge or fitness that relate to specific useful competencies..</p>
                                                </div>
                                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#training">Read More</button>
                                                <div class="count-wrap">
                                                    <span class="count">5000 +</span> Trained
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="action-wrap">
                                    <button type="button" class="btn btn-link with-line">View More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-section typ-testimonial-v2">
                    <div class="container">
                        <div class="sec-cont">
                            <!-- testimonial start -->
                            <div class="bs-testimonials typ-v2">
    <h2 class="sec-title">Testimonials</h2>
    <div class="swiper-container testimonial-decs">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-container testimonial-img">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img2.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img3.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img4.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img5.jpg" alt="testimonial images">
                </div>
            </div>
        </div>
    </div>
    <div class="slide-count">
        <span class="active">01</span> / <span class="total">03</span>
    </div>
    <div class="slideNav">
        <div class="swiper-button-next"></div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
                            <!-- testimonial end -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

    
        <!-- footer start -->
    <?php $this->load->view('include/footer.php'); ?>  
        <!-- footer end -->
    
    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>