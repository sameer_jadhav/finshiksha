<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('include/head.php'); ?>

<body>
    
<?php $this->load->view('include/header.php'); ?>
    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-slider js-home-banner">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="banner-cont cm-bg-green">
                    <div class="container">
                        <div class="banner-info">
                            <h2 class="title">Learn Skills by Our Courses & Enhance Your Career</h2>
                            <div class="desc">
                                <p>Build skills with courses, certificates, and degrees online from world-class universities and companies</p>
                            </div>
                            <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                        </div>
                        <div class="banner-img js-addto">
                            <img src="assets/images/banner/home-banner-1.jpg" alt="banner1" class="js-img">
                        </div>
                        <div class="banner-element">
                            <span class="net"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="banner-cont cm-bg-blue">
                    <div class="container">
                        <div class="banner-info">
                            <h2 class="title">Starts from <span class="cm-line-break rupee">&#8377; 11,500</span></h2>
                            <div class="desc">
                                <p>On demand video lectures subjects like business, computer science, data science, language learning</p>
                            </div>
                            <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                        </div>
                        <div class="banner-img js-addto">
                            <img src="assets/images/banner/home-banner-2.jpg" alt="banner1" class="js-img">
                        </div>
                        <div class="banner-element">
                            <span class="net"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="banner-cont typ-gradient js-addto">
                    <div class="container">
                        <div class="banner-info">
                            <h2 class="title">Learn Skills by Our Courses & Enhance Your Career</h2>
                            <div class="desc">
                                <p>Build skills with courses, certificates, and degrees online from world-class universities and companies</p>
                            </div>
                            <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                        </div>
                        <!-- <div class="banner-img "> -->
                        <img src="assets/images/banner/home-banner-3.jpg" alt="banner1" class="js-img">
                        <!-- </div> -->
                        <!-- <div class="banner-element">
                            <span class="net"></span>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>
            <!-- banner end -->
            <div class="bs-section typ-bg-green">
                <div class="sec-cont">
                    <div class="container">
                        <div class="bs-filter">
                            <ul class="filter-with-icon hidden-xs">
                                <li class="js-fliter active">
                                    <a href="#" id="graduate">
                                        <span class="icon icon-cap"></span>
                                        <span class="text">Graduate</span>
                                    </a>
                                </li>
                                <li class="js-fliter">
                                    <a href="#" id="mba">
                                        <span class="icon icon-tie"></span>
                                        <span class="text">MBA</span>
                                    </a>
                                </li>
                                <li class="js-fliter">
                                    <a href="#" id="professional">
                                        <span class="icon icon-businessman"></span>
                                        <span class="text">Professional</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="bs-select visible-xs">
                                <select class="js-select js-typfliter">
                                    <option value="graduate">Graduate</option>
                                    <option value="mba">MBA</option>
                                    <option value="professional">Professional</option>
                                </select>
                            </div>
                            <div class="rhs">
                                <a href="course-listing.html" class="btn btn-link with-line">All Courses</a>
                            </div>
                        </div>
                        <div class="swiper-container swiper-course js-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="bs-course-mrkt js-addto js-equalize">
                                        <div class="img-wrap">
                                            <img class="js-img" src="assets/images/img7.jpg" alt="course">
                                        </div>
                                        <div class="info-wrap">
                                            <label class="lbl">EV + Credit + Finance</label>
                                            <h3 class="title">
                                                <span class="big">70<em>%</em></span>
                                                <span class="cm-line-break">of MBA Students</span>
                                                <span class="cm-line-break">take this course</span>
                                            </h3>
                                            <button type="button" class="btn btn-icon"><i class="icon  icon-rightArrow"></i></button>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                                <div class="swiper-slide js-filteritem active">
                                    <div class="bs-course js-item">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/products/course.jpg" alt="Image" class="js-img">
                                            <span class="cm-tag">Bestseller</span>
                                            <span class="cm-rating"><i class="icon icon-star"></i></span>
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                                <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                                <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                            </ul>
                                            <h2 class="course-name">Certification in Equity Valuation</h2>
                                            <ul class="meta-wrap">
                                                <li>
                                                    <p>Certification in credit analysis</p>
                                                </li>
                                                <li>
                                                    <p>Certification in investment banking</p>
                                                </li>
                                                <li>
                                                    <p>Certification in lorem ipsum</p>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">6,000</span>
                                                    </div>
                                                    <div class="old-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">12,000</span>
                                                    </div>
                                                </div>
                                                <button type="button" onclick="buyCourse('396146','6')" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                                <div class="swiper-slide js-filteritem active">
                                    <div class="bs-course js-item">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                                <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                            </ul>
                                            <h2 class="course-name">Certification in Applied FSA</h2>
                                            <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">7,500</span>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                                <div class="swiper-slide js-filteritem active">
                                    <div class="bs-course js-item">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/products/course3.jpg" alt="Image" class="js-img">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">mba</a></li>
                                                <li><a href="#" class="tag-link">working professional</a></li>
                                                <li><a href="#" class="tag-link">combo course</a></li>
                                            </ul>
                                            <h2 class="course-name">Finance Essentials - Banking and Markets</h2>
                                            <ul class="meta-wrap">
                                                <li>
                                                    <p>Certification in credit analysis</p>
                                                </li>
                                                <li>
                                                    <p>Certification in investment banking</p>
                                                </li>
                                                <li>
                                                    <p>Certification in lorem ipsum</p>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">10,000</span>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                                <div class="swiper-slide js-filteritem active">
                                    <div class="bs-course js-item">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/products/course4.jpg" alt="Image" class="js-img">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                                <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                            </ul>
                                            <h2 class="course-name">Certification in Credit Analysis</h2>
                                            <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">6,000</span>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                                <div class="swiper-slide js-filteritem active">
                                    <div class="bs-course js-item">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/products/course.jpg" alt="Image" class="js-img">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" data-filter="graduate" class="tag-link">Graduate</a></li>
                                                <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                            </ul>
                                            <h2 class="course-name">Certification in Investment Banking</h2>
                                            <ul class="meta-wrap">
                                                <li>
                                                    <p>Certification in credit analysis</p>
                                                </li>
                                                <li>
                                                    <p>Certification in investment banking</p>
                                                </li>
                                                <li>
                                                    <p>Certification in lorem ipsum</p>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">6,000</span>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                                <div class="swiper-slide js-filteritem active">
                                    <div class="bs-course js-item">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/products/course2.jpg" alt="Image" class="js-img">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                                <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                            </ul>
                                            <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                            <ul class="meta-wrap">
                                                <li>
                                                    <p>The most comprehensive course on Valuation in India</p>
                                                </li>
                                                <li>
                                                    <p>Live Project with FinShiksha</p>
                                                </li>
                                                <li>
                                                    <p>Build Valuation Model on 3 different listed companies</p>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">7,500</span>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                                <div class="swiper-slide js-filteritem active">
                                    <div class="bs-course js-item">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/products/course.jpg" alt="Image" class="js-img">
                                            <span class="cm-tag">Bestseller</span>
                                            <span class="cm-rating"><i class="icon icon-star"></i></span>
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" data-filter="mba" class="tag-link">mba</a></li>
                                                <li><a href="#" data-filter="professional" class="tag-link">working professional</a></li>
                                                <li><a href="#" data-filter="all" class="tag-link">combo course</a></li>
                                            </ul>
                                            <h2 class="course-name">Investment Banking + Credit Analysis</h2>
                                            <ul class="meta-wrap">
                                                <li>
                                                    <p>Certification in credit analysis</p>
                                                </li>
                                                <li>
                                                    <p>Certification in investment banking</p>
                                                </li>
                                                <li>
                                                    <p>Certification in lorem ipsum</p>
                                                </li>
                                            </ul>
                                            <div class="action-wrap">
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">6,000</span>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default btn-with-icon btn-action">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                        <a href="course-details1.html" class="link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slideNav">
                                <div class="swiper-button-next"></div>
                                <div class="swiper-pagination"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        </div>
                        <div class="act-btn visible-xs">
                            <a href="course-listing.html" class="btn btn-link with-line">All Courses</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-right">Webinar Events</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="lyt-webinar dash-bor">
                            <h3 class="subtitle">Upcoming Webinars</h3>
                            <ul class="webinar-list">
                                <li class="item">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img6.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Free</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                                <!-- <li>
                                                    <span class="icon icon-friend"></span>
                                                    <span class="text">14356 Participants</span>
                                                </li> -->
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                    <div class="old-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">12,000</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="bs-webinar typ-horizontal">
                                        <div class="img-wrap">
                                            <img src="assets/images/img2.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="para">
                                                <p>Equity Valuation Primer aims to give the candidate an introduction..</p>
                                            </div>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-idea"></span>
                                                    <span class="text">Premium</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                                <!-- <li>
                                                    <span class="icon icon-friend"></span>
                                                    <span class="text">14356 Participants</span>
                                                </li> -->
                                            </ul>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-default btn-with-icon">
                                                    Buy Course <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                                </button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="lyt-webinar dash-bor">
                            <h3 class="subtitle">Past Webinars</h3>
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img3.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                    <div class="old-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">12,000</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img5.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="bs-webinar">
                                        <div class="img-wrap">
                                            <img src="assets/images/img4.jpg" alt="webinar">
                                        </div>
                                        <div class="info-wrap">
                                            <ul class="mod-tags">
                                                <li><a href="#" class="tag-link">Banking</a></li>
                                                <li><a href="#" class="tag-link">Finance</a></li>
                                                <li><a href="#" class="tag-link">Credit</a></li>
                                            </ul>
                                            <h2 class="title">Investment Banking + Credit Analysis Finance</h2>
                                            <ul class="mod-icon-text">
                                                <li>
                                                    <span class="icon icon-calendar"></span>
                                                    <span class="text">Sat 16th May</span>
                                                </li>
                                                <li>
                                                    <span class="icon icon-clock"></span>
                                                    <span class="text">5pm - 6pm (1 Hours)</span>
                                                </li>
                                            </ul>
                                            <label class="name">
                                                <span>By</span>
                                                <span class="italic">Abhinav Gupta</span>
                                                <a href="#" class="lin icon icon-in"></a>
                                            </label>
                                            <div class="action-wrap">
                                                <button type="button" class="btn btn-secondary">Buy Recording</button>
                                                <div class="bs-currency">
                                                    <div class="og-price">
                                                        <span class="icon icon-inr">Rs.</span>
                                                        <span class="value">14,300</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="link" href="webinar-details.html"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="act-wrap view-more-btn">
                                <a href="webinar-listing.html" class="btn btn-link with-line">all webinars</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-head">
                    <div class="container">
                        <h2 class="sec-title">Best Plans for Webinar</h2>
                    </div>
                </div>
                <div class="sec-desc">
                    <div class="lyt-plan typ-bg">
                        <div class="container">
                            <ul>
                                <li>
                                    <div class="bs-plan">
                                        <div class="img-wrap">
                                            <span class="icon icon-leaf"></span>
                                        </div>
                                        <h2 class="title">Single Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">500</span>
                                                <span class="qty">onwards</span>
                                            </div>
                                        </div>
                                        <label class="count">1 Webinar</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <a href="#" class="plan-link"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="bs-plan active">
                                        <div class="img-wrap">
                                            <span class="icon icon-money"></span>
                                        </div>
                                        <h2 class="title">Quadra Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">1,400</span>
                                                <span class="qty">/ 6 month</span>
                                            </div>
                                        </div>
                                        <label class="count">4 Webinars</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <a href="#" class="plan-link"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="bs-plan">
                                        <div class="img-wrap">
                                            <span class="icon icon-finance"></span>
                                        </div>
                                        <h2 class="title">Octo Plan</h2>
                                        <div class="bs-currency">
                                            <div class="og-price">
                                                <span class="icon icon-inr">Rs.</span>
                                                <span class="value">2,400</span>
                                                <span class="qty">/ 1 year</span>
                                            </div>
                                        </div>
                                        <label class="count">8 Webinars</label>
                                        <button type="button" class="btn btn-default btn-with-icon btn-action">
                                            Avail <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span>
                                        </button>
                                        <a href="#" class="plan-link"></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-desc">
                    <div class="bs-ad-banner">
                        <div class="container">
                            <div class="img-wrap js-addto">
                                <img src="assets/images/ad-banner.jpg" class="js-img">
                            </div>
                            <div class="element-wrap">
                                <span class="net"></span>
                            </div>
                            <div class="info-wrap">
                                <h3 class="title">Your Personal and Professional Goals with Finshiksha </h3>
                                <div class="desc">
                                    <p>Join now to receive personalized recommendations from finshiksha</p>
                                </div>
                                <button type="button" class="btn btn-default btn-with-icon btn-reverse">Join Now <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Our Corporate Partners</h2>
                        <div class="sec-desc">
                            <p>We offer the worlds largest network of finance industry</p>
                        </div>
                    </div>
                    <div class="sec-desc">
                        <div class="lyt-partners row">
                            <div class="lhs col-md-6 col-sm-6">
                                <div class="swiper-container" id="partnerSwiper2">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="mod-logo-list">
                                                <ul>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/2.jpg" alt="amazon">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/3.jpg" alt="loreal">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/4.jpg" alt="google">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/5.jpg" alt="deloitte">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/6.jpg" alt="db">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/7.jpg" alt="et">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/8.jpg" alt="scb">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/9.jpg" alt="bajaj">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/10.jpg" alt="honda">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="mod-logo-list">
                                                <ul>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/2.jpg" alt="amazon">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/3.jpg" alt="loreal">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/4.jpg" alt="google">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/5.jpg" alt="deloitte">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/6.jpg" alt="db">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/7.jpg" alt="et">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/8.jpg" alt="scb">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/9.jpg" alt="bajaj">
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="img-wrap">
                                                            <img src="assets/images/logos/10.jpg" alt="honda">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slideNav">
                                        <div class="swiper-button-next"></div>
                                        <!-- <div class="swiper-pagination"></div> -->
                                        <div class="swiper-button-prev"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rhs col-md-6 col-sm-6">
                                <h4 class="title">Our Alumni</h4>
                                <ul class="bs-alumni-list">
                                    <li class="item">
                                        <div class="bs-alumni">
                                            <div class="img-wrap js-addto" style="background-image: url(&quot;assets/images/user.png&quot;);">
                                                <img src="assets/images/img2.jpg" alt="Alumni 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="name">Rahul Agrawal</h3>
                                                <p class="designation">Financial Advisor</p>
                                            </div>
                                            <div class="logo-wrap">
                                                <img src="assets/images/logos/google.png" alt="logo">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item">
                                        <div class="bs-alumni">
                                            <div class="img-wrap js-addto" style="background-image: url(&quot;assets/images/user.png&quot;);">
                                                <img src="assets/images/img6.jpg" alt="Alumni 1" class="js-img">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="name">Rahul Agrawal</h3>
                                                <p class="designation">Financial Advisor</p>
                                            </div>
                                            <div class="logo-wrap">
                                                <img src="assets/images/logos/deloitte.png" alt="logo">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-desc">
                    <div class="lyt-achieve">
                        <div class="container">
                            <div class="top-band">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="bs-section typ-left">
                                            <div class="sec-head">
                                                <h2 class="sec-title">
                                                    Achieve World Class Learning
                                                </h2>
                                                <div class="sec-desc">
                                                    <p>Join now to receive personalized recommendations from finshiksha Join now to receive personalized recomme ndations from finshikshaJoin now </p>
                                                </div>
                                                <button type="button" class="btn btn-default btn-with-icon hidden-xs">Know More<span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <div class="bs-learnings">
                                            <ul>
                                                <li class="item">
                                                    <span class="icon icon-university"></span>
                                                    <span class="text">Learn from leading universities and companies</span>
                                                </li>
                                                <li class="item">
                                                    <span class="icon icon-pig"></span>
                                                    <span class="text">Accesible, Find flexible, affordable options</span>
                                                </li>
                                                <li class="item">
                                                    <span class="icon icon-skills"></span>
                                                    <span class="text">Master skills with in-depth learning</span>
                                                </li>
                                                <li class="item">
                                                    <span class="icon icon-credentials"></span>
                                                    <span class="text">Earn industry recognized credentials</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <button type="button" class="btn btn-default btn-with-icon visible-xs">Know More<span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-band">
                                <div class="bs-advantage">
                                    <span class="net"></span>
                                    <div class="mock-wrap hidden-xs">
                                        <div class="img-wrap js-addto">
                                            <img src="assets/images/play.png" alt="image 1" class="js-img js-target">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6">
                                            <div class="info-wrap">
                                                <h2 class="title">Top In Class Advantage</h2>
                                                <h3 class="sub-title">Accessible, Find Flexible, Affordable Options</h3>
                                                <div class="mock-wrap visible-xs">
                                                    <div class="img-wrap js-addto">
                                                        <img src="assets/images/play.png" alt="image 1" class="js-img js-target">
                                                    </div>
                                                </div>
                                                <ul class="optiotn-list">
                                                    <li class="item">
                                                        <div class="option js-trigger active" data-img="assets/images/play.png">
                                                            <span class="icon icon-trianlge"></span>
                                                            <p>Start streaming on-demand video lectures today from top instructors in subjects like business.</p>
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="option js-trigger" data-img="assets/images/img3.jpg">
                                                            <span class="icon icon-circle"></span>
                                                            <p>Lorem ipsum is simply a dummy text of printing and typesetting industry.</p>
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="option js-trigger" data-img="assets/images/img4.jpg">
                                                            <span class="icon icon-plus"></span>
                                                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                                        </div>
                                                    </li>
                                                    <li class="item">
                                                        <div class="option js-trigger" data-img="assets/images/img5.jpg">
                                                            <span class="icon icon-square"></span>
                                                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-left">Students Testimonials</h2>
                    </div>
                    <div class="sec-cont">
                        <!-- testimonial start -->
                        <div class="bs-testimonials">
    <div class="swiper-container testimonial-img">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img2.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img3.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img4.jpg" alt="testimonial images">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="img-wrap js-addto">
                    <img class="js-img" src="assets/images/img5.jpg" alt="testimonial images">
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-container testimonial-decs">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="cont">
                    <div class="decs">
                        <p>I started at stage zero. With Coursera I was able to start learning online and
                            eventually build up enough knowledge and skills to  Start streaming
                            on-demand video lectures today from top</p>
                        <div class="meta-info">
                            <span class="name">Rahul Agrawal</span>
                            <span class="designation">Financial Advisor</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide-count">
        <span class="active">01</span> / <span class="total">03</span>
    </div>
    <div class="slideNav">
        <div class="swiper-button-next"></div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
                        <!-- testimonial end -->
                    </div>
                </div>
            </div>
            <div class="bs-section">
                <div class="sec-desc">
                    <div class="bs-quiz">
                        <span class="net"></span>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="title-wrap">
                                        <h2 class="title">Take a Quiz and Know About Your Knowledge</h2>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="info-wrap">
                                        <div class="qa-qrap">
                                            <div class="pagination">
                                                <span class="current">01</span> <span>/</span> <span>03</span>
                                            </div>
                                            <form>
                                                <ul class="quiz-list">
                                                    <li class="item item1">
                                                        <h3 class="question">What is a hybrid investment?</h3>
                                                        <ul class="answer-list">
                                                            <li>
                                                                <div class="bs-radio">
                                                                    <input type="radio" name="question1" id="q1">
                                                                    <label for="q1">A product with debt and equity features</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="bs-radio">
                                                                    <input type="radio" name="question1" id="q2">
                                                                    <label for="q2">A safe, low-yield investment model</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="bs-radio">
                                                                    <input type="radio" name="question1" id="q3">
                                                                    <label for="q3">A type of bond</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <div class="action-wrap">
                                                    <button type="button" class="btn btn-default btn-with-icon btn-reverse">Submit <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- blog start -->
            <div class="bs-section">
    <div class="container">
        <div class="sec-head">
            <h2 class="sec-title element-left">Read Our Blogs</h2>
        </div>
        <div class="sec-desc swiper-container js-blog-swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img7.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog typ2 js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/img8.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="bs-blog js-addto">
                        <div class="img-wrap">
                            <img src="assets/images/team.jpg" class="js-img">
                        </div>
                        <span class="cm-tag">Latest</span>
                        <div class="cont-wrap">
                            <ul class="mod-tags">
                                <li><a href="#" class="tag-link">mba</a></li>
                                <li><a href="#" class="tag-link">working professional</a></li>
                                <li><a href="#" class="tag-link">combo course</a></li>
                            </ul>
                            <div class="desc">
                                <p>Know how everyone can be rich through online courses</p>
                            </div>
                            <div class="action-wrap">
                                <button type="button" class="btn btn-default btn-with-icon">Read <span class="icon-wrap"><i class="icon  icon-rightArrow"></i></span></button>
                            </div>
                        </div>
                        <a href="#" class="link"></a>
                    </div>
                </div>
            </div>
            <div class="act-wrap view-more-btn">
                <button type="button" class="btn btn-link with-line">View all blogs</button>
            </div>
        </div>
    </div>
</div>
            <!-- blog end -->
        </div>
    </main>

    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>
    

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <?php $this->load->view('include/common_js.php'); ?>
    <!-- js group end -->
</body>

</html>