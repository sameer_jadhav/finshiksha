
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('include/head.php'); ?>

<body>
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-center typ-faq typ-sm">
    <div class="banner-cont">
        <div class="banner-info">
            <h2 class="title">Frequently Asked Questions</h2>
            <div class="desc">
                <p>How we may help you</p>
            </div>
            <!-- <div class="bs-search">
                <input type="text" class="form-control" placeholder="Have any Questions">
                <button type="button" class="btn-addon"><span class="icon icon-search"></span></button>
            </div> -->
        </div>
        <div class="banner-element">
            <span class="net"></span>
        </div>
    </div>
</div>
            <!-- banner end -->
            <div class="bs-section">
                <div class="container">
                    <div class="sec-head">
                        <h2 class="sec-title element-right">Quick find what you need</h2>
                    </div>
                    <div class="sec-cont">
                        <div class="bs-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#general" aria-controls="general" role="tab" data-toggle="tab" class="mod-tile">
                                        <span class="icon icon-general"></span>
                                        <span class="text">General</span>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#pricing" aria-controls="pricing" role="tab" data-toggle="tab" class="mod-tile">
                                        <span class="icon icon-pricing"></span>
                                        <span class="text">Pricing Plans</span>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#webinar" aria-controls="webinar" role="tab" data-toggle="tab" class="mod-tile">
                                        <span class="icon icon-webinar"></span>
                                        <span class="text">Course &amp; webinar</span>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#guide" aria-controls="guide" role="tab" data-toggle="tab" class="mod-tile">
                                        <span class="icon icon-guide"></span>
                                        <span class="text">Usage guide</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-select bs-custom-select typ-box">
                                <select class="js-select" id="tabSelect">
                                    <option value="all" data-href="#home">General</option>
                                    <option value="moisturiser" data-href="#menu1">Pricing Plans</option>
                                    <option value="facescrub" data-href="#menu2">Course &amp; webinar</option>
                                    <option value="superfoods" data-href="#menu3">Usage guide</option>
                                </select>
                            </div>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="general">
                                    <div class="bs-search-list">
                                        <h3 class="title">Most searched topics</h3>
                                        <ul class="list-wrap">
                                            <li class="item">
                                                <div class="bs-radio typ-btn">
                                                    <input type="radio" name="search" id="op1">
                                                    <label for="op1">How to Issues</label>
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="bs-radio typ-btn">
                                                    <input type="radio" name="search" id="op2">
                                                    <label for="op2">How to solve payment Issues</label>
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="bs-radio typ-btn">
                                                    <input type="radio" name="search" id="op3">
                                                    <label for="op3">How to solve payment Issues ayment </label>
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="bs-radio typ-btn">
                                                    <input type="radio" name="search" id="op4">
                                                    <label for="op4">How to solve payment Issues</label>
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="bs-radio typ-btn">
                                                    <input type="radio" name="search" id="op5">
                                                    <label for="op5">How to solve payment Issues</label>
                                                </div>
                                            </li>
                                            <li class="item">
                                                <div class="bs-radio typ-btn">
                                                    <input type="radio" name="search" id="op1">
                                                    <label for="op1">How to solve payment Issues</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="bs-accordian">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Will I receive the recording after the live webinar is over? <span class="icon icon-plus"></span></a>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        We host more than 150,000 courses on our online learning marketplace. Our marketplace model means we do not own the copyright to the content of the courses; the respective instructors own these rights. On rare occasions, instructors may remove their courses
                                                        from our marketplace, or, Udemy may have to remove a course from the platform for policy or legal reasons. If this does happen to a course you're enrolled in, please contact us and we'll be ready
                                                        to help
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Where can I find the recording of the live webinar?<span class="icon icon-plus"></span></a>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                        Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Will I receive the recording after the live webinar is over?<span class="icon icon-plus"></span></a>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                        Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Will I receive the recording after the live webinar is over?<span class="icon icon-plus"></span></a>
                                                </div>
                                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                        Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFive">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Will I receive the recording after the live webinar is over?<span class="icon icon-plus"></span></a>
                                                </div>
                                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                        Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Will I receive the recording after the live webinar is over?<span class="icon icon-plus"></span></a>
                                                </div>
                                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                                        on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                                        Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="pricing">..2.</div>
                                <div role="tabpanel" class="tab-pane" id="webinar">..3.</div>
                                <div role="tabpanel" class="tab-pane" id="guide">.4..</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    
        <!-- footer start -->
    <?php $this->load->view('include/footer.php'); ?>  
        <!-- footer end -->
    

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>

    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
        <!-- js group end -->
</body>

</html>