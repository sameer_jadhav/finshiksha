
<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('include/head.php'); ?>

<body>
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="main">
            <!-- banner start -->
            <div class="bs-banner typ-center typ-sm">
    <div class="banner-cont">
        <div class="banner-info">
            <h2 class="title">Terms & Condition</h2>
        </div>
        <div class="banner-element">
            <span class="net"></span>
        </div>
    </div>
</div>
            <!-- banner end -->
            <div class="bs-section">
                <div class="sec-cont">
                    <div class="container-sm">
                        <div class="bs-message">
                            <ul class="list">
                                <li class="m-item">
                                    <h3 class="title">Terms and Conditions related to Public Forums and Groups run by FinShiksha</h3>
                                    <h4 class="sub-title">The group will be monitored by FinShiksha. FinShiksha will have sole discretion to allow or disallow membership to these groups.</h4>
                                    <div class="bs-list typ-clr-number">
                                        <h5 class="list-title">Table of Contents</h5>
                                        <ul>
                                            <li>Accounts</li>
                                            <li>Course Enrollment and Lifetime Access</li>
                                            <li>Payments, Credits, and Refunds</li>
                                            <li>Content and Behavior Rules</li>
                                            <li>Udemy’s Rights to Content You Post</li>
                                            <li>Using Udemy at Your Own Risk</li>
                                            <li>Udemy’s Rights</li>
                                            <li> Miscellaneous Legal Terms</li>
                                            <li>Dispute Resolution</li>
                                            <li>Updating These Terms</li>
                                            <li>How to Contact Us</li>
                                        </ul>
                                    </div>
                                    <p>By registering in the group, you agree to these terms and conditions. You agree that your contact details may be available in the group details.</p>
                                    <p>FinShiksha will have the sole discretion to resolve any potential conflict between members.</p>
                                    <p>The group is solely meant for discussions related to financial services and economics. Any content that is unrelated is not to be posted on these groups. No unrelated forwards would be accepted in the groups</p>
                                    <p>Participants can’t post forwards which are improper in discourse and are against the law of the land. No posts which may be offensive would be allowed, and the person posting any offensive content will face action.
                                        Content promoting any kind of violence or nudity will be considered offensive. FinShiksha will – on a best effort basis – remove any posts that are of offensive nature. However, FinShiksha or its employees will
                                        not bear any responsibility of any personal views or comments of the participants in these groups.</p>
                                    <p>Participant can’t commercially exploit the content or any part, of these groups, without our prior permission. Any offensive or abusive behaviour will not be tolerated. Any participant causing inconvenience to other
                                        participants will be barred from the groups. The decision of FinShiksha would be final in this case.</p>
                                    <p>All views are welcome. Participant can discuss ideas related to economics, finance, presentations/course content around Finance.</p>
                                    <p>Students from multiple universities could be a part of it. Participant can also invite friends/colleagues from other universities if there’s space in the groups.</p>
                                </li>
                                <li>
                                    <h3 class="title">Terms and Conditions around Courses</h3>
                                    <p>The programs enrolled for are non-transferable.</p>
                                    <p>FinShiksha Reserves the rights to the course content. Any unauthorized reproduction of the same is illegal. In any program, the candidate agrees to abide by the standard set of rules governing content privacy.</p>
                                    <p>Some programs may be non-refundable, so please check for the terms of each program before enrolling. FinShiksha reserves the right to refuse a candidate a seat in the program.</p>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

        <!-- footer start -->
    <?php $this->load->view('include/footer.php'); ?>        
        <!-- footer end -->

    <!-- popup start -->
    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- popup end -->

    <!-- js group start -->
    <?php $this->load->view('include/footer_js.php'); ?>
    <!-- js group end -->
</body>

</html>