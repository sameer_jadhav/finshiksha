
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	 <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
        RasoiTatva
</title>
<style type="text/css">
	@media screen and (max-width: 650px){
		.table{
			width: 100%;
			margin: 0 auto;
		}
	}
</style>
</head>

<body bgcolor="#fff" style="background-color:#ffffff; font-family:verdana;">
<table class="table" width="600" border="0" align="center" cellpadding="0" cellspacing="0" widtd="650" style="background-color:#FFF; margin-top:20px; border:1px solid #ececec;">
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="20"></td></tr>
  <tr>
		<td align="center" valign="middle">
	  	<a href="#"><img src="<?php echo base_url();?>assets/images/emailer/logo.svg" width="65" height="65"></a>
	  	</td>
  </tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="50"></td></tr>
	<tr>
        <td align="center" valign="top"><font face="verdana" color="#471c07" style="line-height:30px; font-size:24px;text-transform: uppercase;font-weight: bold;letter-spacing: 4px">Reset your password</font></td>
      </tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="20"></td></tr>
	<tr><td align="center"><img src="<?php echo base_url();?>assets/images/emailer/title-pattern.png" width="60" height="6"></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="28"></td></tr>
		<tr><td align="center" valign="top"><font face="verdana" color="#666666" style="line-height:18px; font-size:16px;text-transform: capitalize;font-weight: bold;">Hi There!</font></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="12"></td></tr>
	<tr><td align="center" valign="top" style="padding: 0 10px;"><font face="verdana" color="#666666" style="line-height:24px; font-size:16px;font-weight: regular;">It seems you have lost your access to The Skin Pantry. We have received a request to reset your password. You can create a new one by simply clicking the button below.</font></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="35"></td></tr>
	<tr><td align="center"><a href="<?php echo $link;?>"><img src="<?php echo base_url();?>assets/images/emailer/reset-button.png" width="200" height="40"></a></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="35"></td></tr>
	<tr><td align="center" valign="top" style="padding: 0 10px;"><font face="verdana" color="#666666" style="line-height:24px; font-size:16px;font-weight: regular;">If you haven’t requested a password change, kindly ignore this mail.</font></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="55"></td></tr>
	<tr width="100%">
		<td>
			<table width="100%" bgcolor="#f6eee7" cellspacing="0" cellpadding="0">
				<tbody>
					<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="40"></td></tr>
	
					<tr><td align="center"><font style="font-size: 14px;color:#666666;font-weight: regular;text-transform: capitalize;line-height: 18px">phone no.</font><br/><a href="tel:02222650086" style="color:#372819;font-size: 14px;line-height: 18px;font-weight: regular;text-decoration: none">022-22650086</a></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="22"></td></tr>

	<tr><td align="center"><font style="font-size: 14px;color:#666666;font-weight: regular;text-transform: capitalize;line-height: 18px">contact</font><br/><a href="help@rasoitatva.com" style="color:#372819;font-size: 14px;line-height: 18px;font-weight: regular;text-decoration: none">help@rasoitatva.com</a></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="20"></td></tr>
	<tr><td align="center" valign="top"><a href="www.rasoitatva.com" style="color:#372819;font-size: 14px;line-height: 18px;font-weight: regular;text-decoration: none">www.rasoitatva.com</a></td></tr>
	<tr><td><img src="<?php echo base_url();?>assets/images/emailer/point.gif" width="20" height="45"></td></tr>
				</tbody>
			</table>
		</td>
	</tr>
	
	
</table>
</body>

</html>