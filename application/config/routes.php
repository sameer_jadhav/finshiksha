<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['superadmin'] = "superadmin/admin/login";
$route['default_controller'] = "home/index";
$route['the-pantry-wall'] = "pantrywall";
//$route['the-skin-pantry'] = "aboutus";
$route['shop/(:any)/(:any)'] = "product/index";
$route['shop/(:any)'] = "category/index";
$route['product/(:num)'] = "product/summary";
//$route['product/(:any)'] = "product/index";
$route['about-us'] = "aboutus";
//$route['contactus'] = "contact_us";
$route['people-first'] = "people_first";
$route['certifications'] = "certification";
$route['csr-and-values'] = "csr";
$route['india'] = "india_project";
$route['oman'] = "oman_project";
$route['core-team'] = "team";
$route['project-archives'] = "project_archive";
$route['project-detail/(:any)'] = "project_detail/index/$1";
//$route['aboutus'] = "about_us";
//$route['/(:project)'] = "project/index/$1";
$route['expertise/(:any)'] = "project/index/$1";


$route['404_override'] = 'custom404ctrl';
$route['translate_uri_dashes'] = FALSE;
