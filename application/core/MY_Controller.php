<?php

/**
 * The base controller which is used by the Front and the Admin controllers
 */
class Base_Controller extends CI_Controller
{
	public $_layout = "two-column-left";	
	public $_title = "Rasoitatva";
	public $_sidebar = array();
	public $_before_header = array();
	public $_class = "";
	public $_extraCss = array();
	public $_extraJs = array();
	public $_header = "header";	
	public $_default_photo = '';
	public $_userdata = array( );
	public $_finalscore = 0;
	public $_club = 'NA';
	public $_admindata = array( );
	public function __construct()
	{
		
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
	}
	
}//end Base_Controller





//admin 
class Admin_Controller extends Base_Controller
{
	public $_layout = "two-column-left";
	function __construct(){
		
		parent::__construct();
		$this->load->helper('front');

		//echo " Front_Controller";
		$this->_default_photo =  base_url('uploads/default.png');
		$this->_admindata = $this->session->userdata('admin');

	}
	
	function view($view, $vars = array(), $string=false)
	{
		//echo $view; exit;
		if($string)
		{
			return $result;
		}
		else
		{
			
			$vars['class'] = $this->uri->segment(1);
			if($vars['class']=="") {
				$vars['class'] = "admin-home";
			}
			// exit();
			$vars['menu'] = $this->load->view('admin/page/menu', null, true);
			$vars['title'] = $this->_title;
			$vars['before_header'] = "\n";
			foreach($this->_before_header as $value) {
				$vars['before_header'].= $value."\n";
			}
			//$vars['extraCss'] = $this->extraCss;
			// exit;
			$result['header'] = $this->load->view('admin/page/header', $vars, true);
			unset($vars['before_header']);
			unset($vars['menu']);
			unset($vars['title']);
			if($this->_layout=="two-column-left") {
				// $data['sidebar'] = $this->_sidebar;

				
				$result['sidebar'] = $this->load->view('admin/page/sidebar', array(), true);
				// print_r($result['sidebar']);
				/*exit;
				unset($data);*/
			}


			$result['sidebar'] = $this->load->view('admin/page/sidebar', $this->_sidebar, true);
			$result['content'] = $this->load->view('admin/'.$view, $vars, true);
			$result['footer'] = $this->load->view('admin/page/footer', $vars, true);
			$this->load->view('admin/page/'.$this->_layout, $result);

		}
	}
	
	/*
	This function simply calls $this->load->view()
	*/
	function partial($view, $vars = array(), $string=false)
	{
		if($string)
		{
			return $this->load->view('admin/'.$view, $vars, true);
		}
		else
		{
			$this->load->view('admin/'.$view, $vars);
		}
	}
}
class Private_Admin extends Admin_Controller {
	function __construct(){
		parent::__construct();
		$userData = $this->session->userdata('admin');		
		 
		if($userData['id']==null) {
			$this->session->set_flashdata('msg', showError("Please login to continue."));
			redirect(admin_url('login'));
			exit;
		}
		//echo " Admin_Controller";
	}
}
