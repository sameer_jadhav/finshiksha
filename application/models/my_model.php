<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {

    var $client_service = "frontend-client";
    var $auth_key       = "simplerestapi";

    public function check_auth_client(){
        $client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key  = $this->input->get_request_header('Auth-Key', TRUE);
        
        if($client_service == $this->client_service && $auth_key == $this->auth_key){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized.'));
        }
    }

    public function login($username,$password)
    {
        $q  = $this->db->select('password,id')->from('users')->where('username',$username)->get()->row();
       
        if($q == ""){
            return array('status' => 204,'message' => 'Username not found.');
        } else {
            $hashed_password = $q->password;
            $id              = $q->id;
             echo $hashed_password ." ".$password;
        //exit;
            if (hash_equals($hashed_password, crypt($password, $hashed_password))) {
               $last_login = date('Y-m-d H:i:s');
               $token = crypt(substr( md5(rand()), 0, 7));
               $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
               $this->db->trans_start();
               $this->db->where('id',$id)->update('users',array('last_login' => $last_login));
               $this->db->insert('users_authentication',array('users_id' => $id,'token' => $token,'expired_at' => $expired_at));
               if ($this->db->trans_status() === FALSE){
                  $this->db->trans_rollback();
                  return array('status' => 500,'message' => 'Internal server error.');
               } else {
                  $this->db->trans_commit();
                  return array('status' => 200,'message' => 'Successfully login.','id' => $id, 'token' => $token);
               }
            } else {
                echo "Wrong password";
                exit();
               return array('status' => 204,'message' => 'Wrong password.');
            }
        }
    }

    public function logout()
    {
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('Authorization', TRUE);
        $this->db->where('users_id',$users_id)->where('token',$token)->delete('users_authentication');
        return array('status' => 200,'message' => 'Successfully logout.');
    }

    public function auth()
    {
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('Authorization', TRUE);
        $q  = $this->db->select('expired_at')->from('users_authentication')->where('users_id',$users_id)->where('token',$token)->get()->row();
        if($q == ""){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized.'));
        } else {
            if($q->expired_at < date('Y-m-d H:i:s')){
                return json_output(401,array('status' => 401,'message' => 'Your session has been expired.'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id',$users_id)->where('token',$token)->update('users_authentication',array('expired_at' => $expired_at,'updated_at' => $updated_at));
                return array('status' => 200,'message' => 'Authorized.');
            }
        }
    }

    public function book_all_data()
    {
        return $this->db->select('id,title,author')->from('books')->order_by('id','desc')->get()->result();
    }

     public function spaces_all_data()
    {
        //echo 4545454;exit;
        return $this->db->select('id,project_title as project_name,project_quote as project_title,project_short_desc,project_banner_img,project_mobile_img,img_quote,quote_by,icon_class')->from('table_spaces')->order_by('id')->get()->result();
    }

    public function spaces_limited_data()
    {
        //echo 4545454;exit;
        return $this->db->select('project_title,icon_class')->from('table_spaces')->order_by('id')->get()->result();
    }

    public function testimonial_all_data()
    {
        return $this->db->select('testimonial,testimonial_by')->from('table_testimonial')->order_by('id')->get()->result();
    }

     public function banner_all_data()
    {
        //echo 4545454;exit;
        return $this->db->select('id,title,description,v_tour_link,banner_large_img,banner_mobile_img')->from('table_banner')->order_by('id')->get()->result();
    }

    public function get_ourspaces_content()
    {
        return $this->db->select('content')->from('table_content')->where('type','our_spaces')->get()->result();
    }


    public function media_center_listing($page,$start_from,$limit)
    {
        //echo 4545454;exit;
        return $this->db->select('id,title,description,short_desc,newspaper,publisher,publish_date,banner_img as thumb_img,banner_desktop_img,banner_mobile_img,category,pdf_file_name,pdf_file_name as download_file')->from('table_media')->order_by('id')->limit($limit,$start_from)->get()->result();
    }

    public function media_detail($id)
    {
        return $this->db->select('id,title,description,short_desc,publisher,newspaper,publish_date,banner_img as thumb_img,banner_desktop_img,banner_mobile_img,category,pdf_file_name,pdf_file_name as download_file')->from('table_media')->where('id',$id)->order_by('id')->get()->result();
    }

    public function getPopup(){
        return $this->db->select('id,img_name,url')->from('table_popup')->where('is_enabled',1)->order_by('id')->get()->result();
    }

    public function media_detail_by_cat($cat_name,$page,$start_from,$limit)
    {
        $col_str = "banner_desktop_img ,banner_mobile_img ";
        if($cat_name=="photo-gallery")
        {
            $col_str = "banner_desktop_img as jpg_img,banner_mobile_img as tif_img";
        }
        return $this->db->select('id,title,description,short_desc,publisher,newspaper,publish_date,banner_img as thumb_img,'.$col_str.',category,pdf_file_name,pdf_file_name as download_file')->from('table_media')->where('category',$cat_name)->order_by('id')->limit($limit,$start_from)->get()->result();
    }

    public function media_next($cat_name,$media_id)
    {

        return $this->db->select('id,title,newspaper,publish_date,banner_img as thumb_img')->from('table_media')->where('category',$cat_name)->where("id >",$media_id)->limit(1,0)->get()->result();
    }

    public function media_prev($cat_name,$media_id)
    {

        return $this->db->select('id,title,newspaper,publish_date,banner_img as thumb_img')->from('table_media')->where('category',$cat_name)->where("id <",$media_id)->order_by("id","DESC")->limit(1,0)->get()->result();
    }

    public function media_last($cat_name,$media_id){
         return $this->db->select('id,title,newspaper,publish_date,banner_img as thumb_img')->from('table_media')->where('category',$cat_name)->order_by("id","DESC")->limit(1,0)->get()->result();
    }

     public function media_first($cat_name,$media_id){
         return $this->db->select('id,title,newspaper,publish_date,banner_img as thumb_img')->from('table_media')->where('category',$cat_name)->order_by("id","ASC")->limit(1,0)->get()->result();
    }



    public function blog_listing($page,$start_from,$limit,$category="")
    {
        //echo 4545454;exit;
        if($category!="")
        {   
            $cond = "->where('category',$cat_name)";
        }
        return $this->db->select('id,title,publish_date,author,category,banner_desktop_img,banner_mobile_img,thumbnail_img,sub_category,description,short_desc,sort_order')->from('table_blog')->order_by('id')->limit($limit,$start_from)->get()->result();
    }

    public function blog_listing_by_cat($page,$start_from,$limit,$cat_name)
    {
        
        return $this->db->select('id,title,publish_date,author,category,banner_desktop_img,banner_mobile_img,thumbnail_img,sub_category,description,short_desc,sort_order')->where('category',$cat_name)->from('table_blog')->order_by('id')->limit($limit,$start_from)->get()->result();
    }

    public function blog_detail($id)
    {
        return $this->db->select('id,title,publish_date,author,category,banner_desktop_img,banner_mobile_img,thumbnail_img,sub_category,description,short_desc,sort_order')->where('id',$id)->from('table_blog')->order_by('id')->get()->result();
    }


    public function blog_next($cat_name,$media_id)
    {

        return $this->db->select('id,title,publish_date,author,category,thumbnail_img,sub_category,short_desc')->from('table_blog')->where('category',$cat_name)->where("id >",$media_id)->limit(1,0)->get()->result();
    }

    public function blog_prev($cat_name,$media_id)
    {

        return $this->db->select('id,title,publish_date,author,category,thumbnail_img,sub_category,short_desc')->from('table_blog')->where('category',$cat_name)->where("id <",$media_id)->order_by("id","DESC")->limit(1,0)->get()->result();
    }

    public function blog_last($cat_name,$media_id){
         return $this->db->select('id,title,publish_date,author,category,thumbnail_img,sub_category,short_desc')->from('table_blog')->where('category',$cat_name)->order_by("id","DESC")->limit(1,0)->get()->result();
    }

     public function blog_first($cat_name,$media_id){
         return $this->db->select('id,title,publish_date,author,category,thumbnail_img,sub_category,short_desc')->from('table_blog')->where('category',$cat_name)->order_by("id","ASC")->limit(1,0)->get()->result();
    }

    public function event_listing($page,$start_from,$limit,$category="")
    {
        
        return $this->db->select('id,title')->from('table_event')->order_by('id')->limit($limit,$start_from)->get()->result();
    }

    public function event_detail($id)
    {
        return $this->db->select('id,title')->where('id',$id)->from('table_event')->order_by('id')->get()->result();
    }

    public function event_media($id){
        return $this->db->select('id,file_name')->where('ref_id',$id)->from('table_files')->order_by('id')->get()->result();

    }

    public function search_blogs($term)
    {
        return $this->db->select('id,title,publish_date,author,category,banner_desktop_img,banner_mobile_img,thumbnail_img,sub_category,description,short_desc,sort_order')->from('table_blog')->order_by('id')->where("title like ",'%'.$term.'%')->get()->result();
        
    }
    
    public function search_events($term){
        return $this->db->select('id,title,publish_date,short_desc,description,category,thumbnail_img,banner_desktop_img,banner_mobile_img')->from('table_event')->order_by('id')->where("title like ",$term.'%')->get()->result();
        //echo $this->db->last_query();
    }

    public  function location_all_data()
    {
       return $this->db->select('id,title,type,distance,img_name')->from('table_location')->order_by('id')->get()->result();
    }



    public  function mem_banner_sec()
    {
       return $this->db->select('id,title,description,img_name')->from('table_mem_banner')->order_by('id')->get()->result();
    }
    public  function partne_logo()
    {
       return $this->db->select('id,title as logo_url,alt_text,img_name')->from('table_partner_logo')->order_by('id')->get()->result();
    }
    public  function mem_img_sec()
    {
       return $this->db->select('id,title,description,img_name_1,img_name_2,img_name_3,img_name_4,img_name_5')->from('table_mem_image')->order_by('id')->get()->result();
    }
    public  function individual_member()
    {
       return $this->db->select('id,title_big,title_small,description')->from('table_membership
')->where('type','individual')->order_by('plan_order')->get()->result();
    }
    public  function corporate_member()
    {
       return $this->db->select('id,title_big,title_small,description')->from('table_membership
')->where('type','corporate')->order_by('plan_order')->get()->result();
    }
    public  function atelier_member()
    {
       return $this->db->select('id,title_big,title_small,description')->from('table_membership
')->where('type','atelier')->order_by('plan_order')->get()->result();
    }


    public  function reciprocal_club()
    {
       return $this->db->select('id,title,url,img_name')->from('table_club
')->order_by('id')->get()->result();
    }
    

    public function book_detail_data($id)
    {
        return $this->db->select('id,title,author')->from('books')->where('id',$id)->order_by('id','desc')->get()->row();
    }

    public function book_create_data($data)
    {
        $this->db->insert('books',$data);
        return array('status' => 201,'message' => 'Data has been created.');
    }

    public function chk_subscribe_email($data)
    {
        return $this->db->select('id')->from('table_newsletter')->where('email',$data)->get()->row();
       // return array('status' => 201,'message' => 'Data has been created.');
    }

    public function chk_ad_total_view_analytics($data)
    {
        return $this->db->select('id,total_view,total_hits')->from('ad_total_analytics')->where('ad_id',$data)->get()->row();
       // return array('status' => 201,'message' => 'Data has been created.');
    }

    public function chk_ad_total_hits_analytics($data)
    {
        return $this->db->select('id,total_view,total_hits')->from('ad_total_analytics')->where('ad_id',$data)->get()->row();
       // return array('status' => 201,'message' => 'Data has been created.');
    }


    public function chk_ad_unique_hits($data){
        return $this->db->select('id')->from('table_ad_unique_hits')->where('ad_id',$data['ad_id'])->where('ip_address',$data['ip_address'])->get()->row();
    }

    public function chk_ad_unique_views($data){
        return $this->db->select('id')->from('table_ad_unique_views
')->where('ad_id',$data['ad_id'])->where('ip_address',$data['ip_address'])->get()->row();
    }
    
    public function subscribe_email($data)
    {
        $this->db->insert('table_newsletter',$data);
        return array('status' => 201,'message' => 'You have subscibed Successfully.');
    }

    public function adTotalViewCountInsert($data)
    {
        $this->db->insert('ad_total_analytics',$data);
        return array('status' => 201,'message' => 'You have subscibed Successfully.');
    }

    public function adUniqueHitsinsert($data){
         $this->db->insert('table_ad_unique_hits',$data);
        return array('status' => 201,'message' => 'Data has been inserted');
    }

    public function adUniqueViewsinsert($data){
        $this->db->insert('table_ad_unique_views',$data);
        return array('status' => 201,'message' => 'Data has been inserted');
    }

    public function adTotalViewCountUpdate($data,$id)
    {

         $this->db->where('id',$id)->update('ad_total_analytics',$data);
        return array('status' => 200,'message' => 'Data has been updated.');
        
    }

   
    public function get_active_ad()
    {
        //echo 4545454;exit;
        return $this->db->select('*')->from('table_ads')->where('is_active','1')->where('expiry_date >=','CURDATE()', FALSE)->order_by('id')->get()->result();
    }
    

    public function adTotalHitsCountInsert($data)
    {
        $this->db->insert('ad_total_analytics',$data);
        return array('status' => 201,'message' => 'You have subscibed Successfully.');
    }
    
     public function adTotalHitsCountUpdate($data,$id)
    {

         $this->db->where('id',$id)->update('ad_total_analytics',$data);
        return array('status' => 200,'message' => 'Data has been updated.');
        
    }
    

     public function enquiry($data)
    {
        $this->db->insert('table_service',$data);
        return array('status' => 201,'message' => 'Enquiry sent Successfully');
    }

      public function subsciption($data)
    {
        $this->db->insert('table_subscription',$data);
        return array('status' => 201,'message' => 'Enquiry sent Successfully');
    }

    public function book_update_data($id,$data)
    {
        $this->db->where('id',$id)->update('books',$data);
        return array('status' => 200,'message' => 'Data has been updated.');
    }

    public function book_delete_data($id)
    {
        $this->db->where('id',$id)->delete('books');
        return array('status' => 200,'message' => 'Data has been deleted.');
    }



}
