<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_by_id($table, $where) {
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data) {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function updatephone($where1, $data1) {
        $this->db->update($this->table1, $data1, $where1);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}
