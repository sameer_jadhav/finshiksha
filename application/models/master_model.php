<?php if( !defined('BASEPATH')) exit('No direct script access alloed');

class Master_model extends CI_Model
{
	/*
		# function getRecordCount($tbl_name,$condition=FALSE)
		# * indicates paramenter is must
		# Use : 
			1) return number of rows
		# Parameters : 
			$tbl_name* =name of table 
			$condition=array('column_name1'=>$column_val1,'column_name2'=>$column_val2);
		
		# How to call:
			$this->master_model->getRecordCount('tbl_name',$condition_array);
	*/
	
	public function getRecordCount($tbl_name,$condition=FALSE)
	{
		if($condition!="" && count($condition)>0)
		{
			foreach($condition as $key=>$val)
			{ $this->db->where($key,$val); }
		}
		$num=$this->db->count_all_results($tbl_name);
		return $num;
	}
	
	/*
		# function getRecords($tbl_name,$condition=FALSE,$select=FALSE,$order_by=FALSE,$limit=FALSE,$start=FALSE)
		# * indicates paramenter is must
		# Use : 
			1) return array of records from table
		# Parameters : 
			1) $tbl_name* =name of table 
			2) $condition=array('column_name1'=>$column_val1,'column_name2'=>$column_val2);
			3) $select=('col1,col2,col3');
			4) $order_by=array('colname1'=>order,'colname2'=>order); Order='ASC OR DESC'
			5) $limit= limit for paging
			6) $start=start for paging
		
		# How to call:
			$this->master_model->getRecords('tbl_name',$condition_array,$select,...);
			
		# In case where we need joins, you can pass joins in controller also.
		Ex: 
			$this->db->join('tbl_nameB','tbl_nameA.col=tbl_nameB.col','left');
			$this->master_model->getRecords('tbl_name',$condition_array,$select,...);
			
		# Instruction 
			1) check number of counts in controller or where you are displying records
			
	*/
	
	public function getRow($tbl_name,$condition=FALSE,$select=FALSE){
		if($select!="")
		{$this->db->select($select);}
		
		if(count($condition)>0 && $condition!="")
		{ $condition=$condition; }
		else
		{$condition=array();}


		$rst=$this->db->get_where($tbl_name,$condition);		
		return $rst->row_array();
	}
	public function getRecords($tbl_name,$condition=FALSE,$select=FALSE,$order_by=FALSE,$start=FALSE,$limit=FALSE,$group_by=FALSE)
	{
		if($select!="")
		{$this->db->select($select);}
		
		if(count($condition)>0 && $condition!="")
		{ $condition=$condition; }
		else
		{$condition=array();}
		if(count($order_by)>0 && $order_by!="")
		{
			
			foreach($order_by as $key=>$val)
			{$this->db->order_by($key,$val);}
		}
		if($limit!="" || $start!="")
		{ $this->db->limit($limit,$start);}
		if($group_by!="" )
		{ $this->db->group_by($group_by);}
		
		$rst=$this->db->get_where($tbl_name,$condition);		
		return $rst->result_array();
	}
	
	public function insertRecord($tbl_name,$data_array,$insert_id=FALSE)
	{
		if($this->db->insert($tbl_name,$data_array))
		{
			if($insert_id==true)
			{return $this->db->insert_id();}
			else
			{return true;}
		}
		else
		{return false;}
	}
	
	/*
		# function updateRecord($tbl_name,$data_array,$pri_col,$id)
		# * indicates paramenter is must
		# Use : 
			1) updates record, on successful updates return true.
		# Parameters : 
			1) $tbl_name* = name of table 
			2) $data_array* = array('column_name1'=>$column_val1,'column_name2'=>$column_val2);
			3) $pri_col* = primary key or column name depending on which update query need to fire.
			4) $id* = primary column or condition column value.

		# How to call:
			$this->master_model->updateRecord('tbl_name',$data_array,$pri_col,$id)
	*/
	public function updateRecord($tbl_name,$data_array,$where_arr)
	{
		$this->db->where($where_arr,NULL,FALSE);
		if($this->db->update($tbl_name,$data_array))
		{return true;}
		else
		{return false;}
	}
	
	
	/*
		# function deleteRecord($tbl_name,$pri_col,$id)
		# * indicates paramenter is must
		# Use : 
			1) delete record from table, on successful deletion returns true.
		# Parameters : 
			1) $tbl_name* = name of table 
			2) $pri_col* = primary key or column name depending on which update query need to fire.
			3) $id* = primary column or condition column value.

		# How to call:
			$this->master_model->deleteRecord('tbl_name','pri_col',$id)
		# It will useful while deleting record from  single table. delete join will not work here.
	*/
	public function deleteRecord($tbl_name,$pri_col,$id)
	{
		$this->db->where($pri_col,$id);
		if($this->db->delete($tbl_name))
		{return true;}
		else
		{return false;}
	}
	
	/* 
		# function createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
		# * indicates paramenter is must
		# Use : 
			1) create thumb of uploaded image.
		# Parameters : 
			1) $file_name* = name of uploaded file 
			2) $path* = path of directory
			3) $width* = width of thumb
			4) $height* = height of thumb
			5) $maintain_ratio = if need to maintain ration of original image then pass true, in this case thumb may vary in
								height and width provided. default it is FALSE.

		# How to call:
			$this->master_model->createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
			
		# !!Important : thumb foler  name must be 'thumb'
	*/


	public function resize_img($img, $box_w, $box_h)
	{
		$new = imagecreatetruecolor($box_w, $box_h);

    if($new === false) {
        //creation failed -- probably not enough memory
        return null;
    }


    //Fill the image with a light grey color
    //(this will be visible in the padding around the image,
    //if the aspect ratios of the image and the thumbnail do not match)
    //Replace this with any color you want, or comment it out for black.
    //I used grey for testing =)
    $fill = imagecolorallocate($new, 255, 255, 255);

    imagefill($new, 0, 0, $fill);

    //compute resize ratio
    $hratio = $box_h / imagesy($img);
    $wratio = $box_w / imagesx($img);
    $ratio = min($hratio, $wratio);

    //if the source is smaller than the thumbnail size, 
    //don't resize -- add a margin instead
    //(that is, dont magnify images)
    if($ratio > 1.0)
        $ratio = 1.0;

    //compute sizes
    $sy = floor(imagesy($img) * $ratio);
    $sx = floor(imagesx($img) * $ratio);

    //compute margins
    //Using these margins centers the image in the thumbnail.
    //If you always want the image to the top left, 
    //set both of these to 0
    $m_y = floor(($box_h - $sy) / 2);
    $m_x = floor(($box_w - $sx) / 2);

    //Copy the image data, and resample
    //
    //If you want a fast and ugly thumbnail,
    //replace imagecopyresampled with imagecopyresized
    if(!imagecopyresampled($new, $img,
        $m_x, $m_y, //dest x, y (margins)
        0, 0, //src x, y (0,0 means top left)
        $sx, $sy,//dest w, h (resample to this size (computed above)
        imagesx($img), imagesy($img)) //src w, h (the full size of the original)
    ) {
        //copy failed
        imagedestroy($new);
        return null;
    }
    //copy successful
    return $new;
	var_dump($new);
	exit;
	}

	public function createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
	{
		$config_1['image_library']='gd2';
		$config_1['source_image']=$path.$file_name;		
		$config_1['create_thumb']=TRUE;
		$config_1['maintain_ratio']=$maintain_ratio;
		$config_1['thumb_marker']='';
		$config_1['new_image']=$path."small/".$file_name;
		//$config_1['new_image']=$path."thumb/".$file_name;
		$config_1['width']=$width;
		$config_1['height']=$height;
		$this->load->library('image_lib',$config_1);	
		$this->image_lib->initialize($config_1);
		$this->image_lib->resize();
		
		if(!$this->image_lib->resize())
		echo $this->image_lib->display_errors();
		
	}
	
	public function createThumbCourse($file_name,$path,$width,$height,$maintain_ratio=FALSE)
	{
		$config_2['image_library']='gd2';
		$config_2['source_image']=$path.$file_name;
		$config_2['create_thumb']=TRUE;
		$config_2['maintain_ratio']=$maintain_ratio;
		$config_2['thumb_marker']='';
		$config_2['new_image']=$path."small/".$file_name;
		$config_2['width']=$width;
		$config_2['height']=$height;
		$this->load->library('image_lib',$config_2);
		$this->image_lib->initialize($config_2);
		$this->image_lib->resize();

		if(!$this->image_lib->resize())
		echo $this->image_lib->display_errors();
	}
	
	/* create slug */
	function create_slug($phrase,$tbl_name,$title_col,$pri_col='',$id='',$maxLength=100000000000000)
	{
		$result = strtolower($phrase);
		$result = preg_replace("/[^A-Za-z0-9\s-._\/]/", "", $result);
		$result = trim(preg_replace("/[\s-]+/", " ", $result));
		$result = trim(substr($result, 0, $maxLength));
		$result = preg_replace("/\s/", "-", $result);
		$slug=$result;
		if($id!="")
		{
			$this->db->where($pri_col.' !=' ,$id);
		}
		$rst=$this->db->get_where($tbl_name,array($title_col=>$slug));
		
		if($rst->num_rows() > 0)
		{
			$count=$rst->num_rows()+1;
			return $slug=$slug.$count;
		}
		else
		{return $slug;}
	}
	
	public function video_image($url)
	{
		$image_url = parse_url($url);
		if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
		$array = explode("&", $image_url['query']);
		return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
		} else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
		$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
		return $hash[0]["thumbnail_large"];
		}
	}
	
	public function document_upload($file_name,$id,$config,$company_id,$config1)
	{ 
		$this->load->library('upload');
		$this->upload->initialize($config1);
	
		if($this->upload->do_upload($file_name))
		{  echo 'model';exit;
			$dt=$this->upload->data();
			$config_1['image_library']='gd2';
			$config_1['source_image']="uploads/project_document/".$dt['file_name'];
			$config_1['thumb_marker']='';
			$this->load->library('image_lib', $config_1);
			$this->image_lib->initialize($config_1);
			$this->image_lib->resize();	
			
			$qry=array('company_id'=>$company_id,'project_id'=>$id,'document_name'=>$dt['file_name']);
			if($this->db->insert('project_document',$qry))
			{ //echo $this->db->last_query();
		//	exit;
				return true;
			}
			else
			{return false;
			}
		}
		else
		{
			//print_r($this->upload->display_errors());	
		}
	}

	public $master_skill=array();
	
	public function get_all_skill($parent,$level=FALSE)
	{
		global $master_skill;
		if($level!="")
		{$this->db->where('depth != '.$level.'',NULL,FALSE);}
		$this->db->where('parent_id',$parent);
		$this->db->order_by('skill_name');
		$rst=$this->db->get('skill_master');
		if($rst->num_rows() >0)
		{
			$row=$rst->result();
			foreach($row as $row)
			{
				$master_skill[$row->skill_id]=$row;
				$this->get_all_skill($row->skill_id,$level);
			}
		}
		return $master_skill;
	}
	
	public function getDepthArray($result, $fldParent="", $fldId="", $parent=0, $level=0, $finalArr=array(),$rt=true)
	{
		if(sizeof($result)>0 && $fldParent && $fldId){
			foreach($result as $rs){
				if($rs[$fldParent]== $parent){
					$rs['depth']=$level++;								
					$finalArr[]=$rs;
					$rt=false;
					$finalArr = $this->getDepthArray($result,$fldParent,$fldId,$rs[$fldId],$level,$finalArr,$rt);
					$level--;
				}
			}
		}
		return $rt ? $result : $finalArr;
	}
	
	public function getDepthArrayCustome($result, $fldParent="", $fldId="", $parent=0, $level=0, $finalArr=array(),$rt=true)
	{
		if(sizeof($result)>0 && $fldParent && $fldId){
			foreach($result as $rs){
				if($rs[$fldParent]== $parent){
					$rs['depth']=$level++;	
					if($rs['depth']!='2')
					{							
						$finalArr[]=$rs;
					}
					$rt=false;
					if($rs['depth']!='3')
					{
						$finalArr = $this->getDepthArrayCustome($result,$fldParent,$fldId,$rs[$fldId],$level,$finalArr,$rt);
					}
					$level--;
				}
			}
		}
		return $rt ? $result : $finalArr;
	}
	
	public function getTreeDD($arrD=array(), $val="categoryID", $text="categoryTitle", $nmParent="parentID",$selected="")
	{
		$options = "";
		$arr = $this->getDepthArray($arrD, $nmParent, $val);
		if(!empty($arr) && sizeof($arr)>=1 && is_array($arr)) {		
			foreach($arr as $k=>$v) {
				if($v) {				
					$sel = "";				
					if($v[$val] == "$selected") $sel=' selected="selected"';
					if($v["depth"])
						$v[$text] = str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$v["depth"])."&rArr; ".$v[$text];
					$options.= "\n<option value=\"".$v[$val]."\"".$sel.">".$v[$text]."</option>";	 
				}
			}
		}
		return $options; 
	}
	
	public function getRecordsCourse($min,$max,$count=FALSE)
	{
		$this->db->where('tbl_course.isPublish','1');
		$this->db->where('tbl_course.editable','0');
		$this->db->where('tbl_course_tmp.draftMode','1');
		$this->db->join('tbl_course_tmp','tbl_course_tmp.tmpID=tbl_course.tmpID','left');
		if($min!="" && $max!="")
		{
			$this->db->where('tbl_course.couponFee >=',$min);
			if($max!='$')
			{$this->db->where('tbl_course.couponFee <=',$max);			
			}
		
		}
		
		if($count==TRUE)
		{
			$result=$this->db->count_all_results('tbl_course');
		}
		else
		{
			
			$rst=$this->db->get('tbl_course');
			$result=$rst->result_array();
		}
		return $result;
	}
	
	
	/* convert second to time */
	function secondsToTime($seconds)
	{
		// get hours
		$hours = floor($seconds / (60 * 60));
	 
		// get minutes
		$divisor_for_minutes = $seconds % (60 * 60);
		$minutes = floor($divisor_for_minutes / 60);
	 
		// get seconds
		$divisor_for_seconds = $divisor_for_minutes % 60;
		$seconds = ceil($divisor_for_seconds);
	 
		// return in array
		$obj = array(
			"h" => (int) $hours,
			"m" => (int) $minutes,
			"s" => (int) $seconds,
		);
		return $obj;
	 }
	 
	 /* get total time or audio and video */
	 public function get_total_chapter_time($courseSlug)
	 {
		if($courseSlug!="")
		{
			//echo "slug".$courseSlug;
			//$sectionarr=array();
			
			$this->db->join('tbl_section AS sec','sec.tmpID=course.tmpID');
			$sectionInCourse=$this->getRecords('tbl_course AS course',array('course.tmpID'=>$courseSlug),"",array('sec.sectionID'=>'ASC'));
			//echo $this->db->last_query();
			//exit;
			//$data=array('sectionInCourse'=>$sectionInCourse);
			$sectionarr=array();
		
			foreach($sectionInCourse as $sec)
			{ 
				$sectionarr[]=$sec['sectionID'].',';
			}
			//print_r($sectionarr);
			
			//tbl_chapter_complete
			if(count($sectionarr)>0)
			{
			$this->db->where_in('tbl_chapter.sectionID',$sectionarr);
			$this->db->join('tbl_section','tbl_section.sectionID=tbl_chapter.sectionID');
			$chap=$this->getRecords('tbl_chapter',array('tbl_chapter.isApprove'=>'1'),'*',array('tbl_chapter.sortOrder'=>'ASC'));
			
			if(count($chap)>0)
			{
				$videotime='';
				$audiotime='';
				foreach($chap as $cp)
				{
					$extra_mins='';
					if($cp['docFilename']!="" || $cp['forumLink']!="" || $cp['quizTitle']!="")
					{
						$extra_mins=1800;
					}
					
					$videotime+=$cp['videoTime'];
					$audiotime+=$cp['audioTime'];
				}
				
				
				$tot_time=$videotime+$audiotime+$extra_mins;
				$total_time=$this->secondsToTime($tot_time);
				return $total_time;
			}
			else
			{
				return $total_time=array();
			}
			}
			else
			{
				return $total_time=array();
			}
		}
	 }
	
   /* function for showing time ago */	 
   function time_ago($timestamp) {
    $output = ""; //string value to be returned
    $now = time();
    $createtime = strtotime($timestamp); //mysql timestamp 
    $difference = abs($now - $createtime);

    $years = floor($difference / (365*60*60*24));
    $months = floor(($difference-$years*365*60*60*24)/(30*60*60*24));
    $days = floor(($difference - $years*365*60*60*24-$months*30*60*60*24)/(60*60*24));
    $hours = floor($difference / 3600);
    $minutes = floor($difference / 60);
    $seconds = floor($difference);


    if ($output == "") {//to check whether it is years old
        if ($years > 1) {
            $output = $years . " years ago";
        } elseif ($years == 1) {
            $output = $years . " year ago";
        }
    }

    if ($output == "") {//to check whether it is months old
        if ($months > 1) {
            $output = $months . " months ago";
        } elseif ($months == 1) {
            $output = $months . " month ago";
        }
    }

    if ($output == "") {//to check whether it is days old
        if ($days > 1) {
            $output = $days . " days ago";
        } elseif ($days == 1) {
            $output = $days . " day ago";
        }
    }

    if ($output == "") {//to check whether it is hours old
        if ($hours > 1) {
            $output = $hours . " hours ago";
        } elseif ($hours == 1) {
            $output = $hours . " hour ago";
        }
    }
     
    if ($output == "") {//to check whether it is minutes old
        if ($minutes > 1) {
            $output = $minutes . " minutes ago";
        } elseif ($minutes == 1) {
            $output = $minutes . " minute ago";
        }
    }
    
    if ($output == "") {//to check whether it is seconds old
        if ($seconds > 1) {
            $output = $seconds . " seconds ago";
        } elseif ($seconds == 1) {
            $output = $seconds . " second ago";
        }
    }
    return $output;
  }
  
  /* for showing course rating */
  public function ratings($course_id)
  {
	   
	/* get total ratings of particular course */
	$this->db->where("fkCourseID",$course_id);
	$res_total=$get_cnt=$this->master_model->getRecordCount('tbl_course_rating');
	if($res_total)
	{
		$get_total=$res_total;
	}
	else
	{
		$get_total=0;
	}
	
	
	/* get average of partucular course */
	$approx_avg=0;
	$star='';
	$this->db->select_avg('rating');
	$this->db->where("fkCourseID",$course_id);
	$get_avrage=$get_cnt=$this->master_model->getRecords('tbl_course_rating');
	if(count($get_avrage)>0)
	{
		$approx_avg=number_format($get_avrage[0]['rating'],1);
		
		if($approx_avg>0)
		{
			switch($approx_avg)
			{
				case (round($approx_avg)<=1) :
					$star='<i class="fa fa-star golden"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i>';
					break;
				case (round($approx_avg)<=2) :
					$star= '<i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i>';
					break;
				case (round($approx_avg)<=3) :
					$star='<i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i>';
					break;
				case (round($approx_avg)<=4) :
					$star='<i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star gray"></i>';
					break;
				case (round($approx_avg)<=5) :
					$star= '<i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star golden"></i><i class="fa fa-star golden"></i>';
					break;	
			}
			
		}
		else
		{
			$star= '<i class="fa fa-star gray"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i><i class="fa fa-star gray"></i>';
		}
	}
	$result=array('get_total'=>$get_total,'get_avrage'=>$approx_avg,'star'=>$star);
	return $result;
  }
  
  /* show rating graph */
  public function rating_graph($course_id,$star,$total_result)
  {
	  
	  /* five star graph */
	  $this->db->where("fkCourseID",$course_id);
	  $this->db->where("rating",$star);
	  $all_star=$this->master_model->getRecordCount('tbl_course_rating');
	  $star_graph=($all_star/$total_result)*100;//total percentager of star.
	  
	  $returnCount=0;
	  $returnCount=$star_graph/10;
	  $modCount=$star_graph%10;
	  if($modCount>5)
	  {
		 $returnCount+1; 
	  }
	  	  
	  $result=array('total_star'=>$all_star,'total_graph'=>round($returnCount));
	  return $result;
  }
}
?>