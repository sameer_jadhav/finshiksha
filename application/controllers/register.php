<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
session_start();

class Register extends CI_Controller {
	 //protected $nonce_chars;
	public function __construct()	
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		
		$this->load->library('curl');
		$this->load->library('session');
		$this->load->library('facebook');
		$this->load->library('google');
		$this->load->helper('menu_helper');
		$this->load->helper('cookie');	
		$this->config->load('facebook');
	}


	    public function index()
	{
		$userData = $this->session->userdata('user');
		if($userData){
			redirect(base_url().'profile');
		}
		$google_client_id = $this->config->item("google_client_id");
		//var_dump($google_client_id);exit;
		/*$google_client_secret = $this->config->item("google_client_secret");
		$google_redirect_url = $this->config->item("google_redirect_url");*/
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);

		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		
		$data  = array(
						"menu_data"=>$menu_data
						);
		$userData = array();
		if($this->facebook->is_authenticated()){
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
		}
		else
		{
			$data['authUrl'] =  $this->facebook->login_url();
			//echo $data['authUrl'];exit;
		}
		/*$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email') . '&redirect_uri=' . urlencode($google_redirect_url) . '&response_type=code&client_id=' . $google_client_id . '&access_type=online';*/
		

		$data['google_login_url']=$this->google->get_login_url();;
		$data["stored_cookie"] = $stored_cookie;

		$this->load->view('register',$data);
	}	

	public function user_fb(){
		$this->load->library('facebook');
		try{
			
			$this->facebook->is_authenticated();
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email');
			print_r($userProfile);exit;
			$obj_rest = new Restcall();
			$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers?email=".$userProfile['email'],
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 =>  array("email"=>$userProfile['email']),
						
						 );
		
			$res = $obj_rest->_execute($options);
			//var_dump($res);exit;
			if(count($res)==0){
				$args = array(
										"email"			=>$userProfile['email'],
										"first_name"	=>$userProfile['first_name'],
										"last_name"		=>$userProfile['last_name'],
										"oauthId"		=>$userProfile['id'],
										"password"		=>$userProfile['id'],
										"oauthProvider"	=>"facebook",
										"billing"		=> array(
																"first_name"=>$userProfile['first_name'],
																"last_name"=>$userProfile['last_name'],
																"email"		=>$userProfile['email']
																
																),
										"shipping"		=> array(
																"first_name" =>$userProfile['first_name'],
																"last_name"	 =>$userProfile['last_name']
																 )
									 );
				
				$options =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 =>  array(),
								"body"	 => $args
								 );
				
				$user_data = $obj_rest->_execute($options);
				if($user_data['id']!=""){
						$session_data  = array('id' => $user_data['id'],
												'oauthId'=>$userProfile['id'] ,
												"oauthProvider"	=>"facebook",
												"email"=>$userProfile['email'],
												"user_display_name" => $userProfile['first_name']
												);
						$this->session->set_userdata('user', $session_data);
						////////manage redirect
						$redirect = $this->session->userdata('redirect_url');
						if($redirect==""){
							header("Location: ".base_url());
						}else{
							$this->session->unset_userdata('redirect_url');
							header("Location: ".$redirect);
						}
				}
			}else{
				$session_data  = array('id' => $res[0]['id'],
										'oauthId'=>$res[0]['oauthId'] ,
										"oauthProvider"	=>$res[0]['oauthProvider'],
										"email"=>$res[0]['email'],
										"user_display_name" => $userProfile['first_name']
										);
				$this->session->set_userdata('user', $session_data);
				////////manage redirect
				$redirect = $this->session->userdata('redirect_url');
				if($redirect==""){
					header("Location: ".base_url());
				}else{
					$this->session->unset_userdata('redirect_url');
					header("Location: ".$redirect);
				}
			}
		}catch (Exception $e){
			print_r($e);
		}
		
	}


	public function user(){
		$obj_rest = new Restcall();

		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('customer_name','Name','required|xss_clean');
			$this->form_validation->set_rules('email_id','Email ID','required|xss_clean');
			$this->form_validation->set_rules('user_password','Password','required|xss_clean');
			$this->form_validation->set_rules('phone_no','Phone Number','required|xss_clean');
			if($this->form_validation->run())
				{
		 		$name=$this->input->post('customer_name',true);
				$email_id=$this->input->post('email_id',true);
				$user_pass=$this->input->post('user_password',true);
				$phone_no=$this->input->post('phone_no',true);

				
							$args = array(
										"email"			=>$email_id,
										"first_name"	=>$name,
										"password"		=>$user_pass,
										"first_name"    => $name,
										"display_name"	=> $name,
										"billing"		=> array(
																"first_name"=>$name,
																"email"		=>$email_id,
																"phone"		=>$phone_no
																),
										"shipping"		=> array("first_name" =>$name )
									 );
						$options =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 =>  array(),
								"body"	 => $args
								 );
				
						$user_data = $obj_rest->_execute($options);
						//print_r($user_data);
						//echo json_encode($user_data);
						

						if((isset($user_data['code'])) && ($user_data['code']=="registration-error-email-exists")){
							$response['status'] = 'error';
							$response['message'] = $user_data['message'];
							 
						}else{
							$response['status'] = 'success';
							$response['message'] = "Customer registered Successfully. Login to Continue";
							$this->session->set_flashdata('message', "registration-success");

							//header("Location: ".base_url()."login");
						}
				}else{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
				}
				
				echo json_encode($response);
				//if($user_data)
		 }
	}
	

}