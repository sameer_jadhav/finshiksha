<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php');
class Shop extends CI_Controller {
	 //protected $nonce_chars;
	public function __construct()	
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		
		$this->load->library('curl');
		$this->load->helper('menu_helper');
	}

	public function index()
	{
		
		$obj_rest = new Restcall();
		$this->load->helper('cookie');			
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		
		$options_cat =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products/categories",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array()
						 );
		$category = $obj_rest->_execute($options_cat);
		unset($category[0]);
		$category = array_values($category);

		$options_product =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?status=publish",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("status"=>"publish")
						 );
		
		$products = $obj_rest->_execute($options_product);
		//print_r($products);exit;

		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();
		//print_r($cart_data);exit;
		

		$data  = array(

						
						"products" 		=> $products,
						"stored_cookie" => $stored_cookie,
						"category"		=> $category,
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						"meta_title"	=>"",
						"meta_keyword"	=>"",
						"meta_desc"		=> "",
						);
		$this->load->view('category',$data);
	}

}