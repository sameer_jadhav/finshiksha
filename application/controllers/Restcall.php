<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restcall extends CI_Controller {
	 //protected $nonce_chars;
	public function __construct()	
    {
         parent :: __construct();

		 $this->nonce_chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		 $this->consumerKey = "ck_ca26424954d967e4de797b033f8ff66639af6dcc";
		 $this->consumerSecret = "cs_81e8197854238d05e280733b0d54e308cff493dc";
		 $this->tokenSecret = "";
		 $this->accessToken = "";
		 $this->appKey = "abdjsbdwddasjbdfin";
		 $this->appSecret = "ldfvnkjdbckjshfin";
		 $this->domain = $this->config->item('api_url');
		 $this->expiry = "108000";
		 
	}

	public function index(){

		echo 111;
	}
	public function _execute($options = array()){

	    if (!isset($options['url'])) {
	        return;
	    }
	    //echo $options['url']."?".$options['param'];exit;
	    $ch = curl_init();
	    

	    $method = (isset($options['method'])) ? $options['method'] : 'GET';
	    //curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	    //curl_setopt($ch, CURLOPT_ENCODING,  '');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

	    if (isset($options['auth']) && $options['auth']) {
	       	curl_setopt($ch, CURLOPT_URL, $options['url']);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	            "Content-Type: application/json"
	        ));
	    }

	    if (isset($options['body']) && !empty($options['body'])) {
	        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($options['body']));
	    }
	   
	    $result = curl_exec($ch);
	    if (curl_error($ch)) {
    	$error_msg = curl_error($ch);
		}
		//print_r(get_headers($options['url']));
		
	    
	    return json_decode($result,true);
	
	}


	public function _executeAuth($options = array())
	{
	    if (!isset($options['url'])) {
	        return;
	    }
	    //echo $options['url']."?".$options['param'];exit;
	    $ch = curl_init();
	    

	    $method = (isset($options['method'])) ? $options['method'] : 'GET';
	    //curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	    //curl_setopt($ch, CURLOPT_ENCODING,  '');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

	    if (isset($options['auth']) && $options['auth']) {
	        $signature = $this->generateSignature($options, $timestamp, $nonce, $signatureMethod, $version);

	       
	       	curl_setopt($ch, CURLOPT_URL, $options['url']);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	            
	            "Content-Type: application/json"
	        ));
	    }

	    if (isset($options['body']) && !empty($options['body'])) {
	        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($options['body']));
	    }
	   
	    $result = curl_exec($ch);
	    if (curl_error($ch)) {
    	$error_msg = curl_error($ch);
		}
		//print_r(get_headers($options['url']));
		
	    
	    return json_decode($result,true);
	}

	public function getAuthenticatedURL( $uri, $querystring, $http_request_type,$protocol=NULL){
		$appkey = $this->appKey;
		$appsecret = $this->appSecret;
		$domain = $this->domain;
		//echo $domain;exit;
		$expiry = $this->expiry;
	    strlen($querystring) == 0 || $querystring .= '&';
	    $querystring .= 'accesskey=' . $appkey . '&timestamp=' . (time() + $expiry);
	    $string_to_sign = $http_request_type . $uri . '?' . $querystring;
	    $auth_token = bin2hex(hash_hmac("SHA1", $string_to_sign, $appsecret));
	    $url = '';
	    if($protocol != NULL){
	        $url = $protocol.'://';
	    }else{
	        if (!preg_match('/^https?/', $domain)) {
	            $url = 'http://';
	        }
	    }
	    $url .= rtrim($domain, '/') . '/' . ltrim($uri, '/') . '?' . $querystring . '&authtoken=' . $auth_token;
	    return $url;
}

	 public function getNonce($length = 5)
    {
        $result = '';
        $cLength = strlen($this->nonce_chars);
        for ($i = 0; $i < $length; $i++) {
            $rnum = rand(0, $cLength - 1);
            $result .= substr($this->nonce_chars, $rnum, 1);
        }
        $this->parameters['oauth_nonce'] = $result;
        return $result;
    }


    public function generateSignature($request, $timestamp, $nonce, $signatureMethod, $version)
	{

		$url_comps = parse_url($request['url']);
		
		if(empty($request['param'])){
			$req_param  = array('oauth_consumer_key'		=>rawurlencode($this->consumerKey)
								 ,'oauth_nonce' 			=>rawurlencode($nonce)
								 ,'oauth_signature_method'	=>rawurlencode($signatureMethod)
								 ,'oauth_timestamp'			=>rawurlencode($timestamp)
								 , 'oauth_token'			=>rawurlencode($this->accessToken)
								 ,'oauth_version'			=>rawurlencode($version)
								
								 );	
		}else{
			if( (count($request['param'])>0) && (count($request['param'])<2)){
				foreach ($request['param'] as $key=>$value)
		        {
		        	$param_name = $key;
		        	$param_value = $value;
		        }

				$req_param  = array('oauth_consumer_key'		=>rawurlencode($this->consumerKey)
									 ,'oauth_nonce' 			=>rawurlencode($nonce)
									 ,'oauth_signature_method'	=>rawurlencode($signatureMethod)
									 ,'oauth_timestamp'			=>rawurlencode($timestamp)
									 , 'oauth_token'			=>rawurlencode($this->accessToken)
									 ,'oauth_version'			=>rawurlencode($version)
									 , $param_name				=>rawurlencode($param_value) 
									 );	
			}else if( (count($request['param'])>1) && (count($request['param'])<3)){

				foreach ($request['param'] as $key=>$value)
		        {
		        	$param_name[] = $key;
		        	$param_value[] = $value;
		        }

				$req_param  = array('oauth_consumer_key'		=>rawurlencode($this->consumerKey)
									 ,'oauth_nonce' 			=>rawurlencode($nonce)
									 ,'oauth_signature_method'	=>rawurlencode($signatureMethod)
									 ,'oauth_timestamp'			=>rawurlencode($timestamp)
									 , 'oauth_token'			=>rawurlencode($this->accessToken)
									 ,'oauth_version'			=>rawurlencode($version)
									 , $param_name[0]			=>rawurlencode($param_value[0]) 
									 , $param_name[1]			=>rawurlencode($param_value[1]) 
									 );	
			}else{
				foreach ($request['param'] as $key=>$value)
		        {
		        	$param_name[] = $key;
		        	$param_value[] = $value;
		        }

				$req_param  = array('oauth_consumer_key'		=>rawurlencode($this->consumerKey)
									 ,'oauth_nonce' 			=>rawurlencode($nonce)
									 ,'oauth_signature_method'	=>rawurlencode($signatureMethod)
									 ,'oauth_timestamp'			=>rawurlencode($timestamp)
									 , 'oauth_token'			=>rawurlencode($this->accessToken)
									 ,'oauth_version'			=>rawurlencode($version)
									 , $param_name[0]			=>rawurlencode($param_value[0]) 
									 , $param_name[1]			=>rawurlencode($param_value[1]) 
									 , $param_name[2]			=>rawurlencode($param_value[2]) 
									 , $param_name[3]			=>rawurlencode($param_value[3]) 
									 );	
			}
			//print_r($req_param);exit;
		}
        
		ksort($req_param);
		
		$query_string = "";
		foreach ($req_param as $key => $value) {
			# code...
			$query_string .= $key."=".$value."&";
		}
		$query_string = rtrim($query_string,"&");
		
		
	  /*  $base2 =  $request['method'] . "&" . rawurlencode("http://".$url_comps['host'].$url_comps['path']) . "&".rawurlencode($url_comps['query'])
        . rawurlencode("&oauth_consumer_key=" . rawurlencode($this->consumerKey)
        . "&oauth_nonce=" . rawurlencode($nonce)
        . "&oauth_signature_method=" . rawurlencode($signatureMethod)
        . "&oauth_timestamp=" . rawurlencode($timestamp)
        . "&oauth_token=" . rawurlencode($this->accessToken)
        . "&oauth_version=" . rawurlencode($version));*/
      	
        //echo $base."----------";
		$base =  $request['method'] . "&" . rawurlencode("http://".$url_comps['host'].$url_comps['path']) 
        . "&".rawurlencode($query_string);
		//echo $base."----------";
		
	    $key = rawurlencode($this->consumerSecret) . '&' . rawurlencode($this->tokenSecret);
	    //echo $key ;exit;
	    $signature = base64_encode(hash_hmac('sha1', $base, $key, true));
	    
	    return $signature;
	}

}