<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
session_start();

class Login extends CI_Controller {
	public function __construct()	
    {
        parent::__construct();

       	
		$this->load->library('curl');
		$this->load->library('session');
		$this->load->library('facebook');
		$this->load->library('google');
		$this->load->helper('menu_helper');
		$this->load->helper('cookie');	
		$this->config->load('google_config');
		$this->config->load('google_config');
		//$client_id = '< Generated Client Id >';
		/*$client_secret = '465807442212-n7i2ai7anaoo7lkpalsjeuvtpc5nr77m.apps.googleusercontent.com';
		$redirect_uri = 'http://dev.skin.com/login/user_google';
		$simple_api_key = 'iE8Z17hLB8OZWy5pxVQBiYaI';*/
		


    }


    public function index()
	{
		$userData = $this->session->userdata('user');
		if($userData){
			redirect(base_url().'profile');
		}
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);
		$data  = array(
						"menu_data"=>$menu_data
						);
		//check fb login
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}

		$userData = array();
		if($this->facebook->is_authenticated()){
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
		}
		else
		{
			$data['authUrl'] =  $this->facebook->login_url();
			//echo $data['authUrl'];exit;
		}

		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();
		//print_r($cart_data);exit;

		$data["stored_cookie"] = $stored_cookie;
		$data['google_login_url']=$this->google->get_login_url();
		$data['cart'] 	= $cart_data['cart'];
		$data["product_fly"] =$cart_data['product_fly'];
		$data["cart_total"]	= $cart_data['cart_total'];

		//print_r($data);exit;
		//print_r($data);exit;
		$this->load->view('login',$data);
	}	
	public function doLogin($value='')
	{		
		$obj_rest = new Restcall();
		//print_r($_POST);exit;
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/jwt-auth/v1/token",
						"method" =>"POST",
						"auth"	 =>	"true",
						"param"	 =>  array(),
						"body" => array( 'username' => $this->input->post('username'), 'password' => $this->input->post('password') )
						 );
		
		$res = $obj_rest->_execute($options);
		//print_r($res);exit;
		$res['redirect_url'] = "";
		if (isset($res['token']) && $res['token']) {
			$this->session->set_userdata('user', $res);		
		}else{

			echo json_encode($res);
			die();
		}

		$redirect = $this->session->userdata('redirect_url');
		if($redirect!=""){

			$this->session->unset_userdata('redirect_url');
			//echo json_encode($redirect);
			//header("Location: ".$redirect);
			$res['redirect_url'] = $redirect;
		}
		echo json_encode($res);

	}

	public function user_fb(){
		$this->load->library('facebook');
		try{
			
			$this->facebook->is_authenticated();
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email');
			//print_r($userProfile);exit;
			$obj_rest = new Restcall();
			$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers?email=".$userProfile['email'],
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 =>  array("email"=>$userProfile['email']),
						
						 );
		
			$res = $obj_rest->_execute($options);
			//var_dump($res);exit;
			if(count($res)==0){
				$args = array(
										"email"			=>$userProfile['email'],
										"first_name"	=>$userProfile['first_name'],
										"last_name"		=>$userProfile['last_name'],
										"oauthId"		=>$userProfile['id'],
										"password"		=>$userProfile['id'],
										"oauthProvider"	=>"facebook",
										"billing"		=> array(
																"first_name"=>$userProfile['first_name'],
																"last_name"=>$userProfile['last_name'],
																"email"		=>$userProfile['email']
																
																),
										"shipping"		=> array(
																"first_name" =>$userProfile['first_name'],
																"last_name"	 =>$userProfile['last_name']
																 )
									 );
				
				$options =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 =>  array(),
								"body"	 => $args
								 );
				
				$user_data = $obj_rest->_execute($options);
				if($user_data['id']!=""){
						$session_data  = array('id' => $user_data['id'],
												'oauthId'=>$userProfile['id'] ,
												"oauthProvider"	=>"facebook",
												"email"=>$userProfile['email'],
												"user_display_name" => $userProfile['first_name']
												);
						$this->session->set_userdata('user', $session_data);
						////////manage redirect
						$redirect = $this->session->userdata('redirect_url');
						if($redirect==""){
							header("Location: ".base_url());
						}else{
							$this->session->unset_userdata('redirect_url');
							header("Location: ".$redirect);
						}
				}
			}else{
				$session_data  = array('id' => $res[0]['id'],
										'oauthId'=>$res[0]['oauthId'] ,
										"oauthProvider"	=>$res[0]['oauthProvider'],
										"email"=>$res[0]['email'],
										"user_display_name" => $userProfile['first_name']
										);
				$this->session->set_userdata('user', $session_data);
				////////manage redirect
				$redirect = $this->session->userdata('redirect_url');
				if($redirect==""){
					header("Location: ".base_url());
				}else{
					$this->session->unset_userdata('redirect_url');
					header("Location: ".$redirect);
				}
			}
		}catch (Exception $e){
			print_r($e);
		}
		
	}

	public function user_google(){
		$google_client_id = $this->config->item("google_client_id");
		//var_dump($google_client_id);exit;
		$google_client_secret = $this->config->item("google_client_secret");
		$google_redirect_url = $this->config->item("google_redirect_url");
			
		try{		
			$userToken=$this->GetAccessToken($google_client_id,$google_redirect_url,$google_client_secret,$_GET['code']);
			//print_r($userToken);exit;
			$userProfile = $this->GetUserProfileInfo($userToken['access_token']);
			//print_r($userProfile);exit;
			$obj_rest = new Restcall();
			$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers?email=".$userProfile['email'],
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 =>  array("email"=>$userProfile['email']),
						
						 );
		
			$res = $obj_rest->_execute($options);
			if($userProfile['given_name']==null){ $userProfile['given_name']=""; };
			if($userProfile['family_name']==null) { $userProfile['family_name']="";};
			if(count($res)==0){
				$args = array(
										"email"			=>$userProfile['email'],
										"first_name"	=>$userProfile['given_name'],
										"last_name"		=>$userProfile['family_name'],
										"oauthId"		=>$userProfile['id'],
										"password"		=>$userProfile['id'],
										"oauthProvider"	=>"google",
										"billing"		=> array(
																"first_name"=>$userProfile['given_name'],
																"last_name"=>$userProfile['family_name'],
																"email"		=>$userProfile['email']
																
																),
										"shipping"		=> array(
																"first_name" =>$userProfile['given_name'],
																"last_name"	 =>$userProfile['family_name']
																 )
									 );
				//print_r(json_encode($args));exit;
				$options =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 =>  array(),
								"body"	 => $args
								 );
				
				$user_data = $obj_rest->_execute($options);
				
				if($user_data['id']!=""){
						$session_data  = array('id' => $user_data['id'],
												'oauthId'=>$userProfile['id'] ,
												"oauthProvider"	=>"google",
												"email"=>$userProfile['email'],
												"user_display_name" => $userProfile['given_name']
												);
						$this->session->set_userdata('user', $session_data);
						////////manage redirect
						$redirect = $this->session->userdata('redirect_url');
						if($redirect==""){
							header("Location: ".base_url());
						}else{
							$this->session->unset_userdata('redirect_url');
							header("Location: ".$redirect);
						}
				}
			}else{
				$session_data  = array('id' => $res[0]['id'],
										'oauthId'=>$res[0]['oauthId'] ,
										"oauthProvider"	=>$res[0]['oauthProvider'],
										"email"=>$res[0]['email'],
										"user_display_name" => $userProfile['given_name']
										);
				$this->session->set_userdata('user', $session_data);
				////////manage redirect
				$redirect = $this->session->userdata('redirect_url');
				if($redirect==""){
					header("Location: ".base_url());
				}else{
					$this->session->unset_userdata('redirect_url');
					header("Location: ".$redirect);
				}
			}
		}catch (Exception $e){
			print_r($e);
		}
		
	}

	public function set_currency($cur)
	{
		$this->session->unset_userdata('currency');
		$this->session->set_userdata('currency', $cur);			
		$this->session->set_userdata('currency_flag', "manual");	
		echo json_encode( array('status' =>true  ));
	}

	public function getGoogleUserData(){

	}

	function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {	
		$url = 'https://www.googleapis.com/oauth2/v4/token';			

		$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if($http_code != 200) 
			throw new Exception('Error : Failed to receieve access token');
		
		return $data;
	}


	function GetUserProfileInfo($access_token) {	
		$url = 'https://www.googleapis.com/oauth2/v2/userinfo?fields=name,given_name,family_name,email,gender,id,picture,verified_email';	
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		if($http_code != 200) 
			throw new Exception('Error : Failed to get user information');
			
		return $data;
	}
	
}