<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/passwordhash.php');
require_once(APPPATH.'controllers/cart.php');
class Profile extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		$this->load->helper('menu_helper');
	   	check_login_page('profile');
    }

	public function index()
	{
		$userdata = $this->session->userdata('user');
		$this->load->library('curl');
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);		
		$obj_rest = new Restcall();
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers/".$userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true"
						 );
		
		$user = $obj_rest->_execute($options);
		//print_r($userdata);exit;
		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();
		//print_r($cart_data);exit;

		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders?customer=". $userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array('customer' => $userdata['id'] ),
						"body" =>  array()
						 );
		
		$cust_orders = $obj_rest->_execute($options);	
		
		$data  = array(
			"menu_data"=>$menu_data,
			"user"=>$user,
			"cart" => $cart_data['cart'],
			"product_fly"	=> $cart_data['product_fly'],
			"cart_total"	=> $cart_data['cart_total'],
			"cust_orders" => $cust_orders
	);
		$this->load->view('profile',$data);
	}

	public function save(){
		
		$this->load->library('curl');
		if(isset($_POST))
		 {
		 	$userdata = $this->session->userdata('user');
		 	$this->form_validation->set_rules('customer_name','Name','required|xss_clean');
		 	$this->form_validation->set_rules('phone_no','Phone Number','required|xss_clean');
		 	if($this->form_validation->run())
				{
					$customer_name=$this->input->post('customer_name',true);
					$phone_no=$this->input->post('phone_no',true);
					$password=$this->input->post('user_confirm_password',true);
					///set user details
					$obj_rest = new Restcall();
					$args = array(
								"first_name"=>$customer_name,
								"billing" => array('phone' => $phone_no )
								);
					
					$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers/".$userdata['id'],
						"method" =>"POST",
						"auth"	 =>	"true",
						"params" => array(),
						"body"	 => $args
						 );
					$user_update = $obj_rest->_execute($options);
					if($password!=""){
						$this->set_password($password);
					}
					if(isset($user_update) && !empty($user_update)){
						$userdata['user_display_name'] = $customer_name;
						$new_session_data = $userdata;
						$this->session->set_userdata('user', $new_session_data);
						$response['status'] = 'success';
						$response['message'] = "Details updated successfully!!";
					}else{
						$response['status'] = 'error';
						$response['message'] = $user_update['message'];
					}
					echo json_encode($response);
				}else{
					$response['status'] = 'error';
					$response['message'] = "Some error occured,Please try again later";
					echo json_encode($response);
				}
		 }
	}

	public function changePassword(){
		$obj_pass = new PasswordHash(16,true);
		if(isset($_POST))
		 {
		 	$userdata = $this->session->userdata('user');
		 	$this->form_validation->set_rules('current_pass','Current Password','required|xss_clean');
		 	$this->form_validation->set_rules('new_pass','New Password','required|xss_clean');
		 	$this->form_validation->set_rules('confirm_pass','Confirm Password','required|xss_clean');
		 	if($this->form_validation->run())
				{

					$current_pass=$this->input->post('current_pass',true);
					$new_pass=$this->input->post('new_pass',true);
					$confirm_pass=$this->input->post('confirm_pass',true);
					$user = $this->db->where( array('user_email' => $userdata['email']) )->get('wp_users')->row();
					$hash_pass = $obj_pass->CheckPassword(trim($current_pass),$user->user_pass);

					if($hash_pass){
						$new_pass_hash =  $obj_pass->HashPassword(trim($new_pass));
						$this->db->update('wp_users',  array('user_pass' => $new_pass_hash  ), array('id' =>$user->ID ) );
						$response['status'] = 'success';
						$response['message'] = "Password updated successfully!!";
					}else{
						$response['status'] = 'error';
						$response['message'] = "Please enter correct current password";
					}
					echo json_encode($response);
					
				}
		 }
	}

	public function check_password(){
		$userdata = $this->session->userdata('user');

		$current_pass=$this->input->post('current_pass',true);
		$obj_pass = new PasswordHash(16,true);
		$user = $this->db->where( array('user_email' => $userdata['email']) )->get('wp_users')->row();
		$hash_pass = $obj_pass->CheckPassword(trim($current_pass),$user->user_pass);
		if($hash_pass){
			 //$response['status'] = 'false';
			 $state =  "true";
		}else{
			$state =  "Please enter correct current password";
			//return false;
			//$response['status'] = 'false';		
		}
		echo json_encode($state);
	}

	private function set_password($password){
		$userdata = $this->session->userdata('user');
		$user = $this->db->where( array('user_email' => $userdata['email']) )->get('wp_users')->row();
		if($user){
			$this->db->update('wp_users',  array('user_pass' => md5($password) ), array('id' =>$user->ID ) );
		}
	}


	
}