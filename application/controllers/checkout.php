<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once (APPPATH . '/libraries/razorpay-php/Razorpay.php');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php'); 
//require_once(APPPATH.'controllers/bluedart.php'); 
require_once(APPPATH.'controllers/TransactionRequestBean.php'); 
require_once(APPPATH.'controllers/TransactionResponseBean.php'); 
use Razorpay\Api\Api as RazorpayApi;
class Checkout extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		//echo rzp_test_KEY_ID;
		$api = new RazorpayApi('rzp_test_KEY_ID', 'rzp_test_KEY_SECRET');
		//print_r($api);
		$this->load->helper('menu_helper');
    }

	public function index()
	{
		
		if(get_cart_count()==0){
			redirect(base_url().'cart');
		}
		$this->load->library('curl');
		$this->load->helper('cookie');	
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);

		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
			
		$cart_data = array();
		$address_data = array();	
		if(isset($_POST) && !empty($_POST))
		{

			$obj_cart = new Cart();
			$cart_data = $obj_cart->calulate_cart_total();
			//print_r($this->session->userdata('pincode'));exit;
			$userdata = $this->session->userdata('user');	
			$address_id=$this->input->post('address',true);	
			//print_r($address_id);exit;
			$obj_rest = new Restcall();		
			$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/get_ship_address/".$address_id,
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array(),
						 );
			//curst/userid/add_id
			
			$address_data = $obj_rest->_execute($options_address);
			//print_r($address_data);exit;
			//exit;
		}
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers/".$userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true"
						 );
		
		$user = $obj_rest->_execute($options);
		$user_phone = $user['billing']['phone'];
		$cart = $this->session->userdata('cart');
		//print_r($cart_data);exit;
		$data  = array(
						"menu_data"=>$menu_data,
						"stored_cookie" => $stored_cookie,
						"address_data"	=> $address_data,
						"cart_data"		=> $cart_data,
						"cart"			=> $cart,
						"userdata"		=> $userdata,
						"return_url"	=>site_url().'checkout/callback',
						"surl"			=> site_url().'checkout/success',
						"furl"			=> site_url().'checkout/failure',
						"phone_no"		=> $user_phone,
						"meta_title"	=>"",
						"meta_keyword"	=>"",
						"meta_desc"		=> "",
						);
		$this->load->view('checkout',$data);
	}


	// initialized cURL Request
    private function get_curl_handle($payment_id, $amount)  {
        $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
        $key_id = rzp_test_KEY_ID;
        $key_secret = rzp_test_KEY_SECRET;
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');
        return $ch;
    }   


    // callback method
    public function callback() {        
        if (!empty($this->input->post('razorpay_payment_id')) ) {

            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');
            $userdata = $this->session->userdata('user');
		 	$currency = $this->session->userdata('currency');
		 	///cart data
		 	$cartdata = $this->session->userdata('cart');
		 	$pincode = $this->session->userdata('pincode');

		 	foreach ($cartdata as $cart) {
				$param_cart[]  = array('product_id' => $cart['product_id'],
										'quantity'=>$cart['quantity'],
										'variation_id' => $cart['variation_id'],
										"subtotal"=> (string)$cart['line_total'],
										"total"=> (string)$cart['line_total'] ); 
			}

			///cart total_data 
			$obj_cart = new Cart();
			$cart_total = $obj_cart->calulate_cart_total();
			//$cart_shipping = $obj_cart->calculate($pincode);
			$cart_shipping = $this->session->userdata('shipping_cost');

			$payment_method=$this->input->post('payment',true);
			$address_id=$this->input->post('address_id',true);

			$obj_rest = new Restcall();		
					$options_address =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/get_ship_address/".$address_id,
								"method" =>"GET",
								"auth"	 =>	"true",
								"param"	 => array(),
								 );
					//curst/userid/add_id
				
					$address_data = $obj_rest->_execute($options_address);
					$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers/".$userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true"
						 );
		
					$user = $obj_rest->_execute($options);
				 	$user_phone = $user['billing']['phone'];

					$address_data = $address_data['data']['meta_value'];

					$shipping_method[] = array(
											"method_title"=>"Flat rate",
											"method_id"=>"flat_rate",
											"total"=> (string)$cart_shipping);
					//print_r(json_encode($shipping_method));exit;
					
					foreach($address_data as $key=>$value){
						//echo $value."=====";
						if($value==""){
							$address_data[$key] = "";
						}
					}
					
					$billing_address  = array(
											"first_name"=>$address_data['shipping_first_name'],
											"last_name"=>$address_data['shipping_last_name'],
											"company"=>"",
											"address_1"=>$address_data['shipping_address_1'],
											"address_2"=>$address_data['shipping_address_2'],
											"city"=>$address_data['shipping_city'],
											"state"=>$address_data['shipping_state'],
											"postcode"=>$address_data['shipping_postcode'],
											"country"=>$address_data['shipping_country'],
											"email"=>$userdata['email'],
											"phone"=>$address_data['shipping_mobile_no'],
											);
					$shipping_address = array(
											"first_name"=>$address_data['shipping_first_name'],
											"last_name"=>$address_data['shipping_last_name'],
											"company"=>"",
											"address_1"=>$address_data['shipping_address_1'],
											"address_2"=>$address_data['shipping_address_2'],
											"city"=>$address_data['shipping_city'],
											"state"=>$address_data['shipping_state'],
											"postcode"=>$address_data['shipping_postcode'],
											"country"=>$address_data['shipping_country'],
											"email"=>$userdata['email'],
											"phone"=>$address_data['shipping_mobile_no'],
											);

					$args = array(
								"payment_method" 		=>$payment_method,
								"payment_method_title"	=> "Online Prepaid",
								"billing"				=> $billing_address,
								"shipping"				=> $shipping_address,
								"line_items"			=> $param_cart,
								"customer_id"			=> $userdata['id'],
								"currency"				=> $currency,
								"shipping_lines"		=> $shipping_method
								);

					$options_order =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 => array(),
								"body"	 => $args	
								 );
					//print_r($options_order);exit;
					$order_data = $obj_rest->_execute($options_order);
					$merchant_order_id = $order_data["id"];
					$order_id = $order_data['id'];

            $currency_code = $currency;
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = '';
            try {                
                $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                    //var_dump($error);exit;
                } else {
                    $response_array = json_decode($result, true);
                    //echo "<pre>".print_r($response_array);
                    //var_dump($http_status);
                    //exit;
                        //Check success response
                        if ($http_status === 200 and isset($response_array['error']) === false) {
                            $success = true;
                        } else {
                            $success = false;
                            if (!empty($response_array['error']['code'])) {
                                $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                            }
                        }
                }
                //close connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'OPENCART_ERROR:Request to Razorpay Failed';
            }
            if ($success === true) {
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
        
                 }
                 
                 $this->session->unset_userdata('cart');
				 $obj_cart->delete_cart_db();
                 $payload =  array(
											"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/".$order_id,
											"method" =>"PUT",
											"auth"	 =>	"true",
											"param"	 => array(),
											"body" =>  array(
															"payment_method" => $response_array['method'],
															"payment_method_title"	=> "Online Prepaid",
															"status"=>"paid",
															"transaction_id" => $response_array['id']
															)
											 );
                 	 //print_r($payload);
			         $order_update = $obj_rest->_execute($payload);
			         //print_r($order_update);exit;
                if (!$order_info['order_status_id']) {
                    redirect($this->input->post('merchant_surl_id')."/".$order_id);
                } else {
                    redirect($this->input->post('merchant_surl_id')."/".$order_id);
                }

            } else {
            	$payload =  array(
											"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/".$order_id,
											"method" =>"PUT",
											"auth"	 =>	"true",
											"param"	 => array(),
											"body" =>  array("status"=>"failed")
											 );
			    $order_update = $obj_rest->_execute($payload);
                redirect($this->input->post('merchant_furl_id'));
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    } 

    public function process(){
    	 $currency = $this->session->userdata('currency');          
         
		if(isset($_POST) && !empty($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('payment','payment method','required|xss_clean');
		 	$this->form_validation->set_rules('address_id','address','required|xss_clean');
		 	//$this->form_validation->set_rules('quantity','Quantity','required|xss_clean');
		 	///user data
		 	$userdata = $this->session->userdata('user');
		 	$currency = $this->session->userdata('currency');
		 	///cart data
		 	$cartdata = $this->session->userdata('cart');
		 	$pincode = $this->session->userdata('pincode');
		 	//var_dump($cartdata);exit;
		 	foreach ($cartdata as $cart) {
				$param_cart[]  = array('product_id' => $cart['product_id'],
										'variation_id' => $cart['variation_id'],
										'quantity'=>$cart['quantity'],
										"subtotal"=> (string)$cart['line_total'],
										"total"=> (string)$cart['line_total'] ); 
			}
			//print_r($_POST);exit;
			//print_r($param_cart);exit;
			///cart total_data 
			$obj_cart = new Cart();
			$cart_total = $obj_cart->calulate_cart_total();
			//$cart_shipping = $obj_cart->calculate($pincode);
			$cart_shipping = $this->session->userdata('shipping_cost');
			if($this->form_validation->run())
				{
					$payment_method=$this->input->post('payment',true);
					$address_id=$this->input->post('address_id',true);	

					$obj_rest = new Restcall();		
					$options_address =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/get_ship_address/".$address_id,
								"method" =>"GET",
								"auth"	 =>	"true",
								"param"	 => array(),
								 );
					//curst/userid/add_id
				
					$address_data = $obj_rest->_execute($options_address);
					$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/customers/".$userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true"
						 );
		
					$user = $obj_rest->_execute($options);
				 	$user_phone = $user['billing']['phone'];
				 	$address_data = $address_data['data']['meta_value'];

					$shipping_method[] = array(
											"method_title"=>"Flat rate",
											"method_id"=>"flat_rate",
											"total"=> (string)$cart_shipping);
					//print_r(json_encode($shipping_method));exit;
					
					foreach($address_data as $key=>$value){
						//echo $value."=====";
						if($value==""){
							$address_data[$key] = "";
						}
					}
					
					$billing_address  = array(
											"first_name"=>$address_data['shipping_first_name'],
											"last_name"=>$address_data['shipping_last_name'],
											"company"=>"",
											"address_1"=>$address_data['shipping_address_1'],
											"address_2"=>$address_data['shipping_address_2'],
											"city"=>$address_data['shipping_city'],
											"state"=>$address_data['shipping_state'],
											"postcode"=>$address_data['shipping_postcode'],
											"country"=>$address_data['shipping_country'],
											"email"=>$userdata['email'],
											"phone"=>$address_data['shipping_mobile_no'],
											);
					$shipping_address = array(
											"first_name"=>$address_data['shipping_first_name'],
											"last_name"=>$address_data['shipping_last_name'],
											"company"=>"",
											"address_1"=>$address_data['shipping_address_1'],
											"address_2"=>$address_data['shipping_address_2'],
											"city"=>$address_data['shipping_city'],
											"state"=>$address_data['shipping_state'],
											"postcode"=>$address_data['shipping_postcode'],
											"country"=>$address_data['shipping_country'],
											"email"=>$userdata['email'],
											"phone"=>$address_data['shipping_mobile_no'],
											);

					$args = array(
								"payment_method" 		=>$payment_method,
								"payment_method_title"	=> "Cash on Delivery",
								"billing"				=> $billing_address,
								"shipping"				=> $shipping_address,
								"line_items"			=> $param_cart,
								"customer_id"			=> $userdata['id'],
								"currency"				=> $currency,
								"shipping_lines"		=> $shipping_method
								);
					
					
					$options_order =  array(
								"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 => array(),
								"body"	 => $args	
								 );

					$order_data = $obj_rest->_execute($options_order);
					//print_r($order_data);exit;
					if(isset($order_data["id"]) && $order_data["id"]!=""){
						$this->session->unset_userdata('cart');
						$obj_cart->delete_cart_db();
							redirect(base_url().'checkout/success/'.$order_data["id"]);
					}else{
						redirect(base_url().'checkout/failure');
					}
				}else{
						redirect(base_url().'checkout/failure');
					}
		 }
    }
	

	public function process_capture(){
		if(isset($_POST) && !empty($_POST))
		 {
					$response = $_POST;
					$obj_rest = new Restcall();
				    if(is_array($response)){
				        $str = $response['msg'];
				    }else if(is_string($response) && strstr($response, 'msg=')){
				        $outputStr = str_replace('msg=', '', $response);
				        $outputArr = explode('&', $outputStr);
				        $str = $outputArr[0];
				    }else {
				        $str = $response;
				    }
				    //print_r($response);
				    $transactionResponseBean = new TransactionResponseBean();

				    $transactionResponseBean->setResponsePayload($str);
				    $transactionResponseBean->key = $_SESSION['key'];
				    $transactionResponseBean->iv = $_SESSION['iv'];

				    $response = $transactionResponseBean->getResponsePayload();
				    //print_r($response);exit;
				    $response1 = explode('|', $response);
			        $firstToken = explode('=', $response1[0]);
			        $orderToken = explode('=', $response1[3]);
			        $tranToken = explode('=', $response1[5]);
			        $status = $firstToken[1];
			        $order_id =  $orderToken[1];  
			        $transaction_id =  $tranToken[1];  
			        $options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/".$order_id,
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array(),
						"body" =>  array()
						 );
			        if($response=="ERROR067"){
			        	$payload =  array(
											"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/".$order_id,
											"method" =>"PUT",
											"auth"	 =>	"true",
											"param"	 => array(),
											"body" =>  array("status"=>"failed")
											 );
			            $order_update = $obj_rest->_execute($payload);
			        	redirect(base_url().'checkout/failure');
			        }
			        $order_data = $obj_rest->_execute($options);	
			        if( ($order_id != '') && ($order_id == $order_data['id'])){
			        	try{
			        		if($order_data['status'] !=='completed'){
			        			if($status == '300') { 
			        				$transauthorised = true;
			        				$msg['message'] = $this->paynimo_success_msg;
			            			$msg['class'] = 'success';
			            			if($order_data['status'] !=='processing'){
			            				
			            				$payload =  array(
											"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/".$order_id,
											"method" =>"PUT",
											"auth"	 =>	"true",
											"param"	 => array(),
											"body" =>  array(
															"payment_method" => "online",
															"status"=>"processing",
															"meta_data"=>$meta_args_data,
															"transaction_id" => $transaction_id
															)
											 );
			            				$order_update = $obj_rest->_execute($payload);

			            				redirect(base_url().'checkout/success/'.$order_id);
			            			}
			        			}
			        		}
			        	}catch (Exception $e) {        
			        		$payload =  array(
											"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/".$order_id,
											"method" =>"PUT",
											"auth"	 =>	"true",
											"param"	 => array(),
											"body" =>  array(
															"payment_method" => "online",
															"status"=>"failed",
															"transaction_id" => $transaction_id
															)
											 );
			            				$order_update = $obj_rest->_execute($payload);
	            						redirect(base_url().'checkout/failure');
            			}	
			        }
			       

				    session_destroy();
				}else{
					header("Location: ".base_url());
				}
	}

	public function success(){
		$this->load->library('curl');
		$this->load->helper('cookie');	
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$order_id =$this->uri->segment(3);           
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		
		//var_dump($product);exit;
		
		$data  = array(
						"menu_data"=>$menu_data,
						"stored_cookie" => $stored_cookie,
						"status"		=> "success",
						"order_id"		=> $order_id
						);
		$this->load->view('status-page',$data);
	}

	public function failure(){
		$this->load->library('curl');
		$this->load->helper('cookie');	
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);

		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		
		
		//var_dump($product);exit;
		
		$data  = array(
						"menu_data"=>$menu_data,
						"stored_cookie" => $stored_cookie,
						"status"		=> "failure"
						);
		$this->load->view('status-page',$data);
	}


	
}