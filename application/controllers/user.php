<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
class User extends CI_Controller {
	 //protected $nonce_chars;
	public function __construct()	
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		
		$this->load->library('curl');
		$this->load->library('session');
		$this->load->helper('menu_helper');
		/*$this->load->library('facebook');
		$this->load->library('google');
		
		$this->load->helper('cookie');	
		$this->config->load('facebook');*/
	}

	public function register(){
		$this->load->model('email_sending');

		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('name','Name','required|xss_clean');
			$this->form_validation->set_rules('emailid','Email ID','required|xss_clean');
			$this->form_validation->set_rules('pass','Password','required|xss_clean');
			$this->form_validation->set_rules('mob','Phone Number','required|xss_clean');
			if($this->form_validation->run())
				{
		 		$name=$this->input->post('name',true);
		 		$name = explode(" ", $name);
		 		$first_name = $name[0];
		 		//print_r($name);exit;
		 		if(!isset($name[1])){
		 			
		 			$last_name = "test";
		 		}else{
		 			$last_name = $name[1];
		 		}
		 		
		 		$otp = mt_rand(100000, 999999);
				$email_id=$this->input->post('emailid',true);
				$user_pass=$this->input->post('pass',true);
				$phone_no=$this->input->post('mob',true);
				$user = $this->master_model->getRow("table_verification",array("email"=>$email_id),"id,registered");
				if(empty($user)){
					$args = array(
									"email"			=>$email_id,
									"first_name"	=>$first_name,
									"password"		=>$user_pass,
									"last_name"     => $last_name,
									"mobile"		=> $phone_no,
									"otp"			=> $otp,
									"registered"	=> 0
									);
					$insert_user = $this->master_model->insertRecord("table_verification",$args);
					if($insert_user){
						$info_arr=array('from'=>'noreply@finshiksha.com','to'=>$email_id,'subject'=>'Otp for Finshiksha Registration','view'=>'register-otp');
						$other_info = array('otp'=>$otp);
						$status = $this->email_sending->sendmail($info_arr,$other_info);
						$response['status'] = 'success';
						$response['email_id'] = $email_id;
						$response['message'] = "Otp Sent to email id";
					}

				}else if($user['registered']==0){
					$otp = mt_rand(100000, 999999);
					$args = array(
									"otp"			=> $otp,
									);
					$update_user = $this->master_model->updateRecord("table_verification",$args,array("id"=>$user['id']));
					if($update_user){
						$info_arr=array('from'=>'noreply@finshiksha.com','to'=>$email_id,'subject'=>'Otp for Finshiksha Registration','view'=>'register-otp');
						$other_info = array('otp'=>$otp);
						$status = $this->email_sending->sendmail($info_arr,$other_info);
						$response['status'] = 'success';
						$response['email_id'] = $email_id;
						$response['message'] = "Otp Sent to email id";
					}
				}else{
					$response['status'] = 'error';
					$response['message'] = "User already registered";
					//$this->session->set_flashdata('message', "registration-success");
				}

				
			}else{
				$response['status'] = 'error';
				$response['message']=$this->form_validation->error_string();
			}
				
				echo json_encode($response);
				//if($user_data)
		 }
	}

	public function submit_otp(){
		$obj_rest = new Restcall();
		if(isset($_POST))
		 {
		 	$response = array();
			$this->form_validation->set_rules('otp_val','OTP','required|xss_clean');
			if($this->form_validation->run())
				{
					$otp_val=$this->input->post('otp_val',true);
					$user = $this->master_model->getRow("table_verification",array("otp"=>$otp_val,"registered"=>0),"*");
					if(!empty($user)){
						$args = array(
									"username"		=>$user['email'],
									"email"			=>$user['email'],
									"first_name"	=>$user['first_name'],
									"password"		=>$user['password'],
									"last_name"     =>$user['last_name'],
									"mobile"		=>$user['mobile'],
									"groups"		=> "Software Testing",
									"registered_date" => "10/07/2020",
									"user_type"=> "student",
									"alternate_number"=>"9845098450",
									"social_id"=>"23245",
									"course_status"=> "completed",
									"college"=>"abc college",
									"year_of_passing"=> "2000",
									"years_of_work_exp"=> "5",
									"current_organisation"=> "CRY Foundation",
									"current_area_of_work"=>"social-service",
									"target_area_of_work"=> "finance"
									);
						//print_r($args);
						$options =  array(
								"url" 	 => $this->config->item('api_url')."/api/createuser",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 =>  array(),
								"body"	 => $args
								 );
				
						$user_data = $obj_rest->_execute($options);
						//var_dump($user_data);
						if($user_data[0]['status']=="205"){
							$response['status'] = 'error';
							$response['message'] = "User already exist.Please <a href='".base_url()."#logintab' onclick='gotologin();'>login</a> to continue";
						}else if($user_data[0]['status']=="201"){
							$update_user = $this->master_model->updateRecord("table_verification",array("registered"=>1),array("id"=>$user['id']));
							$response['status'] = 'success';
							$response['message'] = "Registered Susccessfully.Please <a href='".base_url()."#logintab' onclick='gotologin();'>login</a> to continue";

						}
						//print_r($user_data);
					}else{
						$response['status'] = 'error';
						$response['message'] = "Invalid OTP";
					}
				}else
				{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
				}
				echo json_encode($response);
		 }
	}

	public function resend_otp(){
		$this->load->model('email_sending');

		if(isset($_POST))
		 {
		 	$response = array();
			$this->form_validation->set_rules('reg_email','Email ID','required|xss_clean');
			if($this->form_validation->run())
				{
					$email_id=$this->input->post('reg_email',true);
					$otp = mt_rand(100000, 999999);
					$user = $this->master_model->getRow("table_verification",array("email"=>$email_id),"id,registered");
					if(!empty($user) && $user['registered']==0){
						$args = array(
									"otp"			=> $otp,
									);
						$update_user = $this->master_model->updateRecord("table_verification",$args,array("id"=>$user['id']));
						if($update_user){
							$info_arr=array('from'=>'noreply@finshiksha.com','to'=>$email_id,'subject'=>'Otp for Finshiksha Registration','view'=>'register-otp');
							$other_info = array('otp'=>$otp);
							$status = $this->email_sending->sendmail($info_arr,$other_info);
							$response['status'] = 'success';
							$response['email_id'] = $email_id;
							$response['message'] = "Otp Sent to email id";
						}
					}else{

					}

				}else{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
					

				}
				echo json_encode($response);
		 }
	}

	public function login(){

		$obj_rest = new Restcall();

		if(isset($_POST))
		 {
		 	$response = array();
			$this->form_validation->set_rules('emailid','Email ID','required|xss_clean');
			$this->form_validation->set_rules('pass','Password','required|xss_clean');

			if($this->form_validation->run())
				{
					$email_id=$this->input->post('emailid',true);
					$user_pass=$this->input->post('pass',true);
					
						$args = array(
									"username"		=>$email_id,
									"password"		=>$user_pass,
									);
						//print_r($args);
						$options =  array(
								"url" 	 => $this->config->item('api_url')."/api/login/user",
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 =>  array(),
								"body"	 => $args
								 );
				
						$user_data = $obj_rest->_execute($options);
						//print_r($user_data);exit;
						switch ($user_data[0]['status']) {
							case '401':
									$response['status'] = 'error';
									$response['message'] = "Invalid Password";
								break;
							case '403':
									$response['status'] = 'error';
									$response['message'] = "User is Blocked";
								break;
							case '404':
									$response['status'] = 'error';
									$response['message'] = "User not registered";
								break;
							case '200':
									$response['status'] = 'success';
									$response['message'] = "Login successfull";

								break;
							
							default:
								# code...
								break;
						}
						//print_r($user_data[0]['status']);exit;
						if($user_data[0]['status']==200){
							//echo 111;
							$user = $this->master_model->getRow("table_verification",array("email"=>$user_data[0]['username']),"first_name,last_name");
							$res= array(
										"username"=>$user_data[0]['username'],
										"uid"	  =>$user_data[0]['uid'],
										"groups"  =>$user_data[0]['groups'],
										"first_name"=>$user['first_name'],
										"last_name"=>$user['last_name'],
										);
							$this->session->set_userdata('user', $res);	
							//header("Location: ".base_url());
							//header("Location: http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
							//exit;
						}
						
						//print_r($user_data);
					
				}else
				{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
				}
				echo json_encode($response);
		 }
	

	}

	public function logout(){
		$this->session->unset_userdata('user');
		header("Location: ".base_url());
	}

	public function buycourse(){
		$obj_rest = new Restcall();
		$userdata = $this->session->userdata('user');		
		if($userdata){
			if(isset($_POST))
			 {
			 	$response = array();
				$this->form_validation->set_rules('courseId','Course Id','required|xss_clean');
				$this->form_validation->set_rules('subType','Password','required|xss_clean');
				if($this->form_validation->run())
					{
						$courseId=$this->input->post('courseId',true);
						$subType=$this->input->post('subType',true);

						$callback = "/api/addtocart";
						$parameters = "uid=".$userdata['uid'];
						$method = "POST";
						$authUrl = $obj_rest->getAuthenticatedURL($callback,$parameters,$method);
						//print_r($authUrl);
						$args = array(
										"product_nid" => $courseId,
										"subscription_type" => $subType
									 );
						$options =  array(
								"url" 	 => $authUrl,
								"method" =>"POST",
								"auth"	 =>	"true",
								"param"	 =>  array(),
								"body"	 => $args
								 );
				
						$cart_data = $obj_rest->_execute($options);
						if($cart_data){
							$cart = $this->getCartData();
							$cartcount = count($cart);
							$response['status'] = 'success';
							$response['data'] = $cartcount;
							$response['message'] = "Product added to cart successfully";
						}else{
							$response['status'] = 'error';
							$response['message'] = "Product already exist in cart";
						}
					}
			}	
		}else{
			$response['status'] = 'login-error';
			$response['message']= "Please login";
		}
		echo json_encode($response);
		
	}

	public function getCartData(){
		$obj_rest = new Restcall();
		$userdata = $this->session->userdata('user');	
			$callback = "/api/getcartitems";
			$parameters = "uid=".$userdata['uid'];
			$method = "GET";
			$authUrl = $obj_rest->getAuthenticatedURL($callback,$parameters,$method);
			$args = array();
			$options =  array(
									"url" 	 => $authUrl,
									"method" =>"GET",
									"auth"	 =>	"true",
									"param"	 =>  array(),
									"body"	 => $args
									 );
					
			$cart_data = $obj_rest->_execute($options);
			return $cart_data;
	}

	public function getcart(){
		$obj_rest = new Restcall();
		$userdata = $this->session->userdata('user');		
		if($userdata){
			$userdata = $this->session->userdata('user');	
			$callback = "/api/getcartitems";
			$parameters = "uid=".$userdata['uid'];
			$method = "GET";
			$authUrl = $obj_rest->getAuthenticatedURL($callback,$parameters,$method);
			$args = array();
			$options =  array(
									"url" 	 => $authUrl,
									"method" =>"GET",
									"auth"	 =>	"true",
									"param"	 =>  array(),
									"body"	 => $args
									 );
					
			$cart_data = $obj_rest->_execute($options);
			$response['status'] = 'success';
			$response['message']= "Please login";
			$response['data'] = $cart_data;
			//print_r($response);
		}else{
			$response['status'] = 'login-error';
			$response['message']= "Please login";
		}
		echo json_encode($response);
		//print_r($cart_data);
	}

	public function removecart(){
			$obj_rest = new Restcall();
			$userdata = $this->session->userdata('user');		
			$callback = "/api/removecartitems";
			$parameters = "uid=".$userdata['uid'];
			$method = "POST";
			$authUrl = $obj_rest->getAuthenticatedURL($callback,$parameters,$method);
			$args = array("cartitem_id"=>"4");
			$options =  array(
									"url" 	 => $authUrl,
									"method" =>"POST",
									"auth"	 =>	"true",
									"param"	 =>  array(),
									"body"	 => $args
									 );
			echo $authUrl;
			$cart_data = $obj_rest->_execute($options);
			print_r($cart_data);
	}
}