<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		$this->load->helper('menu_helper');
    }

	public function index()
	{
		$this->load->library('curl');
		
		$this->load->model('master_model');
		$faqs = $this->master_model->getRecords('faq');
		//print_r($faqs);exit;
		$data  = array("faqs"=>$faqs,"meta_title"	=> "Faqs",);

		$this->load->view('faqs',$data);
	}

	
	
}