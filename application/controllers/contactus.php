<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		$this->load->helper('menu_helper');
    }

	public function index(){

		$this->load->library('curl');
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		
		$data  = array("menu_data"=>$menu_data,"meta_title"	=> "Contact Us",);
		$this->load->view('contactus',$data);
	}

	public function submitEnquiry(){
		$this->load->model('email_sending');
		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('enquiry_name','Name','required|xss_clean');
			$this->form_validation->set_rules('enquiry_email','Email ID','required|xss_clean');
			$this->form_validation->set_rules('enquiry_comment','Password','required|xss_clean');
			if($this->form_validation->run())
				{
					$name=$this->input->post('enquiry_name',true);
					$email_id=$this->input->post('enquiry_email',true);
					$comment=$this->input->post('enquiry_comment',true);
					$admin_id = 'contact@rasoitatva.com,sammeer.jadhav@gmail.com';
					$info_arr=array('from'=>'noreply@rasoitatva.com','to'=>$admin_id,'subject'=>'Enquiry from Rasoitatva','view'=>'contact-us');
					$other_info = array('name' => $name, 'email_id'=>$email_id,'comment'=>$comment);
					$status = $this->email_sending->sendmail($info_arr,$other_info);

					$info_arr_customer=array('from'=>'noreply@rasoitatva.com','to'=>$email_id,'subject'=>"We're Listening",'view'=>'contact-us-customer');
					$other_info_customer = array('name' => $name);
					$status_customer = $this->email_sending->sendmail($info_arr_customer,$other_info_customer);
					if($status){
						//json_encode(array("status"=>"success"));
						echo "success";
					}else{ 
						echo "fail";
						//json_encode(array("status"=>"fail"));
					}
		    	    //$resp = $this->my_model->enquiry($params);
				}
		 }
	}
	public function _index(){
		$this->load->model('email_sending');
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			
		        
					$params = json_decode(file_get_contents('php://input'), TRUE);
			//print_r($params);
			if ($params['email'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Email ID can\'t empty');

					} else if($params['g-recaptcha-response']==""){
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Please click on the reCAPTCHA box.');
					}else {
							$secret = '6Le7t0YUAAAAAFmxnm5LjCF_9aKOG-CYRuXFdh4q';
							$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$params['g-recaptcha-response']);
							$responseData = json_decode($verifyResponse);
							
							if($responseData->success){
							
								$respStatus = 200;
			unset($params['g-recaptcha-response']);
								$email_id = 'connect@thea.club,deepti@thea.club,';
								//$email_id="sammeer.jadhav@gmail.com,pulkit.rangwani@gmail.com";
								$info_arr=array('from'=>'noreply@thea.club','to'=>$email_id,'subject'=>'Enquiry from The A Club','view'=>'contact-us');
								$other_info=$params;
								$this->email_sending->sendmail($info_arr,$other_info);
		    	    			$resp = $this->my_model->enquiry($params);
			}else{
								$respStatus = 400;
								$resp = array('status' => 400,'message' =>  'Captcha Not Valid');
							}
		        			        		
					}
					json_output($respStatus,$resp);
		        
			
		}
	}

	public function location(){
		$method = $_SERVER['REQUEST_METHOD'];
			if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			
		        	$response['status'] = 200;
		        	$resp = $this->my_model->location_all_data();

		        	foreach ($resp as $key => $value) {
		        		//
		        		//print_r($value);
		        		$value->img_name = base_url().'uploads/'.$value->img_name;
		        	}
		        	
		        	$output  = array(
		        					'data' =>  $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);
			
		}
	}

	public function enquiry(){
		//echo 123;exit;
		$method = $_SERVER['REQUEST_METHOD'];

		$this->load->model('email_sending');
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			
		        
					$params = json_decode(file_get_contents('php://input'), TRUE);
					if ($params['email'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Email ID can\'t empty');

					} else if($params['g-recaptcha-response']==""){
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Please click on the reCAPTCHA box.');
					}else {
							$secret = '6Le7t0YUAAAAAFmxnm5LjCF_9aKOG-CYRuXFdh4q';
							$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$params['g-recaptcha-response']);
							$responseData = json_decode($verifyResponse);
							if($responseData->success){
								$respStatus = 200;
								unset($params['g-recaptcha-response']);
								//$email_id = 'connect@thea.club,deepti@thea.club';
								$email_id="order@theskinpantry.com";
		    	    			$info_arr=array('from'=>'admin@thea.club','to'=>$email_id,'subject'=>'Enquiry from The A Club','view'=>'contact-us');
			 					$other_info=$params;
								$this->email_sending->sendmail($info_arr,$other_info);
		    	    			$resp = $this->my_model->enquiry($params);
		    	    			
							}else{
								$respStatus = 400;
								$resp = array('status' => 400,'message' =>  'Captcha Not Valid');
							}
		        			        		
					}
					json_output($respStatus,$resp);
		        
			
		}
	}



}