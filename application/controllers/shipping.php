<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php'); 
class Shipping extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/

		$this->load->helper('menu_helper');
		check_login_checkout();
    }

	public function index()
	{
		if(get_cart_count()==0){
			redirect(base_url().'cart');
		}
		$this->load->library('curl');
		$userdata = $this->session->userdata('user');
		$obj_rest = new Restcall();
		$obj_cart = new Cart();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);

		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/get_ship_address",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array()
						 );
		
		$customer_address = $obj_rest->_execute($options);
		
		$country = $this->master_model->getRecords("countries",array('id' => 101 ),"*", array('name' => "asc" ));
		//print_r($customer_address);exit;
		$cart_total = $obj_cart->calulate_cart_total();
		//print_r($cart_total);exit;

		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();


		$data  = array(
						"menu_data"=>$menu_data,
						"address"  =>$customer_address,
						"country_data"=> $country,
						"cart_total"	=>$cart_total,
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						);
		$this->load->view('shipping',$data);
	}

	public function get_state(){
		$country_id = $this->input->get('country_id',true);
		$country = $this->master_model->getRecords("countries",array('name' => $country_id ),"*", array('name' => "asc" ));
		
		$state_data = $this->master_model->getRecords("states",array('country_id' => $country[0]['id'] ),"*", array('name' => "asc" ));
		$data  = array(
						"state_data"=>$state_data);
		//$this->load->view("states",$data);
		echo json_encode($state_data);
	}

	public function get_city(){
		$state_id = $this->input->get('state_id',true);
		$state = $this->master_model->getRecords("states",array('name' => $state_id ),"*", array('name' => "asc" ));
		
		$city_data = $this->master_model->getRecords("cities",array('state_id' => $state[0]['id'] ),"*", array('name' => "asc" ));
		$data  = array(
						"city_data"=>$city_data);
		//$this->load->view("cities",$data);
		echo json_encode($city_data);
	}

	public function add(){
		$obj_rest = new Restcall();
		$userdata = $this->session->userdata('user');
		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('shipping_first_name','Name','required|xss_clean');
			$this->form_validation->set_rules('shipping_address_1','Address','required|xss_clean');
			$this->form_validation->set_rules('shipping_city','City','required|xss_clean');
			$this->form_validation->set_rules('shipping_state','State','required|xss_clean');
			$this->form_validation->set_rules('shipping_postcode','Pincode','required|xss_clean');
			$this->form_validation->set_rules('label','Type of address','required|xss_clean');
			if($this->form_validation->run()){

				$shipping_first_name=$this->input->post('shipping_first_name',true);
				$shipping_address_1=$this->input->post('shipping_address_1',true);
				$shipping_address_2=$this->input->post('shipping_address_2',true);
				$shipping_city=$this->input->post('shipping_city',true);
				$shipping_state=$this->input->post('shipping_state',true);
				$shipping_postcode=$this->input->post('shipping_postcode',true);
				$label=$this->input->post('label',true);
				$label = $label[0];
		 		
		 		$args = array(
		 					"label"=>$label,
							"shipping_first_name"	=>$shipping_first_name,
							"shipping_last_name" 	=> "",
							"shipping_country" 		=> "IN",
							"shipping_address_1"	=>$shipping_address_1,
							"shipping_address_2"	=>$shipping_address_2,
							"shipping_city"			=>$shipping_city,
							"shipping_state"		=>$shipping_state,
							"shipping_postcode"		=>$shipping_postcode,
							"shipping_address_is_default" => "false"
		 					);

			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/add_shipping",
							"method" =>"POST",
							"auth"	 =>	"true",
							"param"	 => array(),
							"body"	 => $args
							 );
			
			$add_address = $obj_rest->_execute($options);
				$response['status'] = $add_address['code'];
				$response['message'] = $add_address['message'];
			}else{
				$response['status'] = 'error';
				$response['message']=$this->form_validation->error_string();
			}
			echo json_encode($response);
		}
	}

	

	
}
