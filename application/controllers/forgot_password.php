<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
session_start();

class forgot_password extends CI_Controller {
	public function __construct()	
    {
        parent::__construct();

       	
		$this->load->library('curl');
		$this->load->library('session');
		$this->load->library('facebook');
		$this->load->library('google');
		$this->load->helper('menu_helper');
		$this->load->helper('cookie');	
		//$client_id = '< Generated Client Id >';
		/*$client_secret = '465807442212-n7i2ai7anaoo7lkpalsjeuvtpc5nr77m.apps.googleusercontent.com';
		$redirect_uri = 'http://dev.skin.com/login/user_google';
		$simple_api_key = 'iE8Z17hLB8OZWy5pxVQBiYaI';*/
		


    }


    public function index()
	{
		
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);
		$data  = array(
						"menu_data"=>$menu_data
						);
		//check fb login		

		$this->load->view('forgot-password',$data);
	}	
	public function send_mail_forget_password()
	{
		$this->load->model('email_sending');
		$user = $this->db->where( array('user_email' => $this->input->post('email')) )->get('wp_users')->row();
		if ($user) {
			echo $token = md5(time());
			$link = base_url('forgot_password/set_password/'.$token);

        //   print_r($user);exit;

        	/*$html = 'Hello '.$user->display_name.',<br><br><a href="'.$link.'">Click here</a> to change your password. Ignore if its not you.<br><br><br>Thank you,<br>Theskinpantry Team';
        	
        	$this->load->library('email');       
            $config['protocol']    = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'devtestmail911@gmail.com';
            $config['smtp_pass']    = 'sammeer911$';
            $config['charset']    = 'utf-8';
            $config['newline']    = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not      

            $this->email->initialize($config);
            $this->email->from('devtestmail911@gmail.com', 'Theskinpantry');
            $this->email->to($user->user_email); 
            $this->email->subject('Forget password');
            $this->email->message($html);
           	$this->email->send();*/
           	//$this->email->print_debugger();exit;
           	$info_arr =array('from'=>'noreply@rasoitatva.com','to'=>$user->user_email,'subject'=>"Forgot Password ? Reset it with link below",'view'=>'forgot_pass');
			$other_info = array('link' => $link, 'customer_name'=>$user->display_name);
			$this->email_sending->sendmail($info_arr,$other_info);
			$this->session->set_flashdata('msg', 'forgot-pass-mail-sent');
        	$this->db->update('wp_users',  array('token' => $token ), array('id' =>$user->ID ) );
		}else{
			$this->session->set_flashdata('msg', 'not-valid-user');

		}
		//redirect('forget_password');
		redirect(base_url().'forgot_password');
	}

	public function set_password($value='')
	{
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);
		$data  = array(
						"menu_data"=>$menu_data
						);
		//check fb login
		$token =  $this->uri->segment('3');
		$user = $this->db->where( array('token' => $token) )->get('wp_users')->row();
		
		if ($user) {
			$this->load->view('set_password', $data);
		}else{
			$this->load->view('set_password_disable', $data);
		}
	}
	public function save_password()
	{
		if ($this->input->post('token')) {			
		$user = $this->db->where( array('token' => $this->input->post('token')) )->get('wp_users')->row();		
			if ($user) {
	        	$this->db->update('wp_users',  array('user_pass' => md5($this->input->post('password')), 'token'=>'' ), array('id' =>$user->ID ) );
				$this->session->set_flashdata('msg', 'Your password is successfully changed.');
			}else{
				$this->session->set_flashdata('msg', 'User not found');
			}//

		}else{			
				$this->session->set_flashdata('msg', 'User not found');
		}
		redirect(base_url().'login');			
		
	}

	
}