<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
//require_once(APPPATH.'controllers/bluedart.php'); 
require_once(APPPATH.'controllers/cart.php');

class Orders extends CI_Controller {
	public function __construct()	
    {
        parent::__construct();

       	
		$this->load->library('curl');
		$this->load->helper('menu_helper');
	   	check_login_page('orders');
    }


    public function index()
	{
		$userdata = $this->session->userdata('user');		
		$this->load->helper('cookie');	
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		
		//print_r($userdata);exit;
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders?customer=". $userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array('customer' => $userdata['id'] ),
						"body" =>  array()
						 );
		
		$cust_orders = $obj_rest->_execute($options);

			
		

		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();

		$data  = array(
						"menu_data"=>$menu_data,
						"stored_cookie" => $stored_cookie,
						"userdata" =>$userdata,
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						"cust_orders" => $cust_orders
						);
		$this->load->view('orders',$data);
	}	

	public function order_summary(){
		$order_id = $this->input->get('order_id');
		
		$userdata = $this->session->userdata('user');		
		$this->load->helper('cookie');	
		$obj_rest = new Restcall();
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders/".$order_id,
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array()
						 );
		
		$order_summary = $obj_rest->_execute($options);	
		//print_r($order_summary);exit;
		$cart_total = $order_summary['total'] - $order_summary['shipping_total'] ;
		$order_tax = $order_summary['total_tax'];
		if($order_summary['currency']=="INR"){
			$cart_amt = $order_summary['total'] - $order_summary['shipping_total'] ;
			$cart_total =  round($cart_amt / (1.18)); 
			$order_tax = round($order_summary['total'] - $cart_total);
		}
		$res_data = array(
						"currency"=>$order_summary['currency'],
						"cart_total"=>$cart_total,
						"total_tax"=>$order_tax,
						"subtotal" => $cart_total + $order_tax,
						"total"	=>$order_summary['total'],
						"shipping_total"=>$order_summary['shipping_total'],
						"first_name"=>$order_summary['shipping']['first_name'],
						"last_name"=>$order_summary['shipping']['last_name'],
						"address_1"=>$order_summary['shipping']['address_1'],
						"address_2"=>$order_summary['shipping']['address_2'],
						"city"=>$order_summary['shipping']['city'],
						"state"=>$order_summary['shipping']['state'],
						"postcode"=>$order_summary['shipping']['postcode'],
						"country"=>$order_summary['shipping']['country']
						);
		echo json_encode($res_data);
	}

	public function support(){
		$this->load->model('email_sending');
		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('support_order_id','Order Id','required|xss_clean');
			$this->form_validation->set_rules('support_comment','Query','required|xss_clean');
			
			if($this->form_validation->run())
				{
					$order_id=$this->input->post('support_order_id',true);
					$query=$this->input->post('support_comment',true);
					
					$email_id = 'help@theskinpantry.com';
					$info_arr=array('from'=>'noreply@theskinpantry.com','to'=>$email_id,'subject'=>'Support query for order no '.$order_id,'view'=>'support');
					$other_info = array('order_id' => $order_id, 'query'=>$query,);

					////customer mail
					$userdata = $this->session->userdata('user');
					$info_arr_customer =array('from'=>'noreply@theskinpantry.com','to'=>$userdata['email'],'subject'=>"We're eager to help",'view'=>'customer_support');
					$other_info_customer = array('order_id' => $order_id, 'customer_name'=>$userdata['user_display_name'],);
					

					$status = $this->email_sending->sendmail($info_arr,$other_info);
					if($status){
						$this->email_sending->sendmail($info_arr_customer,$other_info_customer);
						//json_encode(array("status"=>"success"));
						echo "success";
					}else{ 
						echo "fail";
						//json_encode(array("status"=>"fail"));
					}
		    	    //$resp = $this->my_model->enquiry($params);
				}
		 }
	}
	
}