<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php');

class Product extends CI_Controller {
	public function __construct()	
    {
        parent::__construct();

       	
		$this->load->library('curl');
		$this->load->helper('menu_helper');
    }

 	public function index()
	{

		$obj_rest = new Restcall();
		$last = $this->uri->total_segments();
		$product_slug = $this->uri->segment($last);
		$this->load->helper('cookie');			
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}

		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?slug=".$product_slug,
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("slug"=>$product_slug)
						 );
		
		$product = $obj_rest->_execute($options);
		//print_r($product);exit;
		$seo_title = "";
		$seo_description = "";
		$seo_keyword = "";
		foreach($product[0]['meta_data'] as $product_meta){
			if($product_meta['key']=="seo_title"){
				$seo_title= $product_meta['value'];
			}
			if($product_meta['key']=="seo_description"){
				$seo_description = $product_meta['value'];
			}
			if($product_meta['key']=="seo_keyword"){
				$seo_keyword = $product_meta['value'];
			}
			
		}

		$breadcrum = array();
		foreach ($product[0]['categories'] as $key => $row)
		{
		    //$price[$key] = $row['id'];
		    $breadcrum[$key]  = array(
		    					'id' => $row['id'],
		    					'name'=>$row['name'],
		    					"slug"=>$row['slug'] );
		}
		//print_r($breadcrum);exit;
		/*for($i=0;$i<count($price);$i++){
			$price[$i] = 
		}*/
		array_multisort($breadcrum, SORT_ASC, $product[0]['categories']);

		//print_r($breadcrum);exit;
		$upsell_product = array();
		if(!empty($product[0]['upsell_ids'])){
			$upsell_product_ids = implode($product[0]['upsell_ids'],",");	
			//echo $product_ids;
			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?include=".$upsell_product_ids,
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array("include"=>$upsell_product_ids)
							 );


			
			$upsell_product = $obj_rest->_execute($options);

		}else{
			$related_product_ids = implode($product[0]['related_ids'],",");	

			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?include=".$related_product_ids,
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array("include"=>$related_product_ids)
							 );


			
			$upsell_product = $obj_rest->_execute($options);
		}
		//print_r($upsell_product);exit;
		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();
		//print_r($cart_data);exit;
		//print_r($upsell_product);exit;
		$data  = array(
						
						"product" 		=> $product,
						"breadcrum"		=> $breadcrum,
						"upsell_product"=>$upsell_product,
						"stored_cookie" => $stored_cookie,
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						"meta_title"	=> $seo_title,
						"meta_keyword"	=> $seo_keyword,
						"meta_desc"		=> 	$seo_description,					
						);
		//print_r($data);exit;
		$this->load->view('product',$data);
	}

	public function summary($product_id){
		$obj_rest = new Restcall();
		$this->load->helper('cookie');			
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products/".$product_id,
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array()
						 );
		
		$product = $obj_rest->_execute($options);
		//print_r($product);exit;
		$data  = array("product" => $product,"stored_cookie" => $stored_cookie);
		$this->load->view('product-summary',$data);

	}


}