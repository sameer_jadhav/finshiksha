<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php');
class Wishlist extends CI_Controller {
	 protected $nonce_chars;
	public function __construct()	
    {
        parent::__construct();
        $this->load->library('curl');
		$this->load->helper('cookie');	
		$this->load->helper('menu_helper');
		//$this->load->helper('menu_helper');
	   	
	}

	public function index()
	{

		check_login_page('wishlist');
		
		//print_r($stored_cookie);exit;
		$this->load->library('curl');
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);

		$obj_rest = new Restcall();

		$userdata = $this->session->userdata('user');
		$data = $this->master_model->getRow('table_wishlist', array('user_id'=>$userdata['id']),  array('product_id') );
		if(!empty($data)){
          set_cookie('wishlist_cookie',$data['product_id'],'3600');
        }else{
        	delete_cookie('wishlist_cookie');
        	//redirect($this->uri->uri_string());
        	$stored_cookie  = get_cookie('wishlist_cookie');	
        	if($stored_cookie!=""){
        		redirect($this->uri->uri_string());
        	}
        }

		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		//print_r($stored_cookie);exit;
		

		$products = array();		
				
			for ($i=0; $i < count($stored_cookie); $i++) { 
				$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products/".$stored_cookie[$i],
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array()
							 );
			
				$products[] = $obj_rest->_execute($options);
			}

		//$products = $obj_rest->_execute($options_product);
		//print_r($products);exit;

		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();

		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders?customer=". $userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array('customer' => $userdata['id'] ),
						"body" =>  array()
						 );
		
		$cust_orders = $obj_rest->_execute($options);
		//print_r($cart_data);exit;
		
		$data  = array(
			"menu_data"=>$menu_data,
			"userdata" =>$userdata,
			"products" => $products,
			"cart" 			=> $cart_data['cart'],
			"product_fly"	=> $cart_data['product_fly'],
			"cart_total"	=> $cart_data['cart_total'],
			"stored_cookie" => $stored_cookie,
			"cust_orders" => $cust_orders


		);
		$this->load->view('wishlist',$data);
	}	

	public function add($product_id='')
	{		
		$log_status = check_login_ajax("wishlist");
		if($log_status==true){
			echo json_encode( array('status' => true,"redirect"=>true ) ); 
			exit;
		}

		$stored_cookie = get_cookie('wishlist_cookie');
		
		if ($stored_cookie) {
			$product_cookie = explode(",",$stored_cookie);
				if(!in_array($product_id, $product_cookie)){
					 $product_id = $stored_cookie.','.$product_id;
				}
		  	
		}
		set_cookie('wishlist_cookie',$product_id,'3600');
		$this->add_wish_db($product_id);
		echo json_encode( array('status' => true,"redirect"=>false ) ); 
	}	
	public function remove($product_id='')
	{
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}

		$new_cookie = '';
		if (($key = array_search($product_id, $stored_cookie)) !== false) {
		    unset($stored_cookie[$key]);
		    if ($stored_cookie) {
		    	$new_cookie = implode(',', $stored_cookie);	    	
		    }else{
		    	$no_cookie= "";
		    	$this->remove_wish_db($no_cookie);	
		    }

			delete_cookie('wishlist_cookie');
			if ($new_cookie) {
				set_cookie('wishlist_cookie',$new_cookie,'3600');	
				$this->remove_wish_db($new_cookie);			
			}
		}

		echo json_encode( array('status' => true ) ); 
	}	

	public function add_wish_db($product_id){
		$userdata = $this->session->userdata('user');
		$input_array = array(
							'user_id' 	 => $userdata['id'],
							'product_id' => $product_id
							 );

		$wish_data = $this->master_model->getRecords('table_wishlist',array("user_id"=>$userdata['id']),'table_wishlist.*'); 
		
		if(count($wish_data)>0){
			$input_array_data = array(
							'user_id' 	 => $userdata['id'],
							'product_id' => $product_id
							 );
			$this->master_model->updateRecord('table_wishlist',$input_array_data,array('id'=>$wish_data[0]['id']));
		}else{
			$this->master_model->insertRecord('table_wishlist',$input_array);
		}
		//

	}

	public function remove_wish_db($product_id){
		$userdata = $this->session->userdata('user');
		$wish_data = $this->master_model->getRecords('table_wishlist',array("user_id"=>$userdata['id']),'table_wishlist.*');
		if($product_id==""){
			
			$this->master_model->deleteRecord('table_wishlist','id',$wish_data[0]['id']);
		}else{
			if(count($wish_data)>0){
			$input_array_data = array(
							'user_id' 	 => $userdata['id'],
							'product_id' => $product_id
							 );
			
			$this->master_model->updateRecord('table_wishlist',$input_array_data,array('id'=>$wish_data[0]['id']));
			}	
		} 
		
		
	}
}