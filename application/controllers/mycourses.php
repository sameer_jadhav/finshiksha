<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
class Mycourses extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('menu_helper');
		
		//$this->load->library('../controllers/cart');

		
    }

	public function index()
	{
		check_login();
		$obj_rest = new Restcall();
		$userData = $this->session->userdata('user');
		
		$callback = "/api/login/redirect";
		$parameters = "uid=".$userData['uid']."&path=/show/all/courses";

		$method = "GET";
		$authUrl = $obj_rest->getAuthenticatedURL($callback,$parameters,$method);
		//print_r($authUrl);
		header('Location:'.$authUrl);
	}
}


