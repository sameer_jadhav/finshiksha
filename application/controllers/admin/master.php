<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Master extends Private_Admin {
	function __construct() {
        parent::__construct();
        $this->load->model('slider_model', 'model');
	}
	public function index() { 
        $data['master']  = $this->db->get('master')->row();
        $data['about_images']  = $this->db->get('about_images')->result();
        
		$this->view('master', $data);
	}

    public function update()
    {
      /*  print_r( $this->input->post());    
        exit;*/
        //$this->_validate();    
        $data = array(
                'short_about' => $this->input->post('short_about'),
                'description1' => $this->input->post('description1'),
                'description2' => $this->input->post('description2'),
                'description3' => $this->input->post('description3'),
                'address' => $this->input->post('address'),
                'contact' => $this->input->post('contact'),
                'toll_free' => $this->input->post('toll_free'),
                'copy_right' => $this->input->post('copy_right'),
            );
            if (!empty($_FILES['image1']['name'])) {
                $upload = _do_upload('image1', 'slider');
                $data['image1'] = $upload;
            }
            if (!empty($_FILES['image2']['name'])) {
                $upload = _do_upload('image2', 'slider');
                $data['image2'] = $upload;
            }
            if (!empty($_FILES['image3']['name'])) {
                $upload = _do_upload('image3', 'slider');
                $data['image3'] = $upload;
            }
            if (!empty($_FILES['image4']['name'])) {
                $upload = _do_upload('image4', 'slider');
                $data['image4'] = $upload;
            }
            if (!empty($_FILES['image5']['name'])) {
                $upload = _do_upload('image5', 'slider');
                $data['image5'] = $upload;
            }
            if (!empty($_FILES['image6']['name'])) {
                $upload = _do_upload('image6', 'slider');
                $data['image6'] = $upload;
            }
            if (!empty($_FILES['image7']['name'])) {
                $upload = _do_upload('image7', 'slider');
                $data['image7'] = $upload;
            }
            $this->db->update('master', $data, array('id' => 1));     
            //print_r($_FILES['multiple_image']);exit;
         /*   if (!empty($_FILES['multiple_image']['name'][0])) {                 
                        echo $count = count($_FILES['multiple_image']['name']); // count element exi
                       $files = $_FILES;
                        $count = count($_FILES['multiple_image']['name']); // count element 
                        for($i=0; $i<$count; $i++):
                            $_FILES['multiple_image']['name'] = $files['multiple_image']['name'][$i];
                            $_FILES['multiple_image']['type'] = $files['multiple_image']['type'][$i];
                            $_FILES['multiple_image']['tmp_name'] = $files['multiple_image']['tmp_name'][$i];
                            $_FILES['multiple_image']['error'] = $files['multiple_image']['error'][$i];
                            $_FILES['multiple_image']['size'] = $files['multiple_image']['size'][$i];
                            $config['upload_path'] = './uploads/slider/';
                            $target_path = './uploads/slider';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg';
                            $config['max_size'] = '10000'; //limit 1 mb
                            $config['remove_spaces'] = true;
                            $config['overwrite'] = false;
                            $fileName = _do_upload('multiple_image', 'slider');                            
                            $about_images['image'] = $fileName;            
                            $gallery_insert = $this->db->insert('about_images', $about_images);
                                     
                        endfor;         
                    }*/
            //echo $this->db->last_query();exit;
        redirect(admin_url('master'));
    }


    public function delete_image($id)
    {   
        $this->db->where('id', $id);
        $this->db->delete('about_images');
        echo json_encode(array("status" => TRUE));
    }
	
}