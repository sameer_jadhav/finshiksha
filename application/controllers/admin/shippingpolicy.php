<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shippingpolicy extends Private_Admin {
	function __construct() {
        parent::__construct();        
	}
	public function index() { 
        $data['shippingpolicy']  = $this->db->get('shippingpolicy')->row();       
		$this->view('shipping-policy', $data);
	}

    public function update()
    {        
        $data = array(
                'content' => $this->input->post('content'),               
            );        
            $this->db->update('shippingpolicy', $data, array('id' => 1));     
        redirect(admin_url('shippingpolicy'));
    }

}