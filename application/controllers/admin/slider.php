<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Slider extends Private_Admin {
	function __construct() {
        parent::__construct();
       $this->load->model('slider_model', 'model');
	}
	public function index() { 	
		$this->view('slider');
	}
	public function data_list()
    {
        //also used while adding user
        
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $model->id;
            $row[] = $model->title;
            $row[] = $model->sequence;
            $content = '';
            if ($model->content_type==2) {
                $content = '<a target="_blank" href="'.$model->video.'">Video</a>';
                # code...
            }
            $row[] ='<a href="'.base_url('uploads/slider/'.$model->image).'" target="_blank"><img width="50px" src="'.base_url('uploads/slider/'.$model->image).'"></a> <br>'. $content;
         
            //add html for action
            $row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$model->id."'".')"><i class="fa fa-edit"></i></a>
                  <a href="javascript:void(0)" title="Delete" onclick="delete_value('."'".$model->id."'".')"><i class="fa fa-trash-o"></i></a>';
         
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function edit($id)
    {
        $data = $this->model->get_by_id($id);
        echo json_encode($data);
    }

    public function add()
    {
        $this->_validate();
        $data = array(
                'title' => $this->input->post('title'),
                'video' => $this->input->post('video'),   
                'video_mobile' => $this->input->post('video_mobile'),            
                'content_type' => $this->input->post('content_type'),    
                'sequence' => $this->input->post('sequence'),   

            );
        
            if (!empty($_FILES['image']['name'])) {
                $upload = _do_upload('image', 'slider');
                $data['image'] = $upload;
            }else{
                $data['inputerror'][] = 'image';
                $data['error_string'][] = 'Required';
                $data['status'] = FALSE;
                echo json_encode($data);
                exit();
            }

            if (!empty($_FILES['mobile_image']['name'])) {
                $upload = _do_upload('mobile_image', 'slider');
                $data['mobile_image'] = $upload;
            }else{
                $data['inputerror'][] = 'mobile_image';
                $data['error_string'][] = 'Required';
                $data['status'] = FALSE;
                echo json_encode($data);
                exit();
            }
        
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }
    public function update()
    {
        $this->_validate();    
        $data = array(
                'title' => $this->input->post('title'),
                'video' => $this->input->post('video'),
                'video_mobile' => $this->input->post('video_mobile'),
                'content_type' => $this->input->post('content_type'),
                'sequence' => $this->input->post('sequence'),


            );
        
            if (!empty($_FILES['image']['name'])) {
                $upload = _do_upload('image', 'slider');
                $data['image'] = $upload;
            }

             if (!empty($_FILES['mobile_image']['name'])) {
                $upload = _do_upload('mobile_image', 'slider');
                $data['mobile_image'] = $upload;
            }
         
        $this->model->update(array('id' => $this->input->post('id')), $data); 
        //echo $this->db->last_query() ;
        echo json_encode(array("status" => TRUE));
    }
 
    public function delete($id)
    {          
        $this->model->delete_by_id($id); 
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
        

        if($this->input->post('title') == '')
        {
            $data['inputerror'][] = 'title';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }
        if($this->input->post('content_type') == '')
        {
            $data['inputerror'][] = 'content_type';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }
        if ($this->input->post('content_type')==2) {
            if($this->input->post('video') == '')
            {
                $data['inputerror'][] = 'video';
                $data['error_string'][] = 'Required';
                $data['status'] = FALSE;
            }  
            if(!filter_var($this->input->post('video'), FILTER_VALIDATE_URL))
            {
                $data['inputerror'][] = 'video';
                $data['error_string'][] = 'Invalid URL';
                $data['status'] = FALSE;
            }     
        }
        
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }	


	
}