<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Private_Admin {
	function __construct() {
        parent::__construct();
        //$this->load->model('master_model', 'master');
	}

	public function index() { 	
		$this->view('home');
	}	
	
}