<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payments extends Private_Admin {
	function __construct() {
        parent::__construct();        
	}
	public function index() { 
        $data['payments']  = $this->db->get('paymentpolicy')->row();       
		$this->view('payments', $data);
	}

    public function update()
    {        
        $data = array(
                'content' => $this->input->post('content'),               
            );        
            $this->db->update('paymentpolicy', $data, array('id' => 1));     
        redirect(admin_url('payments'));
    }

}