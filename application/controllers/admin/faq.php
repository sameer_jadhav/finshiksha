<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Faq extends Private_Admin {
	function __construct() {
        parent::__construct();
       $this->load->model('faq_model', 'model');
	}
	public function index() { 	
		$this->view('faq');
	}
	public function data_list()
    {
        //also used while adding user
        
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $model->id;
            $row[] = $model->question;
            $row[] = $model->answer;
            $row[] = ucfirst($model->category);
          
            //add html for action
            $row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$model->id."'".')"><i class="fa fa-edit"></i></a>
                  <a href="javascript:void(0)" title="Delete" onclick="delete_value('."'".$model->id."'".')"><i class="fa fa-trash-o"></i></a>';
         
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function edit($id)
    {
        $data = $this->model->get_by_id($id);
        echo json_encode($data);
    }

    public function add()
    {
        $this->_validate();
        $data = array(
                'question' => $this->input->post('question'),
                'category' => $this->input->post('category'),
                'answer' => $this->input->post('answer'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }
    public function update()
    {
        $this->_validate();    
        $data = array(
                'question' => $this->input->post('question'),
                'category' => $this->input->post('category'),
                'answer' => $this->input->post('answer'),
            );
        $this->model->update(array('id' => $this->input->post('id')), $data); 
        echo json_encode(array("status" => TRUE));
    }
 
    public function delete($id)
    {          
        $this->model->delete_by_id($id); 
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
        
        if($this->input->post('question') == '')
        {
            $data['inputerror'][] = 'question';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }
        /*if($this->input->post('category') == '')
        {
            $data['inputerror'][] = 'category';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }*/
        if($this->input->post('answer') == '')
        {
            $data['inputerror'][] = 'answer_error';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }
        
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }	


	
}