<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/Restcall.php');

class Testimonial extends Private_Admin {
	function __construct() {
        parent::__construct();
       $this->load->model('testimonial_model', 'model');
	}
	public function index() { 	
        /*$obj_rest = new Restcall();

        $options =  array(
                        "url"    => $this->config->item('api_url')."wp-json/wc/v3/products",
                        "method" =>"GET",
                        "auth"   => "true",
                        "param"  => array()
                         );
        
        $data['product'] = $obj_rest->_execute($options);   */  
        //echo $this->_title;exit;           
        //print_r($this->_admindata);exit;           
		$this->view('testimonial');
	}
	public function data_list()
    {
        //also used while adding user

        
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $model->id;
            $row[] = $model->sequence;
            $row[] = $model->comment;
            $row[] = $model->author;
            
            $row[] = $model->pages;
          
            //add html for action
            $row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit('."'".$model->id."'".')"><i class="fa fa-edit"></i></a>
                  <a href="javascript:void(0)" title="Delete" onclick="delete_value('."'".$model->id."'".')"><i class="fa fa-trash-o"></i></a>';
         
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function edit($id)
    {
        $data = $this->model->get_by_id($id);        
        echo json_encode($data);
    }

    public function getPorduct(){
        $obj_rest = new Restcall();
        //echo $this->config->item('api_url')."wp-json/wc/v3/products";exit;
        $options =  array(
                        "url"    => $this->config->item('api_url')."wp-json/wc/v3/products?order=asc&per_page=100",
                        "method" =>"GET",
                        "auth"   => "true",
                        "param"  => array("order"=>"asc","per_page"=>100)
                         );
        
        $product = $obj_rest->_execute($options);
        //print_r($product);exit;
        $product_arr  = array();
        foreach($product as $key => $value){
            $product_arr[$key]['id'] = $value['slug'];
            $product_arr[$key]['title'] = $value['name'];
        }
       
        array_push($product_arr,array('id' =>'home', 'title' =>'Home'),array('id' =>'the-skin-pantry', 'title' =>'About'));

        $data =  array(
                     array('id' =>'home', 'title' =>'Home'),
                     array('id' =>'the-skin-pantry', 'title' =>'About'),
                     
                 );
       

        echo json_encode($product_arr);

    }

    public function add()
    {
        $this->_validate();
        $data = array(

                'comment' => $this->input->post('comment'),
                'author' => $this->input->post('author'),
                'sequence' => $this->input->post('sequence'),
                'pages' =>  implode(",",$this->input->post('pages')),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }
    public function update()
    {
        $this->_validate();    
        $data = array(
                'comment' => $this->input->post('comment'),
                'author' => $this->input->post('author'),
                'sequence' => $this->input->post('sequence'),
                'pages' =>  implode(",",$this->input->post('pages')),
                
            );
        $this->model->update(array('id' => $this->input->post('id')), $data); 
        echo json_encode(array("status" => TRUE));
    }
 
    public function delete($id)
    {          
        $this->model->delete_by_id($id); 
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
        
        if($this->input->post('comment') == '')
        {
            $data['inputerror'][] = 'comment';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }
        if($this->input->post('author') == '')
        {
            $data['inputerror'][] = 'author';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }
        if($this->input->post('pages') == '')
        {
            $data['inputerror'][] = 'pages_hidden';
            $data['error_string'][] = 'Required';
            $data['status'] = FALSE;
        }
        
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }	


	
}