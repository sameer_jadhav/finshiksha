<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php');

class Manageaddress extends CI_Controller {
	public function __construct()	
    {
        parent::__construct();

       	
		$this->load->library('curl');
		$this->load->helper('menu_helper');
	   	check_login();
    }


    public function index()
	{
		$userdata = $this->session->userdata('user');		
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);
		

		$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/get_ship_address",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array(),						
						 );
		$country = $this->master_model->getRecords("countries","","*", array('name' => "asc" ));
		$user = $obj_rest->_execute($options_address);
		//print_r($user);exit;
		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();

		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/orders?customer=". $userdata['id'],
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array('customer' => $userdata['id'] ),
						"body" =>  array()
						 );
		
		$cust_orders = $obj_rest->_execute($options);
		
		$data  = array(
						"menu_data"=>$menu_data,
						"user_address"=>$user,	
						"country_data"=> $country,
						"userdata" =>$userdata,
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						"meta_title"	=>"",
						"meta_keyword"	=>"",
						"meta_desc"		=> "",	
						"cust_orders" => $cust_orders		
						);


		//$this->db->where('status','pending');
        $query=$this->db->get('countries');
        $country_data 	 =  $query->result();
        $data['country'] = $country;
		$this->load->view('manage-address',$data);
	}

	public function get_by_id($id)
	{
		$userdata = $this->session->userdata('user');		

		$obj_rest = new Restcall();		
		$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/get_ship_address/".$id,
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array(),
						 );
			//curst/userid/add_id
		
		$data = $obj_rest->_execute($options_address);

		
		
        echo json_encode($data);
	}

	public function get_state($country_id){
		$this->db->where('counry_id',$country_id);
        $query=$this->db->get('states');
        $country_data 	 =  $query->result();
        $data['state'] = $country_data;
        echo json_encode($data);
	}

	public function getpincodedata($pincode){

		$this->db->where('pincode',$pincode);
        $query=$this->db->get('table_pincode');
        $pincode_data 	 =  $query->result();
        $data['pindata'] = $pincode_data;
        echo json_encode($data);
	}

	public function get_city($state_id){
		$this->db->where('state_id',$country_id);
        $query=$this->db->get('cities');
        $country_data 	 =  $query->result();
        $data['city'] = $country_data;
        echo json_encode($data);
	}



	public function add($value='')
	{
		$userdata = $this->session->userdata('user');		
		$obj_rest = new Restcall();
		$this->_validate();		
		$label=$this->input->post('label',true);
		$label = $label[0];
		//print_r($_POST);exit;
				$address=  array(
						"label" => $label,
						"shipping_first_name" => $this->input->post('shipping_first_name'),
						"shipping_country" => $this->input->post('shipping_country'),
						"shipping_address_1" => $this->input->post('shipping_address_1'),
						"shipping_address_2" => $this->input->post('shipping_address_2'),
						"shipping_city" => $this->input->post('shipping_city'),
						"shipping_state" => $this->input->post('shipping_state'),
						"shipping_postcode" => $this->input->post('shipping_postcode'),
						"shipping_mobile_no" => $this->input->post('shipping_mobile_no'),
						"shipping_email_id" => $this->input->post('shipping_email_id'),
						"shipping_address_is_default" => $this->input->post('shipping_address_is_default')
					);		  
		$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/add_shipping",
						"method" =>"POST",
						"auth"	 =>	"true",
						"param"	 => array(),	
						"body" => $address				
						 );

		$this->session->set_flashdata('address-msg', 'address-ad-success');
		//$this->session->set_flashdata('set-address', 'true');
		//$options_address;
		$user = $obj_rest->_execute($options_address);
		if($this->input->post('shipping_address_is_default')=="true"){
			$this->make_default_internal($userdata['id'],$user['data'][0]['add_id']);
		}
		//print_r($user);exit;
		$this->session->set_flashdata('set-address-id', $user['data'][0]['add_id']);
		echo json_encode( array('status' => true ));
		
	}
	public function update()
	{
		$userdata = $this->session->userdata('user');		
		$obj_rest = new Restcall();
		//print_r($this->input->post());exit;
		$this->_validate();
		$label=$this->input->post('label',true);
		$label = $label[0];
		
				$address=  array(
						"label" => $label,
						"shipping_first_name" => $this->input->post('shipping_first_name'),
						
						"shipping_country" => $this->input->post('shipping_country'),
						"shipping_address_1" => $this->input->post('shipping_address_1'),
						"shipping_address_2" => $this->input->post('shipping_address_2'),
						"shipping_city" => $this->input->post('shipping_city'),
						"shipping_state" => $this->input->post('shipping_state'),
						"shipping_postcode" => $this->input->post('shipping_postcode'),
						"shipping_mobile_no" => $this->input->post('shipping_mobile_no'),
						"shipping_email_id" => $this->input->post('shipping_email_id'),
						"shipping_address_is_default" => $this->input->post('shipping_address_is_default')
					);		  
		$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/update_shipping"."/".$this->input->post('add_id'),					
						"method" =>"POST",
						"auth"	 =>	"true",
						"param"	 => array(),	
						"body" => $address				
						 );
		
		//$options_address;

		$this->session->set_flashdata('address-msg', 'address-up-success');
		$user = $obj_rest->_execute($options_address);
		if($this->input->post('shipping_address_is_default')=="true"){
			$this->make_default_internal($userdata['id'],$this->input->post('add_id'));
		}


		echo json_encode( array('status' => true ));
		
	}

	public function make_default_internal($user_id,$add_id){
			$userdata = $this->session->userdata('user');		
			$obj_rest = new Restcall();
			$address=  array(

						"shipping_address_is_default" => 1
					);		  
			$options_address =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$user_id."/default"."/".$add_id,					
							"method" =>"POST",
							"auth"	 =>	"true",
							"param"	 => array(),	
							"body" => $address				
							 );
			
			//$options_address;
			$user_add = $obj_rest->_execute($options_address);
		
	}
	public function make_default($add_id)
	{
		$userdata = $this->session->userdata('user');		
		$obj_rest = new Restcall();
		//print_r($this->input->post());exit;
		
				$address=  array(

						"shipping_address_is_default" => 1
					);		  
		$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/default"."/".$add_id,					
						"method" =>"POST",
						"auth"	 =>	"true",
						"param"	 => array(),	
						"body" => $address				
						 );
		
		//$options_address;
		$user = $obj_rest->_execute($options_address);
		$this->session->set_flashdata('address-msg', 'address-up-success');
		echo json_encode( $user);
		
	}

	public function delete($id)
	{
		
		$userdata = $this->session->userdata('user');		
		$obj_rest = new Restcall();					
		$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/delete_shipping/".$id,
						"method" => "DELETE",
						"auth"	 =>	"true",
						"param"	 => array(),				
						 );
		
		//$options_address;
		$this->session->set_flashdata('address-msg', 'address-del-success');
		$result = $obj_rest->_execute($options_address);
		echo json_encode( $result);
	}

	public function _validate($value='')
	{
		   $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
        
		if($this->input->post('label') == '')
		{
			$data['inputerror'][] = 'label';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}
		if($this->input->post('shipping_first_name') == '')
		{
			$data['inputerror'][] = 'shipping_first_name';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}
		
		if($this->input->post('shipping_country') == '')
		{
			$data['inputerror'][] = 'shipping_country';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}
		if($this->input->post('shipping_address_1') == '')
		{
			$data['inputerror'][] = 'shipping_address_1';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}
		if($this->input->post('shipping_address_2') == '')
		{
			$data['inputerror'][] = 'shipping_address_2';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}
		if($this->input->post('shipping_city') == '')
		{
			$data['inputerror'][] = 'shipping_city';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}

		if($this->input->post('shipping_email_id') == '')
		{
			$data['inputerror'][] = 'shipping_email_id';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}
		if($this->input->post('shipping_state') == '')
		{
			$data['inputerror'][] = 'shipping_state';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}
		if($this->input->post('shipping_postcode') == '')
		{
			$data['inputerror'][] = 'shipping_postcode';
			$data['error_string'][] = 'Required';
			$data['status'] = FALSE;
		}

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	


/*	

	public function add($value='')
	{
		$userdata = $this->session->userdata('user');		
		$obj_rest = new Restcall();

		//print_r($this->input->post());exit;
		for ($i=0; $i < count($this->input->post('label')) ; $i++) { 			
			if ($this->input->post('shipping_first_name')[$i]!='') {				
				$address[] =  array(
						"label" => $this->input->post('label')[$i],
						"shipping_first_name" => $this->input->post('shipping_first_name')[$i],
						"shipping_last_name" => $this->input->post('shipping_last_name')[$i],
						"shipping_country" => $this->input->post('shipping_country')[$i],
						"shipping_address_1" => $this->input->post('shipping_address_1')[$i],
						"shipping_address_2" => $this->input->post('shipping_address_2')[$i],
						"shipping_city" => $this->input->post('shipping_city')[$i],
						"shipping_state" => $this->input->post('shipping_state')[$i],
						"shipping_postcode" => $this->input->post('shipping_postcode')[$i],
						"shipping_address_is_default" => $this->input->post('shipping_address_is_default')[$i]
					);
			}
			
		}
		$options_address =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/multiaddress/".$userdata['id']."/add_ship_address",
						"method" =>"POST",
						"auth"	 =>	"true",
						"param"	 => array(),	
						"body" => $address				
						 );
		
		//$options_address;
		$user = $obj_rest->_execute($options_address);

		echo json_encode( array('status' => true ));
		
	}
*/
	
}